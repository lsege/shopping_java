package com.lsege.service.shopService;

import com.lsege.entity.Result;
import com.lsege.entity.WithdrawInfo;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import com.lsege.mapper.WithdrawInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2018/3/22.
 */
@Service
public class WithdrawInfoServiceImpl implements WithdrawInfoService {

    @Autowired
    WithdrawInfoMapper withdrawInfoMapper;

    /**
     * 保存提现预设信息
     * @param request
     * @param payAccount
     * @param realName
     * @return
     */
    @Override
    public Result saveWithdrawInfo(HttpServletRequest request, String payAccount, String realName) {
        String userId=request.getAttribute("id").toString();
        if (null == userId || userId.equals("") || userId.length()<=0){
            /**未登录*/
            return ResultUtil.error(ResultEnum.TOKEN_ERROR);
        } else {
            WithdrawInfo withdrawInfo = new WithdrawInfo();
            withdrawInfo.setMerchantUser(userId);
            withdrawInfo.setPayAccount(payAccount);
            withdrawInfo.setRealName(realName);
            withdrawInfoMapper.saveWithdrawInfo(withdrawInfo);
            return ResultUtil.success(ResultEnum.SUCCESS);
        }
    }

    /**
     * 删除提现预设信息
     * @param request
     * @param withdrawInfoId
     * @return
     */
    @Override
    public Result deleteWithdrawInfo(HttpServletRequest request, Integer withdrawInfoId) {
        String userId=request.getAttribute("id").toString();
        if (null == userId || userId.equals("") || userId.length()<=0){
            /**未登录*/
            return ResultUtil.error(ResultEnum.TOKEN_ERROR);
        } else {
            Integer id = withdrawInfoMapper.queryWithdrawInfoId(userId,withdrawInfoId);
            if (null == id){
                /**该提现记录不属于当前用户*/
                return ResultUtil.error(ResultEnum.IS_NOT_YOURS);
            } else {
                /**可以删除预设信息*/
                withdrawInfoMapper.deleteWithdrawInfo(withdrawInfoId);
                return ResultUtil.success(ResultEnum.SUCCESS);
            }
        }
    }

    /**
     * 修改提现预设信息
     * @param request
     * @param withdrawInfo
     * @return
     */
    @Override
    public Result updateWithdrawInfo(HttpServletRequest request, WithdrawInfo withdrawInfo) {
        String userId=request.getAttribute("id").toString();
        if (null == userId || userId.equals("") || userId.length()<=0){
            /**未登录*/
            return ResultUtil.error(ResultEnum.TOKEN_ERROR);
        } else {
            Integer id = withdrawInfoMapper.queryWithdrawInfoId(userId,withdrawInfo.getId());
            if (null == id){
                /**该提现记录不属于当前用户*/
                return ResultUtil.error(ResultEnum.IS_NOT_YOURS);
            } else {
                withdrawInfoMapper.updateWithdrawInfo(withdrawInfo);
                return ResultUtil.success(ResultEnum.SUCCESS);
            }
        }
    }

    @Override
    public Result queryWithDrawInfo(HttpServletRequest request) {
        String userId=request.getAttribute("id").toString();
        if (null == userId || userId.equals("") || userId.length()<=0){
            /**未登录*/
            return ResultUtil.error(ResultEnum.TOKEN_ERROR);
        } else {
            List<WithdrawInfo> list = withdrawInfoMapper.queryWithDrawInfo(userId);
            return ResultUtil.success(list);
        }
    }
}
