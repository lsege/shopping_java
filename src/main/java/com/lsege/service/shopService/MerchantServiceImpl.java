package com.lsege.service.shopService;

import com.alibaba.fastjson.support.odps.udf.CodecCheck;
import com.lsege.entity.otherVo.Alog;
import com.lsege.entity.otherVo.Attention;
import com.lsege.entity.otherVo.Orders;
import com.lsege.entity.shopVo.Merchandise;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.entity.shopVo.RecommendDynamicsVo;
import com.lsege.entity.shopVo.RecommendStaticVo;
import com.lsege.integration.HomemadeException;
import com.lsege.integration.ResultEnum;
import com.lsege.mapper.shopMapper.MerchantMapper;
import org.apache.ibatis.type.Alias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by zhanglei on 2017/10/12.
 */

@Service
public class MerchantServiceImpl implements MerchantService{

    @Autowired
    MerchantMapper merchantMapper;

    @Override
    public List<Merchant> getMerchantList(Integer type,Integer pageNum,Integer pageSize) throws HomemadeException{
        Integer startPage = (pageNum - 1) * pageSize;
        return merchantMapper.getMerchantList(type,startPage,pageSize);
    }

    @Override
    public List<Merchandise> getMerchandise(Integer merchantId,Integer pageNum,Integer pageSize) throws HomemadeException {
        Integer startPage = (pageNum - 1) * pageSize;
        List<Merchandise> list= merchantMapper.getMerchandise(merchantId,startPage,pageSize);
        return list;
    }
    @Override
    public Merchant getMerchant(Integer id) {
        return merchantMapper.getMerchant(id);
    }

    @Override
    public String queryAttent(String userId, Integer merchantId) {
        return merchantMapper.queryAttent(userId,merchantId);
    }

    @Override
    @Transactional
    public void toAttentionMerchant(String userId, Integer merchantId) {
        String state = "1";
        Date date = new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String bDate = f.format(date);
        Attention attention = new Attention();
        attention.setUserId(userId);
        attention.setMerchantId(merchantId+"");
        attention.setState(state);
        attention.setbDate(bDate);
        merchantMapper.toAttentionMerchant(attention);

        //生成动态
        String operationId = merchantMapper.queryAttent(userId,merchantId);
        //关注的字典值
        String type = "2";
        //店铺的字典值
        String logClass = "6";
        Alog alog = new Alog();
        alog.setUserId(userId);
        alog.setType(Integer.parseInt(type));
        alog.setLogClass(Integer.parseInt(logClass));
        alog.setOperationId(Integer.parseInt(operationId));
        alog.setbDate(bDate);
        merchantMapper.addLog(alog);
    }

    @Override
    public void closeAttentionMerchant(String userId, Integer merchantId) {
        merchantMapper.closeAttentionMerchant(userId,merchantId);
    }

    @Override
    public String queryALogId(String userId, String type, String logClass, String id) {
        return merchantMapper.queryALogId(userId,type,logClass,id);
    }

    @Override
    public void deleteALog(String id) {
        merchantMapper.deleteALog(id);
    }

    @Override
    public RecommendStaticVo queryRecommendStatic(String type) {
        return merchantMapper.queryRecommendStatic(type);
    }

    @Override
    public List<RecommendDynamicsVo> queryRecommendDynamics(String type) {
        return merchantMapper.queryRecommendDynamics(type);
    }

    @Override
    public void createOrder(String merchantId, String userId, Integer needMoney, Integer payMoney) {
        Date date = new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String bDate = f.format(date);
        Integer preferentialMoney = needMoney - payMoney;
        String state = "1";
        Orders orders = new Orders();
        orders.setMerchantId(merchantId);
        orders.setUserId(userId);
        orders.setPayDate(bDate);
        orders.setNeedMoney(needMoney.toString());
        orders.setPayMoney(payMoney.toString());
        orders.setState(state);
        orders.setPreferentialMoney(preferentialMoney.toString());
        merchantMapper.createOrder(orders);
    }

    @Override
    @Transactional
    public void upDate(String merchantId,Integer payMoney) {
        Integer oldMoney = merchantMapper.queryMerchantMoney(merchantId);
        Integer newMoney = oldMoney+payMoney;
        merchantMapper.upDateMerchantMoney(merchantId,newMoney);
    }
}
