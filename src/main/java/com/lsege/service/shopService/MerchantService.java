package com.lsege.service.shopService;

import com.lsege.entity.shopVo.Merchandise;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.entity.shopVo.RecommendDynamicsVo;
import com.lsege.entity.shopVo.RecommendStaticVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zhanglei on 2017/10/12.
 */
@Service
public interface MerchantService {

    List<Merchant> getMerchantList(Integer type,Integer pageNum,Integer pageSize) throws Exception;

    List<Merchandise> getMerchandise(Integer merchantId,Integer pageNum,Integer pageSize) throws Exception;
    Merchant getMerchant(Integer merchantId);

    String queryAttent(String userId, Integer merchantId);

    void toAttentionMerchant(String userId, Integer merchantId);

    void closeAttentionMerchant(String userId, Integer merchantId);

    String queryALogId(String userId, String type, String logClass, String id);

    void deleteALog(String id);

    RecommendStaticVo queryRecommendStatic(String type);

    List<RecommendDynamicsVo> queryRecommendDynamics(String type);

    void createOrder(String merchantId, String userId, Integer needMoney, Integer payMoney);

    void upDate(String merchantId,Integer payMoney);
}
