package com.lsege.service.shopService;

import com.lsege.entity.Result;
import com.lsege.entity.WithdrawInfo;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2018/3/22.
 */
public interface WithdrawInfoService {

    /**保存预设提现信息*/
    Result saveWithdrawInfo(HttpServletRequest request, String payAccount, String realName);

    /**删除提现预设信息*/
    Result deleteWithdrawInfo(HttpServletRequest request, Integer withdrawInfoId);

    /**修改提现预设信息*/
    Result updateWithdrawInfo(HttpServletRequest request, WithdrawInfo withdrawInfo);

    /**查询提现预设信息*/
    Result queryWithDrawInfo(HttpServletRequest request);
}
