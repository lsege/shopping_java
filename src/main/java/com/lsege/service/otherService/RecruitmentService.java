package com.lsege.service.otherService;

import com.lsege.entity.otherVo.Alog;
import com.lsege.entity.otherVo.Recruitment;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by zhanglei on 2017/10/12.
 */
@Service
public interface RecruitmentService {
    List<Recruitment> getRecLsit(Integer type,Integer pageNum,Integer pageSize);

    Recruitment getRecDatial(Integer id);

    void saveRecruitment(String userId, String type, String title, String context, String imageName);
}
