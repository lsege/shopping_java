package com.lsege.service.otherService;

import com.lsege.entity.otherVo.News;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.entity.vo.ActivityPicture;
import com.lsege.mapper.otherMapper.HomePageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/10/12.
 */
@Service
public class HomePageServiceImpl implements HomePageService {

    @Autowired
    HomePageMapper homePageMapper;

    @Override
    public List<ActivityPicture> loadActivityPictures() {
        return homePageMapper.loadActivityPictures();
    }

    @Override
    public List<News> loadNewsAndAdvList(Integer pageNum,Integer pageSize) {
        Integer startPage = (pageNum-1)*pageSize;
        List<News> list = homePageMapper.loadNewsAndAdvList(startPage,pageSize);
        return list;
    }

    @Override
    public List<Merchant> queryMerchantsByKeyWord(String keyWord) {
        return homePageMapper.queryMerchantsByKeyWord(keyWord);
    }
}
