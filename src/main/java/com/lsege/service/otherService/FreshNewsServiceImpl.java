package com.lsege.service.otherService;

import com.lsege.entity.otherVo.FreshNews;
import com.lsege.entity.otherVo.FreshNewsCommentariesVo;
import com.lsege.entity.otherVo.FreshNewsVo;
import com.lsege.exception.GlobalDefaultExceptionHandler;
import com.lsege.integration.HomemadeException;
import com.lsege.mapper.otherMapper.FreshNewsMapper;
import com.lsege.util.EmojiUtil;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/10/13.
 */
@Service
@Component
public class FreshNewsServiceImpl implements FreshNewsService {

    @Autowired
    FreshNewsMapper freshNewsMapper;

    @Override
    @Transactional
    public void postedFreshNew(String userId, String title, String context,String newContext, String type,String finalType,String image) {
        String readCount = "0";
        String likeCount = "0";
        String state = "1";
        Date date1 = new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(date1);

        final Base64 base64 = new Base64();
        final String text = context;
        final byte[] textByte = text.getBytes();
        final String encodedText = base64.encodeToString(textByte);

        final String title1 = title;
        final byte[] textByte1 = title1.getBytes();
        final String title2 = base64.encodeToString(textByte1);

        freshNewsMapper.postedFreshNew(userId,title2,encodedText,newContext,readCount,likeCount,state,type,date,finalType,image);

        String id = freshNewsMapper.queryId(userId,date);

        String operationType = "1";
        String infoType = "";
        if (type.equals("1")){
            infoType = "4";
        } else {
            infoType = "5";
        }
        freshNewsMapper.addALog(userId,operationType,infoType,id,date);
    }

    @Override
    public FreshNewsVo loadFreshNewInfo(HttpServletRequest request, Integer freshNewId, Integer pageNum, Integer pageSize) {
        //读取文章信息

        FreshNewsVo freshNewsVo = freshNewsMapper.loadFreshNewInfo(freshNewId);
        final Base64 base64 = new Base64();

        String context = new String(base64.decode(freshNewsVo.getContext()));
        freshNewsVo.setContext(context);

        String title = new String(base64.decode(freshNewsVo.getTitle()));
        freshNewsVo.setTitle(title);

        try{
            String userId=request.getAttribute("id").toString();
            //查看个人是否点赞
            String id = freshNewsMapper.queryUserIsLike(userId,freshNewId);
            if (id != null){
                freshNewsVo.setIsLike("1");
            } else {
                freshNewsVo.setIsLike("0");
            }
        }catch (NullPointerException e){
            freshNewsVo.setIsLike("0");
        }

        //获取评论列表
        Integer startPage = (pageNum-1)*pageSize;
        List<FreshNewsCommentariesVo> list = freshNewsMapper.loadFreshNewCommentaries(freshNewId,startPage,pageSize);

        for (int i=0; i<list.size(); i++){
            list.get(i).setCommentariesContext(new String(base64.decode(list.get(i).getCommentariesContext())));
        }

        freshNewsVo.setFreshNewsCommentariesVos(list);
        return freshNewsVo;
    }

    @Override
    public synchronized void addReadCount(Integer freshNewId) {
            Integer count1 = Integer.parseInt(freshNewsMapper.queryReadCount(freshNewId));
            Integer count = count1 + 1;
            freshNewsMapper.editReadCount(freshNewId,count);
    }

    @Override
    public List<FreshNewsVo> loadFreshNewList(String type,Integer pageNum,Integer pageSize) {
        Integer startPage = (pageNum-1)*pageSize;
        List<FreshNewsVo> list = freshNewsMapper.loadFreshNewList(type,startPage,pageSize);

        for (int i=0; i<list.size(); i++){
            Base64 base64 = new Base64();
            list.get(i).setTitle(new String(base64.decode(list.get(i).getTitle())));
            list.get(i).setContext(new String(base64.decode(list.get(i).getContext())));
        }
        return list;
    }

    @Override
    @Transactional
    public void commentFreshNew(String type,String userId, Long freshNewsId, String commentariesContext) throws HomemadeException {
        String state = "1";
        Date date1 = new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(date1);

        final Base64 base64 = new Base64();
        final String text = commentariesContext;
        final byte[] textByte = text.getBytes();
//编码
        final String encodedText = base64.encodeToString(textByte);
        System.out.println(encodedText);
//解码




        freshNewsMapper.commentFreshNew(userId,freshNewsId,encodedText,state,date);
        String id = freshNewsMapper.queryCommentId(userId,date);
        //生成日志
        String operationType = "3";
        String infoType = "";
        if (type.equals("1")){
            infoType = "4";
        } else {
            infoType = "5";
        }
        freshNewsMapper.addALog(userId,operationType,infoType,id,date);
    }

    @Override
    public String queryLike(String userId, Integer freshNewId) {
        return freshNewsMapper.queryLike(userId,freshNewId);
    }

    @Override
    public void freshNewsLike(String userId, Integer freshNewId) {
        Date date1 = new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = f.format(date1);
        freshNewsMapper.addLike(userId,freshNewId,date);
//        String id = freshNewsMapper.queryLikeId(userId,date);
//        String operationType = "5";
//        String infoType = "4";
//        freshNewsMapper.addALog(userId,operationType,infoType,id,date);
    }

    @Override
    public void addLikeCount(Integer freshNewId) {
        synchronized(this){
            Integer count1 = Integer.parseInt(freshNewsMapper.queryLikeCount(freshNewId));
            Integer count = count1 + 1;
            freshNewsMapper.editLikeCount(freshNewId,count);
        }
    }

    @Override
    public FreshNews queryFreshNews(String userId, Integer freshNewId) {
        return freshNewsMapper.queryFreshNews(userId,freshNewId);
    }

    @Override
    public void updateFreshNewsStatus(String userId, Integer freshNewId, String finalType) {
        freshNewsMapper.updateFreshNewsStatus(userId,freshNewId,finalType);
    }
}
