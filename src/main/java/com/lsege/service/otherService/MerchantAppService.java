package com.lsege.service.otherService;

import com.lsege.entity.otherVo.DepositLogs;
import com.lsege.entity.otherVo.Invitation;
import com.lsege.entity.otherVo.MerchantAppOrderVo;
import com.lsege.entity.sys.MerchantUser;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zhanglei on 2017/12/16.
 */
@Service
public interface MerchantAppService {
    List<MerchantAppOrderVo> getOrderList(String userId,Integer startNum,Integer pageSize);

    Integer getBanlance(String userId);

    Integer depositMonery(String id);

    List<DepositLogs> depositLogsList(String userId,Integer pageNum,Integer pageSize);

    Integer getUserById(String userId);

    void addInvitation(Invitation invitation);

    Integer getMerchantById(Integer merchantId);

    List<Invitation> getInvitationList(Integer merchantId, Integer pageNum, Integer pageSize);

    Invitation getInvitationInfo(Integer invitationId);

    Integer querySendCount(Integer merchantId);
}
