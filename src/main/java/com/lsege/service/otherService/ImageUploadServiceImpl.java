package com.lsege.service.otherService;

import com.lsege.util.OSSClientUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Administrator on 2017/11/29.
 */
@Service
public class ImageUploadServiceImpl implements ImageUploadService {

    @Override
    public String updateHead(MultipartFile data, int i) {
        OSSClientUtil ossClient = new OSSClientUtil();
        if (data == null || data.getSize() <= 0) {
        }
        String name = ossClient.uploadImg2Oss(data);
        String imgUrl = ossClient.getImgUrl(name);
        return imgUrl.split("\\?")[0];
    }
}
