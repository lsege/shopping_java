package com.lsege.service.otherService;

import com.lsege.entity.otherVo.Activity;
import com.lsege.entity.otherVo.Invitation;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.mapper.otherMapper.ActivityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/10/12.
 */
@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    ActivityMapper activityMapper;

    @Override
    public List<Activity> loadAllActivity(Integer pageNum, Integer pageSize) {
        Integer startPage = (pageNum - 1) * pageSize;
        return activityMapper.loadAllActivity(startPage, pageSize);
    }

    @Override
    public Activity loadActivityInfo(Integer id, Integer showType) {
        Activity activity = new Activity();
        if (showType == 1) {

            activity = activityMapper.loadTabActivityInfo(id);
        }else{
            activity = activityMapper.loadActivityInfo(id);
        }
        return activity;
    }

    @Override
    public List<Merchant> loadRecommendStoreList(Integer pageNum, Integer pageSize) {
        Integer startPage = (pageNum - 1) * pageSize;
        return activityMapper.loadRecommendStoreList(startPage, pageSize);
    }

    @Override
    public List<Invitation> getTabList(Integer pageNum, Integer pageSize, Integer type) {
        Integer startPage = (pageNum - 1) * pageSize;

        return activityMapper.getTabList(startPage, pageSize, type);
    }

    @Override
    public Invitation getTabDetail(Integer id) {
        return activityMapper.getTabDetail(id);
    }
}
