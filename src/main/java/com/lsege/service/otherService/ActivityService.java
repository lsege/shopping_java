package com.lsege.service.otherService;

import com.lsege.entity.otherVo.Activity;
import com.lsege.entity.otherVo.Invitation;
import com.lsege.entity.shopVo.Merchant;

import java.util.List;

/**
 * Created by Administrator on 2017/10/12.
 */
public interface ActivityService {

    List<Activity> loadAllActivity(Integer pageNum,Integer pageSize);

    Activity loadActivityInfo(Integer id,Integer showType);

    List<Merchant> loadRecommendStoreList(Integer pageNum,Integer pageSize);

    List<Invitation> getTabList(Integer pageNum, Integer pageSize, Integer type);

    Invitation getTabDetail(Integer id);
}
