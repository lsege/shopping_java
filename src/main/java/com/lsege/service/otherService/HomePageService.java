package com.lsege.service.otherService;

import com.lsege.entity.otherVo.News;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.entity.vo.ActivityPicture;

import java.util.List;

/**
 * Created by Administrator on 2017/10/12.
 */
public interface HomePageService {

    List<ActivityPicture> loadActivityPictures();

    List<News> loadNewsAndAdvList(Integer pageNum, Integer pageSize);

    List<Merchant> queryMerchantsByKeyWord(String keyWord);
}
