package com.lsege.service.otherService;

import com.lsege.entity.currency.CurrencyUser;
import com.lsege.entity.otherVo.DepositLogs;
import com.lsege.entity.otherVo.PayOrderVo;
import com.lsege.mapper.otherMapper.PayOrderMapper;
import com.lsege.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by zhanglei on 2017/11/24.
 */
@Service
public class PayOrderServiceImpl implements PayOrderService{

    @Autowired
    PayOrderMapper payOrderMapper;
    @Override
    public void savePayOrder(PayOrderVo payOrderVo) {
        payOrderMapper.savePayOrder(payOrderVo);
    }

    @Override
    public String getMaxOrderNumber() {
        return payOrderMapper.getMaxOrderNumber();
    }

    @Override
    public PayOrderVo getPayOrder(String orderNumber) {
        return payOrderMapper.getPayOrder(orderNumber);
    }

    @Override
    public void updateState(String transactionId) {
        payOrderMapper.updateState(transactionId);
    }

    @Override
    public CurrencyUser getUserByUserId(String userId) {
        return payOrderMapper.getUserByUserId(userId);
    }

    @Override
    public void changeVip(String userId, String vipTimeout) {
        payOrderMapper.changeVip(userId,vipTimeout);
    }

    @Override
    public void saveLogs(DepositLogs depositLogs) {
        payOrderMapper.saveLogs(depositLogs);
    }

    @Override
    public String getDay(Integer amount) {
        return payOrderMapper.getDay(amount);
    }

    @Override
    public DepositLogs getDepositLogsByLogsCode(String transactionId) {
        return payOrderMapper.getDepositLogsByLogsCode(transactionId);
    }

    @Override
    public void updateDepositLogs(DepositLogs depositLogs) {
        payOrderMapper.updateDepositLogs(depositLogs);
    }

    @Override
    public List<DepositLogs> getDepositLogsForWithDraw() {
        return payOrderMapper.getDepositLogsForWithDraw();
    }

    @Override
    public void updateBag(String merchantUserId, Integer newMoney) {
        payOrderMapper.updateBag(merchantUserId,newMoney);
    }

    @Override
    public void updateVipTimeOut(String userId,Integer date1) {
        Long vipTime = payOrderMapper.queryUserVipTimeOut(userId);
        Long vipTimeOut = Long.parseLong(vipTime+"000");
        Long nowTime = System.currentTimeMillis();
        if (nowTime > vipTimeOut){
            //vip已到期
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(nowTime+1000*60*60*24*date1);
            String time = format.format(date);
            payOrderMapper.updateVipTimeOut(userId,time);
        } else {
            //vip未到期
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(vipTimeOut+1000*60*60*24*date1);
            String time = format.format(date);
            payOrderMapper.updateVipTimeOut(userId,time);
        }

    }

    /**
     * 根据单号查询用户ID
     * @param logsCode
     * @return
     */
    @Override
    public Integer queryMerchantIdByCode(String logsCode) {
        return payOrderMapper.queryMerchantIdByCode(logsCode);
    }
}
