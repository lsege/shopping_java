package com.lsege.service.otherService;

import com.lsege.entity.currency.CurrencyUser;
import com.lsege.entity.otherVo.DepositLogs;
import com.lsege.entity.otherVo.PayOrderVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zhanglei on 2017/11/24.
 */
@Service
public interface PayOrderService {

    void savePayOrder(PayOrderVo payOrderVo);

    String getMaxOrderNumber();

    PayOrderVo getPayOrder(String orderNumber);

    void updateState(String transactionId);

    CurrencyUser getUserByUserId(String userId);

    void changeVip(String userId,String vipTimeout);

    void saveLogs(DepositLogs depositLogs);

    String getDay(Integer amount);

    DepositLogs getDepositLogsByLogsCode(String transactionId);

    void updateDepositLogs(DepositLogs depositLogs);

    List<DepositLogs> getDepositLogsForWithDraw();

    void updateBag(String merchantUserId, Integer newMoney);

    void updateVipTimeOut(String userId,Integer date);

    Integer queryMerchantIdByCode(String logsCode);
}
