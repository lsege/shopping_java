package com.lsege.service.otherService;

import com.lsege.entity.otherVo.FreshNews;
import com.lsege.entity.otherVo.FreshNewsVo;
import com.lsege.exception.GlobalDefaultExceptionHandler;
import com.lsege.integration.HomemadeException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/10/13.
 */
public interface FreshNewsService {
    void postedFreshNew(String userId, String title, String context,String newContext, String type,String finalType,String image);

    FreshNewsVo loadFreshNewInfo(HttpServletRequest request, Integer freshNewId, Integer pageNum, Integer pageSize);

    void addReadCount(Integer freshNewId);

    List<FreshNewsVo> loadFreshNewList(String type,Integer pageNum,Integer pageSize);

    void commentFreshNew(String type,String userId, Long freshNewsId, String commentariesContext) throws HomemadeException;

    String queryLike(String userId, Integer freshNewId);

    void freshNewsLike(String userId, Integer freshNewId);

    void addLikeCount(Integer freshNewId);

    FreshNews queryFreshNews(String userId, Integer freshNewId);

    void updateFreshNewsStatus(String userId, Integer freshNewId, String finalType);
}
