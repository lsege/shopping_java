package com.lsege.service.otherService;

import com.lsege.entity.otherVo.Alog;
import com.lsege.entity.otherVo.Recruitment;
import com.lsege.mapper.otherMapper.RecruitmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.apache.commons.codec.binary.Base64;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by zhanglei on 2017/10/13.
 */
@Service
public class RecruitmentServiceImpl implements  RecruitmentService{

    @Autowired
    RecruitmentMapper recruitmentMapper;


    @Override
    public List<Recruitment> getRecLsit(Integer type,Integer pageNum,Integer pageSize) {
        Integer startPage = (pageNum - 1) * pageSize;
        return recruitmentMapper.getRecLsit(type,startPage,pageSize);
    }

    @Override
    public Recruitment getRecDatial(Integer id) {
        Recruitment recruitment = recruitmentMapper.getRecDatial(id);
        return recruitment;
    }

    @Override
    @Transactional
    public void saveRecruitment(String userId, String type, String title, String context, String imageName) {
        if(imageName==null){
            imageName="";
        }
        String state = "1";
        Date date = new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String postedDate = f.format(date);
        //发布
        Recruitment recruitment = new Recruitment();
        recruitment.setType(Integer.parseInt(type));
        //单张图片地址
        String image = "";
        //多张图片地址
        String[] imageUrl;

        imageUrl = imageName.split(",");

        String html1= "<!DOCTYPE html>\n" +
                "\n" +
                "<html>\n" +
                "\n" +
                "\t<head>\n" +
                "\t\t\n" +
                "<meta charset=\"utf-8\">\n" +
                "\t\t<meta name=\"viewport\" content=\"width=device-width,height=device-height, minimum-scale=1.0, maximum-scale=1.0\">\n" +
                "\n" +
                "\t</head>\n" +
                "\t\n" +
                "<style>\n" +
                "\n" +
                "\t\timg { height: auto; width: auto\\9; width:100%; }\n" +
                "\t\t@media only screen and (min-width: 1200px){}\n" +
                "\t\t\n" +
                "media only screen and (min-width: 768px) and (max-width: 991px){}\n" +
                "\n" +
                "\t\t@media only screen and (max-width: 767px){}\n" +
                "\n" +
                "\t\t@media only screen and (-webkit-min-device-pixel-ratio: 1.3),only screen and (-o-min-device-pixel-ratio: 13/10),only screen and (min-resolution: 120dpi){}\n" +
                "\t</style>\n" +
                "\n" +
                "\t<body>\n" +
                "\n" +
                "\t\t<h4 style=\"padding-left:20px\">"+title+"</h4>\n" +
                "\t\t<span style=\"padding-left:20px\">"+context+"</span>\n" +
                "\t\t<table style=\"padding-top:20px;\">";
                                for(int i=0; i<imageUrl.length; i++){
                                    html1 += "<tr>\n" +
                                            "\t\t\t\t<td><img src='"+imageUrl[i]+"'/></td\n" +
                                            "\t\t\t</tr>" +
                                            "<tr><td style=\"height=20px\"></td></td>";
                                }
        String html2 = html1+"</table>"+
                            "</body>"+
                        "</html>";
        recruitment.setTitle(title);
        recruitment.setHtmlStr(html2);
        recruitment.setContext(context);
        recruitment.setState(Integer.parseInt(state));
        recruitment.setUserId(userId);
        recruitment.setPostedDate(postedDate);
        recruitment.setImage(imageName);
        recruitmentMapper.saveRecruitment(recruitment);
        //查询操作ID
        String operationId = recruitmentMapper.queryOperationId(userId,postedDate);
        //关注的字典值
        String aType = "1";
        //店铺的字典值
        String logClass = type;
        Alog alog = new Alog();
        alog.setUserId(userId);
        alog.setType(Integer.parseInt(aType));
        alog.setLogClass(Integer.parseInt(logClass));
        alog.setOperationId(Integer.parseInt(operationId));
        alog.setbDate(postedDate);
        recruitmentMapper.addLog(alog);
    }
}
