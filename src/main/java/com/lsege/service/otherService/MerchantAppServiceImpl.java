package com.lsege.service.otherService;

import com.lsege.entity.otherVo.DepositLogs;
import com.lsege.entity.otherVo.Invitation;
import com.lsege.entity.otherVo.MerchantAppOrderVo;
import com.lsege.entity.otherVo.Orders;
import com.lsege.entity.sys.MerchantUser;
import com.lsege.mapper.otherMapper.MerchantAppMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zhanglei on 2017/12/16.
 */
@Service
public class MerchantAppServiceImpl implements MerchantAppService{

    @Autowired
    MerchantAppMapper merchantAppMapper;
    @Override
    public List<MerchantAppOrderVo> getOrderList(String userId,Integer startNum,Integer pageSize) {
        return merchantAppMapper.getOrderList(userId,startNum,pageSize);
    }

    @Override
    public Integer getBanlance(String userId) {
        return merchantAppMapper.getBanlance(userId);
    }

    @Override
    public Integer depositMonery(String id) {
        return merchantAppMapper.depositMonery(id);
    }

    @Override
    public List<DepositLogs> depositLogsList(String userId,Integer pageNum,Integer pageSize) {
        Integer startPage = (pageNum - 1) * pageSize;
        return merchantAppMapper.depositLogsList(userId,startPage,pageSize);
    }

    @Override
    public Integer getUserById(String userId) {
        return merchantAppMapper.getUserById(userId);
    }

    @Override
    public void addInvitation(Invitation invitation) {
        merchantAppMapper.addInvitation(invitation);
    }

    @Override
    public Integer getMerchantById(Integer merchantId) {
        return merchantAppMapper.getMerchantById(merchantId);
    }

    @Override
    public List<Invitation> getInvitationList(Integer merchantId, Integer pageNum, Integer pageSize) {
        Integer startPage = (pageNum - 1) * pageSize;
        return merchantAppMapper.getInvitationList(merchantId,startPage,pageSize);
    }

    @Override
    public Invitation getInvitationInfo(Integer invitationId) {
        return merchantAppMapper.getInvitationInfo(invitationId);
    }

    @Override
    public Integer querySendCount(Integer merchantId) {
        return merchantAppMapper.querySendCount(merchantId);
    }
}
