package com.lsege.service.otherService;

import com.lsege.entity.Result;
import com.lsege.entity.otherVo.*;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.integration.ResultUtil;
import com.lsege.mapper.otherMapper.UserInfoMapper;
import com.lsege.util.DateUtil;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/10/14.
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    UserInfoMapper userInfoMapper;

    @Override
    public String querUserLevel(String userId) {
        return userInfoMapper.querUserLevel(userId);
    }

    @Override
    @Transactional
    public List<OrdersVo> queryUserOrders(String userId) {
        List<OrdersVo> list = userInfoMapper.queryUserOrders(userId);
        for (int i=0; i<list.size(); i++){
            Merchant merchant = userInfoMapper.queryByOrder(list.get(i).getMerchantId());
            list.get(i).setMerchant(merchant);
        }
        return list;
    }

    @Override
    @Transactional
    public List<AttentionVo> queryAttentionList(String userId) {
        List<AttentionVo> list = userInfoMapper.queryAttentionList(userId);
        for (int i=0; i<list.size(); i++){
            Merchant merchant = userInfoMapper.queryByOrder(list.get(i).getMerchantId());
            list.get(i).setMerchant(merchant);
        }
        return list;
    }

    @Override
    public void postedFeedback(String userId, String title, String context) {
        String state = "0";
        Date date = new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String feedbackDate = f.format(date);
        userInfoMapper.postedFeedback(userId,title,context,state,feedbackDate);
    }

    @Override
    public void changeUserInfo(String userId, String autograph, String name, String portrait, String sex, String birthday, String address, String phone) {
        Integer age=null;
        if(birthday!=null&&!birthday.equals("")&&!birthday.equals("请选择")){
            Integer year=Integer.valueOf(birthday.substring(0,4));
            Integer nowYear=Integer.valueOf(DateUtil.getYear());
            age=nowYear-year;
        }

        userInfoMapper.changeUserInfo(userId,autograph,name,portrait,sex,birthday,address,phone,age);
    }

    @Override
    @Transactional
    public List<ALogVo> loadALogList(String userId) {
        List<ALogVo> list = userInfoMapper.loadALogList(userId);
        final Base64 base64 = new Base64();
        for (int i=0; i<list.size(); i++){
            ALogVo aLogVo=list.get(i);
            if (aLogVo.getTypeName().equals("关注")){
                Merchant merchant = userInfoMapper.queryMerchant(aLogVo.getOperationId());
                aLogVo.setOperationStr("关注了店铺");
                aLogVo.setOperationId(merchant.getId().toString());
                aLogVo.setOperationName(merchant.getName());
                aLogVo.setOperationPic(merchant.getHomeImage());
                aLogVo.setMerchantType(merchant.getMerchantType());
            } else if (aLogVo.getTypeName().equals("发布")){
                if (aLogVo.getLogClassName().equals("婚恋") || aLogVo.getLogClassName().equals("求职招聘") || aLogVo.getLogClassName().equals("求购求售")){
                    Recruitment recruitment = userInfoMapper.queryRecruitment(aLogVo.getOperationId());
                    aLogVo.setOperationStr(aLogVo.getTypeName()+"了"+aLogVo.getLogClassName()+"信息");
                    aLogVo.setOperationName(recruitment.getTitle());
                    aLogVo.setOperationPic(recruitment.getImage());
                } else if (aLogVo.getLogClassName().equals("新鲜事") || aLogVo.getLogClassName().equals("求助")){

                    FreshNews freshNews = userInfoMapper.queryFreshNews(aLogVo.getOperationId());
                    aLogVo.setOperationStr(aLogVo.getTypeName()+"了"+aLogVo.getLogClassName());

                    String title = new String(base64.decode(freshNews.getTitle()));
                    aLogVo.setOperationName(title);
                    aLogVo.setFinalType(freshNews.getFinalType());
                }
            } else if (aLogVo.getTypeName().equals("评论")){
                if (aLogVo.getLogClassName().equals("新鲜事") || aLogVo.getLogClassName().equals("求助")){
                    FreshNewsCommentariesVo freshNewsCommentariesVo = userInfoMapper.queryCommentaries(aLogVo.getOperationId());

                    FreshNews freshNews=userInfoMapper.queryFreshNews(freshNewsCommentariesVo.getFreshNewsId());

                    aLogVo.setOperationStr(new String(base64.decode(freshNewsCommentariesVo.getCommentariesContext())));

                    String title = new String(base64.decode(freshNews.getTitle()));
                    aLogVo.setOperationName(title);
                    aLogVo.setOperationId(freshNews.getId()+"");
                }
            } else if (aLogVo.getTypeName().equals("分享")){

            }
        }
        return list;
    }

    @Override
    public void deleteALog(String userId, Integer logId) {
        userInfoMapper.deleteALog(userId,logId);
    }

    @Override
    public Integer queryUserPreferentialMoney(String userId) {
        return userInfoMapper.queryUserPreferentialMoney(userId);
    }

    @Override
    public List<HotWord> getHotWordList() {
        return userInfoMapper.getHotWordList();
    }

    @Override
    public String queryUserByCode(String code) {
        return userInfoMapper.queryUserByCode(code);
    }

    @Override
    public void addUserCodeCorrelation(String userId, String userCode, String beUserId) {
        //关联邀请码
        userInfoMapper.addUserCodeCorrelation(userId,userCode,beUserId);
        //增加VIP
        //查询两个用户VIP时间
        Date time1 = userInfoMapper.queryUserVipTime(userId);
        Date time2 = userInfoMapper.queryUserVipTime(beUserId);

        //格式化两个时间
        Long time11 = time1.getTime() ;
        Long time22 = time2.getTime() ;

        Date date = new Date();
        Long nowTime = date.getTime();
        Long newUser1VipTime = 0L;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (time11 > nowTime){
            //用户1VIP时间未到
            newUser1VipTime = time11 + (1000 * 60 * 60 * 24);
            userInfoMapper.editUserVipTime(userId,format.format(newUser1VipTime));
        } else {
            //用户1VIP时间已到
            newUser1VipTime = nowTime + (1000 * 60 * 60 * 24);
            userInfoMapper.editUserVipTime(userId,format.format(newUser1VipTime));
        }

        Long newUser2VipTime = 0L;
        if (time22 > nowTime){
            //用户1VIP时间未到
            newUser2VipTime = time22 + (1000 * 60 * 60 * 24);
            userInfoMapper.editUserVipTime(beUserId,format.format(newUser1VipTime));
        } else {
            //用户1VIP时间已到
            newUser2VipTime = nowTime + (1000 * 60 * 60 * 24);
            userInfoMapper.editUserVipTime(beUserId,format.format(newUser1VipTime));
        }
    }

    @Override
    public String queryUserInvitationCode(String userId) {
        return userInfoMapper.queryUserInvitationCode(userId);
    }

    public static String date2TimeStamp(String date_str,String format){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return String.valueOf(sdf.parse(date_str).getTime()/1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String queryMyUserCode(String id) {
        return userInfoMapper.queryMyUserCode(id);
    }

    @Override
    public Integer queryUserVipLevel(String userId) {
        Long date = userInfoMapper.queryUserVipLevel(userId);
        if (date > System.currentTimeMillis()){
            return 2;
        } else {
            return 1;
        }
    }

    @Override
    public Result loadEmergencyCall(Integer pageNum, Integer pageSize) {
        Integer startPage = (pageNum - 1) * pageSize;
        List<EmergencyCall> list = userInfoMapper.loadEmergencyCall(startPage,pageSize);
        return ResultUtil.success(list);
    }

    @Override
    public Result loadUserVipTimeOut(String id) {
        return ResultUtil.success(userInfoMapper.loadUserVipTimeOut(id));
    }
}