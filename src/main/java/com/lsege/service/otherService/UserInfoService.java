package com.lsege.service.otherService;

import com.lsege.entity.Result;
import com.lsege.entity.otherVo.ALogVo;
import com.lsege.entity.otherVo.AttentionVo;
import com.lsege.entity.otherVo.HotWord;
import com.lsege.entity.otherVo.OrdersVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2017/10/14.
 */
public interface UserInfoService {

    String querUserLevel(String userId);

    List<OrdersVo> queryUserOrders(String userId);

    List<AttentionVo> queryAttentionList(String userId);

    void postedFeedback(String userId, String title, String context);

    void changeUserInfo(String userId, String autograph, String name, String portrait, String sex, String birthday, String address, String phone);

    List<ALogVo> loadALogList(String userId);

    void deleteALog(String userId, Integer logId);

    Integer queryUserPreferentialMoney(String userId);

    List<HotWord> getHotWordList();

    String queryUserByCode(String code);

    void addUserCodeCorrelation(String userId, String userCode, String beUserId);

    String queryUserInvitationCode(@Param("userId") String userId);

    String queryMyUserCode(String id);

    Integer queryUserVipLevel(String userId);

    Result loadEmergencyCall(Integer pageNum, Integer pageSize);

    Result loadUserVipTimeOut(String id);
}
