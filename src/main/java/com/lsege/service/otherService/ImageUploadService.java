package com.lsege.service.otherService;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Administrator on 2017/11/29.
 */
public interface ImageUploadService {

    String updateHead(MultipartFile data, int i);
}
