package com.lsege.service.currencyService;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.util.StringUtil;
import com.lsege.controller.currencyController.TransferController;
import com.lsege.entity.JsonResult;
import com.lsege.util.wx.*;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lsd on 2017-09-16.
 */
@Service
public class WxPayServiceImpl implements WxPayService{

    private static final Logger LOG = Logger.getLogger(TransferController.class);


    private static final String TRANSFERS_PAY = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers"; // 企业付款
    private static final String TRANSFERS_PAY_QUERY = "https://api.mch.weixin.qq.com/mmpaymkttransfers/gettransferinfo"; // 企业付款查询
    private static final String APP_ID = ConfigUtil.getProperty("wx.appid");
    private static final String MCH_ID = ConfigUtil.getProperty("wx.mchid");
    private static final String API_SECRET = ConfigUtil.getProperty("wx.api.secret");
    private static final String desc = "企业向个人转账";

    @Override
    public JsonResult transferPay(String phone, Integer amount) {
        JsonResult json = new JsonResult();
//		String  openid = "oL54Awy1NsU4G1Gfu3WrWgdsbAv4";//通过手机号码查询openId;
        String  partner_trade_no = PayUtil.getTransferNo();
        String nonce_str = PayUtil.getNonceStr();
        Map<String, String> restmap = null;
        try {
            Map<String, String> parm = new HashMap<String, String>();
            parm.put("amount", String.valueOf(amount)); //转账金额
            parm.put("check_name", "NO_CHECK"); //校验用户姓名选项 OPTION_CHECK
            parm.put("desc",desc ); //企业付款描述信息
            parm.put("mch_appid", APP_ID); //公众账号appid
            parm.put("mchid", MCH_ID); //商户号
            parm.put("nonce_str", nonce_str); //随机字符串
            parm.put("openid", ""); //用户openid
            parm.put("partner_trade_no", partner_trade_no); //商户订单号
            parm.put("re_user_name", "安迪"); //check_name设置为FORCE_CHECK或OPTION_CHECK，则必填
            parm.put("spbill_create_ip", "121.207.227.236"); //服务器Ip地址
            parm.put("sign", PayUtil.getSign(parm, API_SECRET));
            String restxml = HttpUtils.posts(TRANSFERS_PAY, XmlUtil.xmlFormat(parm, false));
            restmap = XmlUtil.xmlParse(restxml);
            //将转账信息存入log表
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        if (CollectionUtil.isNotEmpty(restmap) && "SUCCESS".equals(restmap.get("result_code"))) {
            LOG.info("转账成功!!!");
            Map<String, String> transferMap = new HashMap<>();
            transferMap.put("partner_trade_no", restmap.get("partner_trade_no"));//商户转账订单号
            transferMap.put("payment_no", restmap.get("payment_no")); //微信订单号
            transferMap.put("payment_time", restmap.get("payment_time")); //微信支付成功时间
            LocalDateTime localDateTime = LocalDateTime.now();
//					WebUtil.response(response,
//							WebUtil.packJsonp(callback,
//									JSON.toJSONString(new WxJsonResult(1, "转账成功", new ResponseData(null, transferMap)),
//											SerializerFeatureUtil.FEATURES)));
            json.setData(transferMap);
            json.setSuccess(true);
        }else {
            if (CollectionUtil.isNotEmpty(restmap)) {
                LOG.info("转账失败：" + restmap.get("err_code") + ":" + restmap.get("err_code_des"));
            }
            LocalDateTime localDateTime = LocalDateTime.now();
            //responseData.setCont("转账失败：" + restmap.get("err_code") + ":" + restmap.get("err_code_des"));
//					WebUtil.response(response, WebUtil.packJsonp(callback, JSON
//							.toJSONString(new WxJsonResult(-1, "转账失败",responseData ), SerializerFeatureUtil.FEATURES)));
            json.setSuccess(false);
            json.setMessage("转账失败：" + restmap.get("err_code") + ":" + restmap.get("err_code_des"));
        }
        return json;
    }

    @Override
    public JsonResult orderPayQuery(HttpServletRequest request, HttpServletResponse response, String tradeno, String callback) {
        JsonResult json = new JsonResult();
        LOG.info("[/transfer/pay/query]");
        if (StringUtil.isEmpty(tradeno)) {
            WebUtil.response(response, WebUtil.packJsonp(callback, JSON
                    .toJSONString(new WxJsonResult(-1, "转账订单号不能为空", new ResponseData()), SerializerFeatureUtil.FEATURES)));
        }

        Map<String, String> restmap = null;
        try {
            Map<String, String> parm = new HashMap<String, String>();
            parm.put("appid", APP_ID);
            parm.put("mch_id", MCH_ID);
            parm.put("partner_trade_no", tradeno);
            parm.put("nonce_str", PayUtil.getNonceStr());
            parm.put("sign", PayUtil.getSign(parm, API_SECRET));
            String restxml = HttpUtils.posts(TRANSFERS_PAY_QUERY, XmlUtil.xmlFormat(parm, true));
            restmap = XmlUtil.xmlParse(restxml);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        if (CollectionUtil.isNotEmpty(restmap) && "SUCCESS".equals(restmap.get("result_code"))) {
            // 订单查询成功 处理业务逻辑
            LOG.info("订单查询：订单" + restmap.get("partner_trade_no") + "支付成功");
            Map<String, String> transferMap = new HashMap<>();
            transferMap.put("partner_trade_no", restmap.get("partner_trade_no"));//商户转账订单号
            transferMap.put("openid", restmap.get("openid")); //收款微信号
            transferMap.put("payment_amount", restmap.get("payment_amount")); //转账金额
            transferMap.put("transfer_time", restmap.get("transfer_time")); //转账时间
            transferMap.put("desc", restmap.get("desc")); //转账描述
            WebUtil.response(response, WebUtil.packJsonp(callback, JSON
                    .toJSONString(new WxJsonResult(1, "订单转账成功", new ResponseData(null, transferMap)), SerializerFeatureUtil.FEATURES)));
        }else {
            if (CollectionUtil.isNotEmpty(restmap)) {
                LOG.info("订单转账失败：" + restmap.get("err_code") + ":" + restmap.get("err_code_des"));
            }
            WebUtil.response(response, WebUtil.packJsonp(callback, JSON
                    .toJSONString(new WxJsonResult(-1, "订单转账失败", new ResponseData()), SerializerFeatureUtil.FEATURES)));
        }
        return json;
    }
}
