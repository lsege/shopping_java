package com.lsege.service.currencyService;

import com.lsege.entity.JsonResult;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by lsd on 2017-09-16.
 */
@Service
public interface WxPayService {

   JsonResult transferPay(String phone, Integer amount);

   JsonResult  orderPayQuery(HttpServletRequest request, HttpServletResponse response, String tradeno,
                             String callback);
}
