package com.lsege.service.currencyService;

import com.lsege.entity.JsonResult;
import com.lsege.entity.Result;
import com.lsege.entity.currency.CurrencyUser;
import com.lsege.entity.sys.MerchantUser;
import org.springframework.stereotype.Service;

import java.text.ParseException;

/**
 * Created by lsd on 2017-09-16.
 */

@Service
public interface RegisterService {

    Result customerRegister(CurrencyUser client)throws ParseException;

    Result getCode(String phone);

    Result customerLogin(String phone,String code);

    Result validationPhone(String code,String phone,String newPassword);

    String queryIdByPhone(String cuPhone);

    CurrencyUser getClientByAccountAndId(String subject, String issuer);

    Result merchantLogin(String password, String username);

    Result customerLoginTrue(String phone);

    MerchantUser getMerchantUserByNameAndId(String subject, String issuer);
}
