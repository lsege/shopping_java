package com.lsege.service.currencyService;

import com.lsege.entity.JsonResult;
import com.lsege.entity.PageData;
import com.lsege.entity.Result;
import com.lsege.entity.currency.CurrencyUser;
import com.lsege.entity.sys.MerchantUser;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import com.lsege.mapper.currencyMapper.RegisterMapper;
import com.lsege.util.ExpiryMap;
import com.lsege.util.RendSMS;
import com.lsege.util.TokenUtil;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.SimpleFormatter;

/**
 * Created by lsd on 2017-09-16.
 */
@Service
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    RegisterMapper registerMapper;


    @Override
    @Transactional
    public synchronized Result customerRegister(CurrencyUser client) throws ParseException {
        //根据手机号码查询验证码
        PageData pageData = registerMapper.getVerificationCode(client.getPhone());
        if (pageData != null) {
            String num = pageData.get("code").toString();
            if (num.equals(client.getCode())) {
                Date date = new Date();
                long l = date.getTime();
                String craeteTime = pageData.get("create_time").toString();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                long millionSeconds = sdf.parse(craeteTime).getTime();//毫秒

                if (l - millionSeconds < (1800000)) {
                    //判断要注册的用户是否存在
                    Integer cus = registerMapper.getClientPhone(client.getPhone());
                    if (cus == 0) {
                        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                        client.setId(uuid);
                        Date createTime = new Date();
                        SimpleDateFormat str = new SimpleDateFormat("yyyyMMdd");
                        String dateStr = str.format(createTime);
                        String maxCode = registerMapper.getMaxUserCode();

                        if (maxCode != null && maxCode != "") {
                            client.setUserCode((Integer.valueOf(maxCode) + 1) + "");
                            switch (maxCode.length() + 1) {
                                case 1:
                                    client.setName("用户0000" + (1 + Integer.valueOf(maxCode)));
                                    break;
                                case 2:
                                    client.setName("用户000" + (1 + Integer.valueOf(maxCode)));
                                    break;
                                case 3:
                                    client.setName("用户00" + (1 + Integer.valueOf(maxCode)));
                                    break;
                                case 4:
                                    client.setName("用户0" + (1 + Integer.valueOf(maxCode)));
                                    break;
                                case 5:
                                    client.setName("用户" + (1 + Integer.valueOf(maxCode)));
                                    break;
                            }
                        } else {
                            client.setUserCode(1 + "");
                            client.setName("用户00001");
                        }
                        client.setLevel(1);
                        client.setCreateDate(createTime);
                        client.setAccount(client.getPhone());
                        registerMapper.saveClient(client);
                        return ResultUtil.success(ResultEnum.SUCCESS);
                    } else {
                        return ResultUtil.error(ResultEnum.AMOUNT_EXISTENCE);
                    }
                } else {
                    return ResultUtil.error(ResultEnum.CODE_IS_OLD);
                }


            } else {
                return ResultUtil.error(ResultEnum.CODE_ERROR);
            }
        } else {
            return ResultUtil.error(ResultEnum.CODE_NOT_EXISTENCE);
        }
    }

    @Override
    public Result getCode(String phone) {
        JsonResult json = new JsonResult();
        RendSMS rendSMS = new RendSMS();
        //生成6位验证码
        String code = rendSMS.getRandNum(6);
        //友盟短信形式推送
        rendSMS.sms_api2(phone, code);
        // 判断用户是否申请过验证码
        Integer num = registerMapper.selectCodeNum(phone);
        Date createTime = new Date();
        if (num == 0) {
            //将验证码存入数据表，并且和手机号对应
            registerMapper.saveVerificationCode(phone, code, createTime);
        } else {
            //已经申请过验证码，在数据表中根据手机号码更新验证码

            registerMapper.updateCode(phone, code,createTime);

        }
        return ResultUtil.success(ResultEnum.SUCCESS);
    }

    @Override
    public Result customerLogin(String phone,String code) {
        PageData pageData = registerMapper.getVerificationCode(phone);
        try {
            if (pageData != null) {
                String num = pageData.get("code").toString();
                if (num.equals(code)) {
                    Date date = new Date();
                    long l = date.getTime();
                    String craeteTime = pageData.get("create_time").toString();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    long millionSeconds = sdf.parse(craeteTime).getTime();
                    if (millionSeconds-  l< (1000*60*3)) {
                        //通过验证,登陆
                        CurrencyUser currencyUser = registerMapper.customerLogin(phone);
                        if (currencyUser == null){
                            //尚未注册 先注册
                            CurrencyUser user = new CurrencyUser();
                            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                            user.setId(uuid);
                            Date createTime = new Date();
                            SimpleDateFormat str = new SimpleDateFormat("yyyyMMdd");
                            String dateStr = str.format(createTime);
                            String maxCode = registerMapper.getMaxUserCode();
                            if (maxCode != null && maxCode != "") {
                                user.setUserCode((Integer.valueOf(maxCode) + 1) + "");
                                switch (maxCode.length() + 1) {
                                    case 1:
                                        user.setName("用户0000" + (1 + Integer.valueOf(maxCode)));
                                        break;
                                    case 2:
                                        user.setName("用户000" + (1 + Integer.valueOf(maxCode)));
                                        break;
                                    case 3:
                                        user.setName("用户00" + (1 + Integer.valueOf(maxCode)));
                                        break;
                                    case 4:
                                        user.setName("用户0" + (1 + Integer.valueOf(maxCode)));
                                        break;
                                    case 5:
                                        user.setName("用户" + (1 + Integer.valueOf(maxCode)));
                                        break;
                                }
                            } else {
                                user.setUserCode(1 + "");
                                user.setName("用户00001");
                            }
                            user.setPhone(phone);
                            user.setLevel(1);
                            user.setCreateDate(createTime);
                            user.setAccount(phone);
                            user.setVipTimeout(new Date(System.currentTimeMillis()));
                            registerMapper.saveClient(user);
                            //再登陆
                            currencyUser = registerMapper.customerLogin(phone);
                            String userCode = currencyUser.getUserCode();
                            switch (userCode.length()) {
                                case 1:
                                    currencyUser.setUserCode("0000" + userCode);
                                    break;
                                case 2:
                                    currencyUser.setUserCode("000" + userCode);
                                    break;
                                case 3:
                                    currencyUser.setUserCode("00" + userCode);
                                    break;
                                case 4:
                                    currencyUser.setUserCode("0" + userCode);
                                    break;
                                case 5:
                                    currencyUser.setUserCode(userCode);
                                    break;
                            }
                            if(currencyUser.getVipTimeout() != null){
                                if(currencyUser.getVipTimeout().getTime()> new Date().getTime()){
                                    currencyUser.setLevel(2);
                                }
                            }
                            String token = TokenUtil.createNewToken(String.valueOf(currencyUser.getId()),currencyUser.getAccount());
                            currencyUser.setToken(token);
                            currencyUser.setTimestamp(System.currentTimeMillis() + "");
                            registerMapper.saveUserToken(currencyUser.getId(),token);
                            return ResultUtil.success(currencyUser);
                        } else {
                            //登陆
                            String userCode = currencyUser.getUserCode();
                            switch (userCode.length()) {
                                case 1:
                                    currencyUser.setUserCode("0000" + userCode);
                                    break;
                                case 2:
                                    currencyUser.setUserCode("000" + userCode);
                                    break;
                                case 3:
                                    currencyUser.setUserCode("00" + userCode);
                                    break;
                                case 4:
                                    currencyUser.setUserCode("0" + userCode);
                                    break;
                                case 5:
                                    currencyUser.setUserCode(userCode);
                                    break;
                            }
                            if(currencyUser.getVipTimeout() != null){
                                if(currencyUser.getVipTimeout().getTime()> new Date().getTime()){
                                    currencyUser.setLevel(2);
                                }
                            }
                            String token = TokenUtil.createNewToken(String.valueOf(currencyUser.getId()),currencyUser.getAccount());
                            currencyUser.setToken(token);
                            currencyUser.setTimestamp(System.currentTimeMillis() + "");
                            registerMapper.saveUserToken(currencyUser.getId(),token);
                            return ResultUtil.success(currencyUser);
                        }
                    } else {
                        //验证码超时
                        return ResultUtil.error(ResultEnum.CODE_IS_OLD);
                    }
                } else {
                    //验证码不正确
                    return ResultUtil.error(ResultEnum.CODE_ERROR);
                }
            } else {
                //获取验证码失败
                return ResultUtil.error(ResultEnum.CODE_ERROR);
            }
        } catch (Exception e){
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }
    }

    @Override
    public Result validationPhone(String code, String phone, String newPassword) {
        PageData pageData = registerMapper.getVerificationCode(phone);
        try {
            if (pageData != null) {
                String num = pageData.get("code").toString();
                if (num.equals(code)) {
                    Date date = new Date();
                    long l = date.getTime();
                    String craeteTime = pageData.get("create_time").toString();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    long millionSeconds = sdf.parse(craeteTime).getTime();
                    if (l - millionSeconds < (1800000)) {
                        //通过验证,修改密码
                        registerMapper.editPassword(phone,newPassword);
                        return ResultUtil.success(ResultEnum.SUCCESS);
                    } else {
                        //验证码超时
                        return ResultUtil.error(ResultEnum.CODE_IS_OLD);
                    }
                } else {
                    //验证码不正确
                    return ResultUtil.error(ResultEnum.CODE_ERROR);
                }
            } else {
                return ResultUtil.error(ResultEnum.CODE_ERROR);
            }
        } catch (Exception e){
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }
    }

    @Override
    public String queryIdByPhone(String cuPhone) {
        return registerMapper.queryIdByPhone(cuPhone);
    }

    @Override
    public CurrencyUser getClientByAccountAndId(String subject, String issuer) {
        return registerMapper.getClientByAccountAndId(subject,issuer);
    }

    @Override
    public Result merchantLogin(String password, String username) {
        MerchantUser merchantUser = registerMapper.getMerchantUser(username);
        if(merchantUser != null){
            if(merchantUser.getPassword().equals(password)){
                if(merchantUser.getState()==0){

                    String token = TokenUtil.createNewToken(String.valueOf(merchantUser.getId()),merchantUser.getUsername());
                    merchantUser.setToken(token);
                    registerMapper.saveMerchantToken(merchantUser.getId(),token);
                    return ResultUtil.success(merchantUser);

                }else{
                    return ResultUtil.error(ResultEnum.AMOUNT_FROZEN);
                }
            }else{
                return ResultUtil.error(ResultEnum.PASSWORD_ERROR);
            }
        }else{
            return ResultUtil.error(ResultEnum.MERCHANT_LOGIN_ERROR);
        }


    }

    @Override
    public Result customerLoginTrue(String phone) {
        CurrencyUser currencyUser = registerMapper.customerLogin(phone);
        String userCode = currencyUser.getUserCode();
        switch (userCode.length()) {
            case 1:
                currencyUser.setUserCode("0000" + userCode);
                break;
            case 2:
                currencyUser.setUserCode("000" + userCode);
                break;
            case 3:
                currencyUser.setUserCode("00" + userCode);
                break;
            case 4:
                currencyUser.setUserCode("0" + userCode);
                break;
            case 5:
                currencyUser.setUserCode(userCode);
                break;
        }
        if(currencyUser.getVipTimeout() != null){
            if(currencyUser.getVipTimeout().getTime()> new Date().getTime()){
                currencyUser.setLevel(2);
            }
        }
        String token = TokenUtil.createNewToken(String.valueOf(currencyUser.getId()),currencyUser.getAccount());
        currencyUser.setToken(token);
        currencyUser.setTimestamp(System.currentTimeMillis() + "");
        registerMapper.saveUserToken(currencyUser.getId(),token);
        return ResultUtil.success(currencyUser);
    }

    @Override
    public MerchantUser getMerchantUserByNameAndId(String subject, String issuer) {
        return registerMapper.getMerchantUserByNameAndId(subject,issuer);
    }
}
