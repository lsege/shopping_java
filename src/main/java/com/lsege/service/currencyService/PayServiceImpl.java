package com.lsege.service.currencyService;

import com.google.gson.Gson;
import com.lsege.entity.currency.PayPledge;
import com.lsege.entity.currency.PingppPaySuccessResult;
import com.lsege.mapper.currencyMapper.UserPledgeMapper;
//import com.pingplusplus.Pingpp;
//import com.pingplusplus.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/** * Created by liuze on 2017/7/7.
 */
@Service
@Transactional
public class PayServiceImpl implements PayService {

    @Autowired
    UserPledgeMapper userPledgeMapper;

    private String apiKey = "sk_live_n9KCy5OKWrbPPS0e909GWnPS";//Live Key

    private String privateKey =
            "MIICXAIBAAKBgQDHBFq1fTRgwdsYYHlFIM5mytd2Cw5AqJeKdUQKdg2sHsB0IviZ\n" +
                    "1Qm6XIICzrY0WD7BHT4B7NN+/23eVzeKaBtEtFV//sCDL41UNSqQxL76Dfzg3BKq\n" +
                    "DRMY9P+PxfgRuq/VHglyVRhUQT8cnTkEV7L0TdQHJp/zBc1yd7J1ENgJqQIDAQAB\n" +
                    "AoGAEYDsiXHNnYTvRCGYCOBtxtSoRpWKV118lozhwxnG8hByVMSOoNj/1gg01xJf\n" +
                    "LBNm0Y1E2t59a3oDq754ABBP9X820HCWwCcqW6Da7G+KaeIHqW8+q++FvufWtYca\n" +
                    "WOrJY1yWDLJFHFnBLGmaixYV5pASAGFVQv60fSeDs1geGkECQQDszXFUcNqFmaq0\n" +
                    "DFUO/NkFfrBSNoJcfhUsjuYLyLcZ63pprxOE2ckc1NktbH3xi9j3jJvomIn8KRab\n" +
                    "fME+H+ENAkEA1ya30ZvW8lLCxCh62HO4YAIIb+rvzxBgcr0r3hOjl/WllkRIxSZE\n" +
                    "IYFDIr5IMSr8g+k9+FDEWn9pt5MzCCEMDQJBALMDD8Rc1+UdaJ1ZoUeGP21W+2X7\n" +
                    "DQtsyQb5M9D+t+RhThXsox3QIaepBFDBbmgWZzukUQxBnDZCvXNnmRIkuVUCQDWR\n" +
                    "KUEh2NF6c42dOoC7xxL4Tpt21gAq5qiKmupu31NNUM3p8GhNj7b71PhIgfbj6fz5\n" +
                    "zHkPuuOBR5PyLUl/2N0CQD19rNFr0gmhpnaOSbtaBeQGLtmtRgJKLfG9tGWBYlPs\n" +
                    "xhpqVk6l9FjjWkbaaOn4DYloEZTgcl7mpyIFp7kfnHw=";
    private String id = "app_OSqP40TCSab5O4Su";
    private String currency = "cny";

//    /**
//     * 会员注册
//     *
//     * @param payPledge
//     * @return
//     */
//    @Transactional
//    @Override
//    public Charge userPledge(PayPledge payPledge) throws  Exception {
//        Pingpp.apiKey = apiKey;
//        Pingpp.privateKey = privateKey;
//        userPledgeMapper.insertUserPledgeOrder(payPledge);
//        Map<String, Object> chargeParams = new HashMap<>();
//        chargeParams.put("order_no", payPledge.getOrderNo());
////        chargeParams.put("amount", payPledge.getAmount());           //单位分
//        chargeParams.put("amount", "1");           //单位分
//        Map<String, String> app = new HashMap<>();
//        app.put("id", id);          //appId
//        chargeParams.put("app", app);
//        chargeParams.put("channel", payPledge.getChannel());          //alipay 支付宝  wx 微信
//        chargeParams.put("currency", currency);            //三位 ISO 货币代码，目前仅支持人民币  cny 。
//        chargeParams.put("client_ip", payPledge.getClientIp());    //发起支付请求客户端的 IPv4 地址，如: 127.0.0.1。
//        chargeParams.put("subject", "会员注册");   //商品的标题，该参数最长为 32 个 Unicode 字符
//        chargeParams.put("body",  "会员注册");         //商品的描述信息，该参数最长为 128 个 Unicode 字符
//        return Charge.create(chargeParams);
//    }

    @Transactional
    @Override
    public boolean payResultOk(String result) {
        Gson gson = new Gson();
        PingppPaySuccessResult successResult = gson.fromJson(result, PingppPaySuccessResult.class);
        PingppPaySuccessResult.DataEntity.ObjectEntity entity = successResult.getData().getObject();
        try {
            PayPledge payPledge = userPledgeMapper.findPayPledgeByOrderNo(entity.getOrder_no());
        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }
}