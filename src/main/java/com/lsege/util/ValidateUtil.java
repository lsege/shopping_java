package com.lsege.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by liuze on 2017/9/16.
 */
public class ValidateUtil {


    /**
     * 验证手机号码
     *
     * @param phone
     * @return
     */
    public static boolean isMobile(final String phone) {
        if (phone == null || phone.equals("")) return false;
        Pattern mPattern;
        Matcher mMatcher;
        mPattern = Pattern.compile("^[1][3,4,5,7,8][0-9]{9}$");
        mMatcher = mPattern.matcher(phone);
        return mMatcher.matches();
    }


    /**
     * 验证输入密码长度 (32位 MD5)
     *
     * @param password
     * @return
     */
    public static boolean isPassword(String password) {
        if (password == null || password.equals("")) return false;
        Pattern mPattern;
        Matcher mMatcher;   //(?![0-9]+$)(?![a-zA-Z]+$)
        mPattern = Pattern.compile("^[0-9A-Za-z]{32}$");
        mMatcher = mPattern.matcher(password);
        return mMatcher.matches();
    }


    /**
     * 验证昵称
     *
     * @param nickName
     * @return
     */
    public static boolean isNickName(String nickName) {
        if (nickName == null || nickName.equals("")) return false;
        Pattern mPattern;
        Matcher mMatcher;
        mPattern = Pattern.compile("^[0-9A-Za-z\\u4e00-\\u9fa5]{2,8}$");
        mMatcher = mPattern.matcher(nickName);
        return mMatcher.matches();
    }

    /**
     * 验证验证码有效性
     *
     * @param authCode
     * @return
     */
    public static boolean isAuthCode(String authCode) {
        if (authCode == null || authCode.equals("")) return false;
        Pattern mPattern;
        Matcher mMatcher;
        mPattern = Pattern.compile("^[0-9]{6}$");
        mMatcher = mPattern.matcher(authCode);
        return mMatcher.matches();
    }
}
