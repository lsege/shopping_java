package com.lsege.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by lsd on 2017-06-10.
 */
public class RendSMS {
    /**
     * 短信接口二，触发类模板短信接口，可以设置动态参数变量。适合：验证码、订单短信等。
     */
    public ArrayList sms_api2(String mob, String p1) {

        Map<String, String> para = new HashMap<String, String>();

        /**
         * 目标手机号码，多个以“,”分隔，一次性调用最多100个号码，示例：139********,138********
         */
        para.put("mob", mob);

        /**
         * 微米账号的接口UID
         */
        para.put("uid", "OnkpBCC55a1D");

        /**
         * 微米账号的接口密码
         */
        para.put("pas","8wrs24gh");

        /**
         * 接口返回类型：json、xml、txt。默认值为txt
         */
        para.put("type", "json");

        /**
         * 短信模板cid，通过微米后台创建，由在线客服审核。必须设置好短信签名，签名规范： <br>
         * 1、模板内容一定要带签名，签名放在模板内容的最前面；<br>
         * 2、签名格式：【***】，签名内容为三个汉字以上（包括三个）；<br>
         * 3、短信内容不允许双签名，即短信内容里只有一个“【】”<br>
         */
        para.put("cid", "ROP9cVHEUBGf");

        /**
         * 传入模板参数。<br>
         * <br>
         * 短信模板示例：<br>
         * 【微米】您的验证码是：%P%，%P%分钟内有效。如非您本人操作，可忽略本消息。<br>
         * <br>
         * 传入两个参数：<br>
         * p1：610912<br>
         * p2：3<br>
         * 最终发送内容：<br>
         * 【微米】您的验证码是：610912，3分钟内有效。如非您本人操作，可忽略本消息。
         */
        para.put("p1", p1);
        para.put("p2", "1");
        ArrayList list = new ArrayList();
        try {
//			System.out.println(HttpClientHelper.convertStreamToString(
//					HttpClientHelper.get("http://api.weimi.cc/2/sms/send.html",
//							para), "UTF-8"));

//			System.out.println(HttpClientHelper.convertStreamToString(
//					HttpClientHelper.post(
//							"http://api.weimi.cc/2/sms/send.html", para),
//					"UTF-8"));
            list.add(HttpClientHelper.convertStreamToString(
                    HttpClientHelper.post(
                            "http://api.weimi.cc/2/sms/send.html", para),
                    "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public String getRandNum(int charCount) {
        String charValue = "";
        for (int i = 0; i < charCount; i++) {
            char c = (char) (randomInt(0, 10) + '0');
            charValue += String.valueOf(c);
        }
        return charValue;
    }
    public int randomInt(int from, int to) {
        Random r = new Random();
        return from + r.nextInt(to - from);
    }
}
