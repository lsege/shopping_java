package com.lsege.util;

/**
 * Created by zhanglei on 2017/10/27.
 */
public enum  ErrorUtil {
    phone_error(1,"请输入有效手机号"),
    code_error(2,"请输入有效验证码"),
    password_error(3,"请输入有效密码");
    private Integer index;
    private String  msg;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    ErrorUtil(Integer index,String msg){
        this.index=index;
        this.msg=msg;
    }
}
