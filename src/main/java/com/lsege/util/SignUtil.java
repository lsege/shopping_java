package com.lsege.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.*;

/**
 * Created by xuzhongyao on 2017/5/19.
 */
public class SignUtil {

    public static String createSign(Map<String, String> params, boolean encode)
            throws UnsupportedEncodingException {
        Set<String> keysSet = params.keySet();
        Object[] keys = keysSet.toArray();
        Arrays.sort(keys);
        StringBuffer temp = new StringBuffer();
        boolean first = true;
        for (Object key : keys) {
            if (first) {
                first = false;
            } else {
                temp.append("&");
            }
            temp.append(key).append("=");
            Object value = params.get(key);
            String valueString = "";
            if (null != value) {
                valueString = String.valueOf(value);
            }
            if (encode) {
                temp.append(URLEncoder.encode(valueString, "UTF-8"));
            } else {
                temp.append(valueString);
            }
        }
        //return temp.toString();
        return DigestUtils.sha1Hex(temp.toString());
    }


    public  static String getSignForPay(String s){
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            byte[] buffer = s.getBytes("UTF-8");
            //获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdTemp = MessageDigest.getInstance("MD5");
            //使用指定的字节更新摘要
            mdTemp.update(buffer);
            //获得密文
            byte[] md = mdTemp.digest();
            //把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            return null;
        }

    }
    public  static String md5Hex(byte[] s){
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            byte[] buffer = s;
            //获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdTemp = MessageDigest.getInstance("MD5");
            //使用指定的字节更新摘要
            mdTemp.update(buffer);
            //获得密文
            byte[] md = mdTemp.digest();
            //把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            return null;
        }

    }
    public static void  main(String args[]){
        Integer z=50;
        System.out.println(Float.parseFloat(z+"")/100);
    }
    public static String createSign(Map<String, String> params)
            throws UnsupportedEncodingException {

        Map<String, String> tempMap = new HashMap<>();
        //排除 params 里的空值
        for (String key : params.keySet()) {
            String value = params.get(key);
            if (value == null || value.equals("")) {
                continue;
            }
            tempMap.put(key, value);
        }
        //生成签名结果

        //把数组所有元素排序，并按照“参数=参数值”的模式用“&”字符拼接成字符串
        List<String> keys = new ArrayList<>(tempMap.keySet());
        Collections.sort(keys);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = params.get(key);
            if (i == keys.size() - 1) {//拼接时，不包括最后一个&字符
                sb.append(key).append("=").append(value);
            } else {
                sb.append(key).append("=").append(value).append("&");
            }
        }

        System.out.println(sb.toString());

        //将拼接好的字符串 进行SHA1加密
        return DigestUtils.sha1Hex(sb.toString());
    }
}
