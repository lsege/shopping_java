package com.lsege.util;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * Created by liuze on 2017/9/17.
 */
public class TokenUtil {

    /**
     * 创建新token
     * @param userId
     */
    public static String createNewToken(String userId, String account){
        //获取当前时间
        //过期时间
        Date now = new Date(System.currentTimeMillis());
        //过期时间
        Date expiration = new Date(now.getTime() + 2592000000l);
        return Jwts
                .builder()
                .setSubject(account)
                .setIssuedAt(now)
                .setIssuer(userId)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS256, "Shop-Six")
                .compact();
    }

}
