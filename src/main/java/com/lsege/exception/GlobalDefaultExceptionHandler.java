package com.lsege.exception;

import com.lsege.entity.Result;
import com.lsege.integration.HomemadeException;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 创建人: 徐众垚
 * 创建时间: 2017/4/18
 * 公司: 唐山徕思歌科技有限公司
 * 描述: 全局异常处理
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler extends Throwable {

    private final static Logger logger = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result defaultErrorHandler(HttpServletRequest req, Exception e)  {
        if (e instanceof HomemadeException){
            HomemadeException homemadeException = (HomemadeException) e;
            return ResultUtil.error(homemadeException.getCode(),homemadeException.getMessage());
        }else if(e instanceof MyException){
            return ResultUtil.error(((MyException) e).getCode()+"",e.getMessage());
        } else {
            logger.info("【系统异常】={}",e.toString());
            if(e.toString().equals("java.lang.NullPointerException")){
                return ResultUtil.error(ResultEnum.NULL_ERROR);
            }else {
                return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
            }

        }
    }

}
