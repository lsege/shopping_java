package com.lsege.exception;


import com.lsege.integration.ResultEnum;

/**
 * 统一的异常类
 * Created by liuze on 2017/4/4.
 */
public class MyException extends RuntimeException{

    private Integer code;

    public MyException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = Integer.valueOf(resultEnum.getCode());
    }

    public MyException(Integer code , String msg) {
      super(msg);
      this.code=code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
