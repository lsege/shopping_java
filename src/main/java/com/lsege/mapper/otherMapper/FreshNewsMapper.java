package com.lsege.mapper.otherMapper;

import com.lsege.entity.otherVo.FreshNews;
import com.lsege.entity.otherVo.FreshNewsCommentariesVo;
import com.lsege.entity.otherVo.FreshNewsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/10/13.
 */
@Mapper
@Repository("/freshNewsMapper")
public interface FreshNewsMapper {

    void postedFreshNew(@Param("user_id") String userId,@Param("title") String title,@Param("context") String context,@Param("htmlContent") String newContext,
                        @Param("read_count") String readCount,@Param("like_count") String likeCount,
                        @Param("state") String state,@Param("type") String type,@Param("date") String date,
                        @Param("finalType") String finalType,@Param("image") String image);

    String queryId(@Param("user_id") String userId,@Param("date") String date);

    void addALog(@Param("user_id") String userId,@Param("operationType") String operationType,@Param("infoType") String infoType,@Param("id") String id,@Param("date") String date);

    FreshNewsVo loadFreshNewInfo(@Param("id") Integer freshNewId);

    List<FreshNewsCommentariesVo> loadFreshNewCommentaries(@Param("id") Integer freshNewId,@Param("startPage") Integer startPage,@Param("pageSize") Integer pageSize);

    String queryReadCount(@Param("freshNewId") Integer freshNewId);

    void editReadCount(@Param("freshNewId") Integer freshNewId,@Param("count") Integer count);

    List<FreshNewsVo> loadFreshNewList(@Param("type") String type,@Param("startPage") Integer startPage,@Param("pageSize") Integer pageSize);

    void commentFreshNew(@Param("userId") String userId,@Param("freshNewsId") Long freshNewsId,@Param("context") String commentariesContext,@Param("state") String state,@Param("date") String date);

    String queryCommentId(@Param("user_id") String userId,@Param("date") String date);

    String queryLike(@Param("user_id") String userId,@Param("freshNewId") Integer freshNewId);

    void addLike(@Param("user_id") String userId,@Param("freshNewId") Integer freshNewId,@Param("date") String date);

    String queryLikeCount(@Param("freshNewId") Integer freshNewId);

    void editLikeCount(@Param("freshNewId") Integer freshNewId,@Param("count") Integer count);

    String queryLikeId(@Param("user_id") String userId,@Param("date") String date);

    String queryUserIsLike(@Param("user_id") String userId,@Param("freshNewId") Integer freshNewId);

    FreshNews queryFreshNews(@Param("userId") String userId, @Param("freshNewId") Integer freshNewId);

    void updateFreshNewsStatus(@Param("userId") String userId,@Param("freshNewId") Integer freshNewId,@Param("finalType") String finalType);
}
