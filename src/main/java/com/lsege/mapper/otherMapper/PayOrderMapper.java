package com.lsege.mapper.otherMapper;

import com.lsege.entity.currency.CurrencyUser;
import com.lsege.entity.otherVo.DepositLogs;
import com.lsege.entity.otherVo.PayOrderVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by zhanglei on 2017/11/24.
 */
@Mapper
@Repository(value = "payOrderMapper")
public interface PayOrderMapper {


    @Insert("insert into payorder " +
            "(" +
            "amount,real_money,sign,userId,createDate,state,orderNumber,merchantId,payType,payFor" +
            ")values(" +
            "#{amount},#{realMoney},#{sign},#{userId},#{createDate},#{state},#{orderNumber},#{merchantId},#{payType},#{payFor})")
    void savePayOrder(PayOrderVo payOrderVo);

    @Select("select max(p.orderNumber) from payorder p where p.orderNumber like (\n" +
            "select * from \n" +
            "(SELECT DATE_FORMAT(NOW(), '%Y%m%d%') from DUAL)as t)")
    String getMaxOrderNumber();

    @Select("select * from payorder where orderNumber=#{orderNumber}")
    PayOrderVo getPayOrder(@Param("orderNumber") String orderNumber);

    @Update("update payorder set state=1  where orderNumber=#{transactionId}")
    void updateState(String transactionId);

    @Select("select * from user where id = #{userId}")
    CurrencyUser getUserByUserId(@Param("userId") String userId);

    @Update("update user set level = 2,vip_timeout = #{vipTimeout} where id = #{userId}")
    void changeVip(@Param("userId") String userId, @Param("vipTimeout") String vipTimeout);

    @Insert("insert into deposit_logs (type,state,info,create_time,out_money,merchant_user_id,logs_code,payee_account,payee_real_name) " +
            "values" +
            " (" +
            "#{type},#{state},#{info},#{createTime},#{outMoney},#{merchantUserId},#{logsCode}, #{payeeAccount},#{payeeRealName}" +
            ")")
    void saveLogs(DepositLogs depositLogs);

    @Update("update deposit_logs " +
            "set state = #{state}," +
            "info = #{info}" +
            "where logs_code = #{logsCode}")
    void updateDepositLogs(DepositLogs depositLogs);

    @Select("select day from vip_amount where amount = #{amount}")
    String getDay(@Param("amount") Integer amount);

    @Select("select * from deposit_logs where logs_code = #{transactionId}")
    DepositLogs getDepositLogsByLogsCode(String transactionId);


    @Select("select a.* from (\n" +
            "select d.*, TIMESTAMPDIFF(second,d.create_time,NOW()) as t\n" +
            " from deposit_logs d where d.state = 1 \n" +
            ")a")
    List<DepositLogs> getDepositLogsForWithDraw();

    @Update("update merchant set bag = #{newMoney} where id =(\n" +
            "SELECT merchant_id from merchant_user where id = #{merchantUserId}\n" +
            ")")
    void updateBag(@Param("merchantUserId")String merchantUserId, @Param("newMoney")Integer newMoney);

    @Select("SELECT UNIX_TIMESTAMP(vip_timeout) FROM USER WHERE id = #{userId}")
    Long queryUserVipTimeOut(String userId);

    @Update("UPDATE USER SET vip_timeout = #{time} WHERE id = #{userId}")
    void updateVipTimeOut(@Param("userId") String userId,@Param("time") String time);

    @Select("SELECT b.merchant_id\n" +
            "FROM deposit_logs AS a\n" +
            "LEFT JOIN merchant_user AS b ON a.merchant_user_id = b.id\n" +
            "WHERE a.logs_code = #{logsCode}")
    Integer queryMerchantIdByCode(String logsCode);
}
