package com.lsege.mapper.otherMapper;

import com.lsege.entity.otherVo.Alog;
import com.lsege.entity.otherVo.Recruitment;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by zhanglei on 2017/10/12.
 */
@Mapper
@Repository(value = "datingMapper")
public interface RecruitmentMapper {

    @Select("SELECT * FROM recruitment WHERE TYPE = #{type} ORDER BY posted_date DESC LIMIT #{startPage},#{pageSize}")
    List<Recruitment> getRecLsit(@Param("type")Integer type, @Param("startPage")Integer startPage, @Param("pageSize")Integer pageSize);

    @Select("select * from recruitment where id=#{id}")
    Recruitment getRecDatial(Integer id);

    @Insert("INSERT INTO recruitment" +
            "(" +
            "type,title,context,state,user_id,posted_date,image,html_str" +
            ") VALUES (" +
            "#{type},#{title},#{context},#{state},#{userId},#{postedDate},#{image},#{htmlStr}" +
            ")")
    void saveRecruitment(Recruitment recruitment);

    @Select("SELECT id  FROM recruitment WHERE user_id = #{userId} AND posted_date = #{postedDate}")
    String queryOperationId(@Param("userId") String userId,@Param("postedDate") String postedDate);

    @Insert("INSERT INTO alog " +
            "(" +
            "user_id,type,log_class,operation_id,b_date"+
            ") VALUES (" +
            "#{userId},#{type},#{logClass},#{operationId},#{bDate}"+
            ")")
    void addLog(Alog alog);
}
