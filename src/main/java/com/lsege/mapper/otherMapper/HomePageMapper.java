package com.lsege.mapper.otherMapper;

import com.lsege.entity.otherVo.News;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.entity.vo.ActivityPicture;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/10/12.
 */
@Mapper
@Repository(value = "homePageMapper")
public interface HomePageMapper {

    List<ActivityPicture> loadActivityPictures();

    List<News> loadNewsAndAdvList(@Param("startPage") Integer startPage,@Param("pageSize") Integer pageSize);

    List<Merchant> queryMerchantsByKeyWord(@Param("keyWord") String keyWord);
}
