package com.lsege.mapper.otherMapper;

import com.lsege.entity.otherVo.*;
import com.lsege.entity.shopVo.Merchant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/10/14.
 */
@Mapper
@Repository("/userInfoMapper")
public interface UserInfoMapper {

    String querUserLevel(@Param("user_id") String userId);

    List<OrdersVo> queryUserOrders(@Param("user_id") String userId);

    Merchant queryByOrder(@Param("merchant_id") String merchantId);

    List<AttentionVo> queryAttentionList(@Param("user_id") String userId);

    void postedFeedback(@Param("user_id") String userId,@Param("title") String title,@Param("context") String context,
                        @Param("state") String state,@Param("feedbackDate") String feedbackDate);

    void changeUserInfo(@Param("user_id") String userId,@Param("autograph") String autograph,@Param("name") String name,@Param("portrait") String portrait,
                        @Param("sex") String sex,@Param("birthday") String birthday,@Param("address") String address,@Param("phone") String phone,@Param("age") Integer age);

    List<ALogVo> loadALogList(@Param("user_id") String userId);

    FreshNews queryFreshNews(@Param("operation_id") String operationId);

    Merchant queryMerchant(@Param("operation_id") String operationId);

    Recruitment queryRecruitment(@Param("operation_id") String operationId);

    FreshNewsCommentariesVo queryCommentaries(@Param("operation_id")String operationId);

    void deleteALog(@Param("userId") String userId,@Param("logId") Integer logId);

    Integer queryUserPreferentialMoney(@Param("userId") String userId);

    List<HotWord> getHotWordList();

    String queryUserByCode(@Param("code") String code);

    void addUserCodeCorrelation(@Param("userId") String userId,@Param("userCode") String userCode,@Param("beUserId") String beUserId);

    Date queryUserVipTime(@Param("userId") String userId);

    void editUserVipTime(@Param("userId") String userId,@Param("newVipTime") String newUser1VipTime);

    String queryUserInvitationCode(@Param("userId") String userId);

    String queryMyUserCode(@Param("id") String id);

    Long queryUserVipLevel(@Param("userId") String userId);

    List<EmergencyCall> loadEmergencyCall(@Param("startPage") Integer startPage,@Param("pageSize") Integer pageSize);

    Long loadUserVipTimeOut(@Param("id") String id);
}
