package com.lsege.mapper.otherMapper;

import com.lsege.entity.otherVo.Activity;
import com.lsege.entity.otherVo.Invitation;
import com.lsege.entity.shopVo.Merchant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/10/12.
 */
@Mapper
@Repository("/activityMapper")
public interface ActivityMapper {

    List<Activity> loadAllActivity(@Param("startPage") Integer startPage,@Param("pageSize") Integer pageSize);

    Activity loadActivityInfo(@Param("id") Integer id);

    List<Merchant> loadRecommendStoreList(@Param("startPage") Integer startPage,@Param("pageSize") Integer pageSize);

    List<Invitation> getTabList(@Param("startPage")Integer startPage, @Param("pageSize")Integer pageSize, @Param("type")Integer type);


    Invitation getTabDetail(Integer id);

    Activity loadTabActivityInfo(@Param("id") Integer id);
}
