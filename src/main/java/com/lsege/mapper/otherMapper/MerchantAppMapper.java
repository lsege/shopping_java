package com.lsege.mapper.otherMapper;

import com.lsege.entity.otherVo.DepositLogs;
import com.lsege.entity.otherVo.Invitation;
import com.lsege.entity.otherVo.MerchantAppOrderVo;
import com.lsege.entity.otherVo.Orders;
import com.lsege.entity.sys.MerchantUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zhanglei on 2017/12/16.
 */
@Mapper
@Repository("merchantAppMapper")
public interface MerchantAppMapper {


    @Select("select p.amount,\n" +
            "       p.real_money,\n" +
            "       p.userId,\n" +
            "       p.createDate,\n" +
            "       p.state,\n" +
            "       p.orderNumber,\n" +
            "       p.merchantId,\n" +
            "       p.payType, \n" +
            "       u.name as username\n" +
            "from payorder  p \n" +
            "left join user u on u.id=p.userId\n" +
            "where p.merchantId = (\n" +
            "select merchant_id from merchant_user where id = #{userId})" +
            "  ORDER BY p.createDate DESC " +
            "limit #{startNum},#{pageSize}")
    List<MerchantAppOrderVo> getOrderList(@Param("userId") String userId,@Param("startNum") Integer startNum,@Param("pageSize") Integer pageSize);

    @Select("select bag from merchant where id = (\n" +
            "select merchant_id from merchant_user where id = #{userId})")
    Integer getBanlance(String userId);

    @Select("select  sum(out_money) from deposit_logs where merchant_user_id = #{id} and state = 1")
    Integer depositMonery(String id);

    @Select("SELECT * FROM deposit_logs WHERE merchant_user_id = #{userId} ORDER BY create_time DESC LIMIT #{startPage},#{pageSize}")
    List<DepositLogs> depositLogsList(@Param("userId") String userId,@Param("startPage") Integer startPage,@Param("pageSize") Integer pageSize);

    @Select("select merchant_id from merchant_user where id = #{userId}")
    Integer getUserById(String userId);

    @Insert("insert into invitation(" +
            "title,introduction,context,merchant_id,image,state,merchant_type,create_time" +
            ")values(" +
            "#{title},#{introduction},#{context},#{merchantId},#{image},#{state},#{merchantType},#{createTime}" +
            ")")
    void addInvitation(Invitation invitation);

    @Select("select merchant_type from merchant where id = #{merchantId}")
    Integer getMerchantById(Integer merchantId);

    @Select("SELECT * FROM invitation WHERE merchant_id = #{merchantId} ORDER BY create_time DESC LIMIT #{startPage},#{pageSize}")
    List<Invitation> getInvitationList(@Param("merchantId") Integer merchantId,@Param("startPage") Integer startPage,@Param("pageSize") Integer pageSize);

    @Select("SELECT * FROM invitation WHERE id = #{invitationId}")
    Invitation getInvitationInfo(Integer invitationId);

    @Select("SELECT COUNT(*) FROM invitation WHERE DATE_FORMAT(create_time, '%Y%m%d') = DATE_FORMAT(NOW(), '%Y%m%d') AND merchant_id = #{merchantId}")
    Integer querySendCount(Integer merchantId);
}
