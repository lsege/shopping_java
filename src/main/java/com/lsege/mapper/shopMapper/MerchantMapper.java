package com.lsege.mapper.shopMapper;

import com.lsege.entity.otherVo.Alog;
import com.lsege.entity.otherVo.Attention;
import com.lsege.entity.otherVo.Orders;
import com.lsege.entity.shopVo.Merchandise;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.entity.shopVo.RecommendDynamicsVo;
import com.lsege.entity.shopVo.RecommendStaticVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zhanglei on 2017/10/12.
 */
@Mapper
@Repository(value = "merchantMapper")
public interface MerchantMapper {

    @Select("   SELECT a.* FROM merchant AS a \n" +
            "left join (select * from (select * from invitation where merchant_type = #{type} ORDER BY id DESC) z  GROUP BY merchant_id) i on i.merchant_id =a.id \n" +
            "WHERE a.merchant_type= #{type} \n" +
            "ORDER BY i.id desc,a.is_recommend DESC,a.is_top DESC,a.start_date DESC LIMIT #{startPage},#{pageSize}")
    List<Merchant> getMerchantList(@Param("type")Integer type,@Param("startPage")Integer startPage,@Param("pageSize")Integer pageSize);

    @Select("SELECT * FROM merchandise WHERE merchant_id = #{merchantId} LIMIT #{startPage},#{pageSize}")
    List<Merchandise> getMerchandise(@Param("merchantId")Integer merchantId, @Param("startPage")Integer startPage, @Param("pageSize")Integer pageSize);
    @Select("select * from merchant where id=#{id}")
    Merchant getMerchant(Integer id);

    @Select("select id from attention where 1=1 AND user_id = #{userId} AND merchant_id = #{merchantId}")
    String queryAttent(@Param("userId") String userId,@Param("merchantId") Integer merchantId);

    @Insert("INSERT INTO attention \n" +
            "(\n" +
            "\tuser_id,\n" +
            "\tmerchant_id,\n" +
            "\tstate,\n" +
            "\tb_date\n" +
            ") VALUE (\n" +
            "\t#{userId},\n" +
            "\t#{merchantId},\n" +
            "\t#{state},\n" +
            "\t#{bDate}\n" +
            ")")
    void toAttentionMerchant(Attention attention);

    @Insert("" +
            "insert into alog (" +
            "user_id," +
            "type," +
            "log_class," +
            "operation_id," +
            "b_date" +
            ") values (" +
            "#{userId}," +
            "#{type}," +
            "#{logClass}," +
            "#{operationId}," +
            "#{bDate}" +
            ")")
    void addLog(Alog alog);

    @Delete("DELETE FROM attention WHERE user_id = #{userId} and merchant_id = #{merchantId}")
    void closeAttentionMerchant(@Param("userId") String userId,@Param("merchantId") Integer merchantId);

    @Select("SELECT id FROM alog WHERE user_id = #{userId} AND type = #{type} AND log_class = #{logClass} AND operation_id = #{id}")
    String queryALogId(@Param("userId") String userId,@Param("type") String type,@Param("logClass") String logClass,@Param("id") String id);

    @Delete("DELETE FROM alog WHERE id = #{id}")
    void deleteALog(@Param("id") String id);

    @Select("SELECT merchant_id,image_show_static FROM recommend_static WHERE style = #{type}")
    RecommendStaticVo queryRecommendStatic(@Param("type") String type);

    @Select("SELECT merchant_id,image_show_dynamics FROM recommend_dynamics WHERE style = #{type}")
    List<RecommendDynamicsVo> queryRecommendDynamics(String type);

    @Insert("INSERT INTO orders (\n" +
            "\tmerchant_id,\n" +
            "\tuser_id,\n" +
            "\tpay_date,\n" +
            "\tneed_money,\n" +
            "\tpay_money,\n" +
            "\tstate,\n" +
            "\tpreferential_money\n" +
            ") VALUES (\n" +
            "\t#{merchantId},\n" +
            "\t#{userId},\n" +
            "\t#{payDate},\n" +
            "\t#{needMoney},\n" +
            "\t#{payMoney},\n" +
            "\t#{state},\n" +
            "\t#{preferentialMoney}\n" +
            ")")
    void createOrder(Orders orders);

    @Select("SELECT bag FROM merchant WHERE id = #{merchantId}")
    Integer queryMerchantMoney(@Param("merchantId") String merchantId);

    @Update("UPDATE merchant SET bag = #{newMoney} WHERE id = #{merchantId}")
    void upDateMerchantMoney(@Param("merchantId") String merchantId,@Param("newMoney") Integer newMoney);

}
