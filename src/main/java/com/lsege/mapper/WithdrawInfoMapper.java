package com.lsege.mapper;

import com.lsege.entity.WithdrawInfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2018/3/22.
 */
@Mapper
@Repository(value = "withdrawInfoMapper")
public interface WithdrawInfoMapper {

    /**
     * 保存提现预设信息
     * @param withdrawInfo
     */
    @Insert("INSERT INTO withdraw_info (merchant_user,pay_account,real_name) VALUES (#{merchantUser},#{payAccount},#{realName})")
    void saveWithdrawInfo(WithdrawInfo withdrawInfo);

    /**
     * 验证提现记录是否属于当前用户
     * @param userId
     * @param withdrawInfoId
     * @return
     */
    @Select("SELECT id FROM withdraw_info WHERE id = #{withdrawInfoId} AND merchant_user = #{userId}")
    Integer queryWithdrawInfoId(@Param("userId") String userId,@Param("withdrawInfoId") Integer withdrawInfoId);

    /**
     * 删除提现预设信息
     * @param withdrawInfoId
     */
    @Delete("DELETE FROM withdraw_info WHERE id = #{withdrawInfoId}")
    void deleteWithdrawInfo(Integer withdrawInfoId);

    /**
     * 修改提现预设信息
     * @param withdrawInfo
     */
    @Update("UPDATE withdraw_info SET pay_account = #{payAccount},real_name = #{realName} WHERE id = #{id}")
    void updateWithdrawInfo(WithdrawInfo withdrawInfo);

    /**
     * 提现预设列表
     * @param userId
     * @return
     */
    @Select("SELECT * FROM withdraw_info WHERE merchant_user = #{userId}")
    List<WithdrawInfo> queryWithDrawInfo(String userId);
}
