package com.lsege.mapper.currencyMapper;

import com.lsege.entity.PageData;
import com.lsege.entity.currency.CurrencyUser;
import com.lsege.entity.sys.MerchantUser;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Created by lsd on 2017-09-16.
 */
@Mapper
@Repository(value = "registerMapper")
public interface RegisterMapper {

    /**
     * 获取验证码，进行匹配
     * @param phone
     * @return
     */
    @Select("select * from sms_send where phone=#{phone} ")
    PageData getVerificationCode(String phone);

    /**
     * 查询用户是否存在
     * @param phone
     * @return
     */
    @Select("SELECT COUNT(*) FROM user WHERE phone = #{phone}")
    Integer getClientPhone(String phone);

    /**
     * 将用户信息存入数据表
     * @param client
     */
    @Insert("INSERT INTO user (\n" +
            "id,account,password,user_code,level,portrait,name,autograph,sex,birthday,address,phone,age,create_date,vip_timeout" +
            ")VALUES(\n" +
            "#{id},#{account},#{password},#{userCode},#{level},#{portrait},#{name},#{autograph},#{sex},#{birthday},#{address}" +
            ",#{phone},#{age},#{createDate},#{vipTimeout}" +
            ")")
    void saveClient(CurrencyUser client);


    /**
     * 获得注册最大的流水号
     * @return
     */
    @Select("select max(user_code) from user ")
    String getMaxUserCode();

    /**
     * 根据手机号码查询是否申请过验证码
     * @param phone
     * @return
     */
    @Select("SELECT COUNT(*) FROM sms_send WHERE phone = #{phone}")
    Integer selectCodeNum(String phone);

    /**
     * 将验证码和手机号一一对应的存入数据表
     * @param phone
     * @param code
     * @param  createTime
     */
    @Insert("INSERT INTO sms_send (\n" +
            "phone,code,create_time" +
            ")VALUES(\n" +
            "#{phone},#{code},#{createTime}\n" +
            ")")
    @Options(useGeneratedKeys = false)
    void saveVerificationCode(@Param("phone") String phone, @Param("code") String code, @Param("createTime")Date createTime);

    /**
     * 更新验证码
     * @param phone
     * @param code
     */
    @Update("UPDATE sms_send SET \n" +
            "code = #{code}," +
            "create_time = #{createTime}\n" +
            "WHERE phone = #{phone}")
    void updateCode(@Param("phone")String phone,@Param("code")String code,@Param("createTime") Date createTime);

    /**
     * 根据手机号码查询所有的用户信息
     * @param phone
     * @return
     */
    @Select("SELECT * FROM user WHERE phone = #{phone}")
    CurrencyUser customerLogin(String phone);

    @Update("UPDATE user SET password = #{newPassword} WHERE phone = #{userId}")
    void editPassword(@Param("userId") String userId,@Param("newPassword") String newPassword);

    @Select("SELECT id FROM user WHERE account = #{cuPhone}")
    String queryIdByPhone(String cuPhone);

    @Update("update user set token = #{token} where id = #{id}")
    void saveUserToken(@Param("id") String id, @Param("token") String token);

    @Select("select * from user where account = #{subject} and id = #{issuer}")
    CurrencyUser getClientByAccountAndId(@Param("subject")String subject,@Param("issuer") String issuer);

    @Select("select * from merchant_user where username = #{username}")
    MerchantUser getMerchantUser(String username);

    @Update("update merchant_user set token = #{token}  where id = #{id}")
    void saveMerchantToken(@Param("id") String id, @Param("token") String token);

    @Select("select * from merchant_user where username = #{subject} and id = #{issuer}")
    MerchantUser getMerchantUserByNameAndId(@Param("subject")String subject, @Param("issuer")String issuer);
}
