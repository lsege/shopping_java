package com.lsege.mapper.currencyMapper;

import com.lsege.entity.currency.PayPledge;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;

/**
 * Created by liuze on 2017/7/7.
 */
@Mapper
public interface UserPledgeMapper {
    /**
     * 添加用户押金订单,并返回订单 Id
     * @param payPledge
     */
    @Select("")
    @Options(statementType= StatementType.CALLABLE )
    void insertUserPledgeOrder(PayPledge payPledge);


    /**
     * 通过orderNo查询 押金记录
     * @param order_no
     * @return
     */
    @Select("")
    PayPledge findPayPledgeByOrderNo(String order_no);
}
