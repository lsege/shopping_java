package com.lsege.intercept.config;

import com.lsege.intercept.DomainIntercept;
import com.lsege.intercept.TokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 创建人: 徐众垚
 * 创建时间: 2017/4/15
 * 公司: 唐山徕思歌科技有限公司
 * 描述:
 */
@Configuration
public class InterceptConfig extends WebMvcConfigurerAdapter {

    @Bean
    public HandlerInterceptor getMyInterceptor(){
        return new TokenInterceptor();
    }
    @Bean
    public HandlerInterceptor noTokenIntercept(){
        return new NoTokenIntercept();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        /*配置跨域拦截器*/
        registry.addInterceptor(new DomainIntercept())
                .addPathPatterns("/**");


        /*安全认证拦截器*/
        registry.addInterceptor(getMyInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/toLogin/customerRegister")
                .excludePathPatterns("/toLogin/getCode")
                .excludePathPatterns("/toLogin/getMsgCode")
                .excludePathPatterns("/toLogin/customerLogin")
                .excludePathPatterns("/toLogin/validationPhone")
                .excludePathPatterns("/toLogin/merchantLogin")
                .excludePathPatterns("/userInfo/getHotWordList")
                .excludePathPatterns("/beeCloudwebHook/test")
                .excludePathPatterns("/beeCloudwebHook/webHookVerify")

                .excludePathPatterns("/homePage/loadNewsAndAdvList")
                .excludePathPatterns("/homePage/loadActivityPictures")

                .excludePathPatterns("/merchant/getMerchantList")


                .excludePathPatterns("/recruitment/getRecList")
                .excludePathPatterns("/recruitment/getRecDatial")
                .excludePathPatterns("/homePage/queryMerchantKeyWord")
                .excludePathPatterns("/merchant/loadRecommendedMerchantList")
                .excludePathPatterns("/merchant/getMerchant")
                .excludePathPatterns("/activity/loadAllActivity")
                .excludePathPatterns("/activity/loadActivityInfo")

                .excludePathPatterns("/activity/getTabList")
                .excludePathPatterns("/activity/getTabDetail")
                .excludePathPatterns("/activity/loadRecommendStoreList")
                .excludePathPatterns("/freshNews/loadFreshNewList")

                .excludePathPatterns("/uploadImage")
                .excludePathPatterns("/freshNews/loadFreshNewInfo");
        registry.addInterceptor(noTokenIntercept())
                .addPathPatterns("/homePage/loadNewsAndAdvList")
                .addPathPatterns("/homePage/loadActivityPictures")
                .addPathPatterns("/merchant/getMerchantList")

                .addPathPatterns("/recruitment/getRecList")
                .addPathPatterns("/recruitment/getRecDatial")
                .addPathPatterns("/homePage/queryMerchantKeyWord")
                .addPathPatterns("/merchant/loadRecommendedMerchantList")
                .addPathPatterns("/merchant/getMerchant")
                .addPathPatterns("/activity/loadAllActivity")
                .addPathPatterns("/activity/loadActivityInfo")

                .addPathPatterns("/activity/getTabList")
                .addPathPatterns("/activity/loadRecommendStoreList")
                .addPathPatterns("/freshNews/loadFreshNewList")

                .addPathPatterns("/uploadImage")
                .addPathPatterns("/freshNews/loadFreshNewInfo");



        /*安全认证拦截器*/
//        registry.addInterceptor(new RequestIntercept())
//                .addPathPatterns("/**")
//                .excludePathPatterns("/toLogin");
//
//        /*操作权限拦截器*/
//        registry.addInterceptor(new UrlIntercept())
//                .addPathPatterns("/**")
//                .excludePathPatterns("/toLogin")
//                .excludePathPatterns("/getRedisToken");

        super.addInterceptors(registry);
    }

}
