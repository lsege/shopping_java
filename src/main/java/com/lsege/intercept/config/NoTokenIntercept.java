package com.lsege.intercept.config;

import com.lsege.entity.currency.CurrencyUser;
import com.lsege.exception.MyException;
import com.lsege.integration.ResultEnum;
import com.lsege.service.currencyService.RegisterService;
import com.lsege.util.SignUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhanglei on 2017/12/1.
 */
public class NoTokenIntercept implements HandlerInterceptor {

    @Autowired
    RegisterService registerService;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //其他请求获取头信息
        final String authHeader = request.getHeader("Shop-Token");

        try {
            //如果没有header信息
            if (authHeader == null || authHeader.trim().equals("")) {
                request.setAttribute("id","");
            }else{

                //获取jwt实体对象接口实例
                Claims claims =  Jwts.parser().setSigningKey("Shop-Six").parseClaimsJws(authHeader).getBody();

                if (claims==null){
                    throw new MyException(ResultEnum.TOKEN_ERROR);
                }
                //判断token是否过期
                if (claims.getExpiration().getTime()< System.currentTimeMillis()){
                    throw new MyException(ResultEnum.TOKEN_EXPIRATION);
                }

                //验证参数
                //创建params Map用于保存临时参数
                Map<String, String> params = new HashMap<>();
                String pSign = request.getParameter("sign");

                //遍历Enumeration集合 将 参数名和 参数体 放入Map中
                Enumeration<String> paraNames = request.getParameterNames();
                for (Enumeration<String> e = paraNames; e.hasMoreElements(); ) {
                    String thisName = e.nextElement();
                    String thisValue = request.getParameter(thisName);
                    if ("sign".equals(thisName)) {  //排除sign 参数
                        continue;
                    }
                    params.put(thisName, thisValue);
                }
                //判断参数是否为空
                if (!params.isEmpty()){
                    String sign = SignUtil.createSign(params);
                    System.out.println(sign);
                    //判断参数拼接成的字符串是否一致
                    if (!sign.equals(pSign)){
                        throw new MyException(ResultEnum.PARAMS_ERROR);
                    }
                }

                //获取token所在的用户对象
                CurrencyUser client = registerService.getClientByAccountAndId(claims.getSubject(),claims.getIssuer());

                //判断用户的Token是否正确
                if (client == null || client.getToken() == null || client.getToken().equals("") || !client.getToken().equals(authHeader)) {
                    throw new MyException(ResultEnum.TOKEN_ERROR);
                }
                //判断用户状态是否被封禁
                if (client.getFrozen() != 1) {
                    //把用户id set进去
                    request.setAttribute("id",client.getId());
                } else {

                    throw new MyException(ResultEnum.AMOUNT_FROZEN);
                }
            }



        } catch (MalformedJwtException | ExpiredJwtException e) {
            throw new MyException(ResultEnum.TOKEN_ERROR);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
