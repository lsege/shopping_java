package com.lsege.entity.otherVo;

import java.util.Date;

/**
 * Created by zhanglei on 2017/12/16.
 */
public class DepositLogs {

    private  Integer id;
    private  String type;//提现类型
    private  Integer state;//提现状况 1成功 0 失败
    private  String info;//
    private  Date    createTime;//
    private  String  createTimeStr;//
    private  String MerchantUserId;//
    private  String  logsCode;//
    private  Integer outMoney;
    private  String  payeeAccount;
    private  String  payeeRealName;

    public String getPayeeAccount() {
        return payeeAccount;
    }

    public void setPayeeAccount(String payeeAccount) {
        this.payeeAccount = payeeAccount;
    }

    public String getPayeeRealName() {
        return payeeRealName;
    }

    public void setPayeeRealName(String payeeRealName) {
        this.payeeRealName = payeeRealName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOutMoney() {
        return outMoney;
    }

    public void setOutMoney(Integer outMoney) {
        this.outMoney = outMoney;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public String getMerchantUserId() {
        return MerchantUserId;
    }

    public void setMerchantUserId(String merchantUserId) {
        MerchantUserId = merchantUserId;
    }

    public String getLogsCode() {
        return logsCode;
    }

    public void setLogsCode(String logsCode) {
        this.logsCode = logsCode;
    }
}
