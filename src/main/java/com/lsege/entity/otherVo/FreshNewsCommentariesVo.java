package com.lsege.entity.otherVo;

/**
 * Created by Administrator on 2017/10/13.
 */
public class FreshNewsCommentariesVo {
    private String userHeadPortrait;
    private String freshNewsId;
    private String userName;

    public String getFreshNewsId() {
        return freshNewsId;
    }

    public void setFreshNewsId(String freshNewsId) {
        this.freshNewsId = freshNewsId;
    }

    private String commentariesContext;
    private String state;
    private String commentariesDate;

    public String getUserHeadPortrait() {
        return userHeadPortrait;
    }

    public void setUserHeadPortrait(String userHeadPortrait) {
        this.userHeadPortrait = userHeadPortrait;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCommentariesContext() {
        return commentariesContext;
    }

    public void setCommentariesContext(String commentariesContext) {
        this.commentariesContext = commentariesContext;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCommentariesDate() {
        return commentariesDate;
    }

    public void setCommentariesDate(String commentariesDate) {
        this.commentariesDate = commentariesDate;
    }
}
