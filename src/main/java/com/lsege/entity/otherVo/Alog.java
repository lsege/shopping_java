package com.lsege.entity.otherVo;

/**
 * Created by zhanglei on 2017/10/13.
 */
public class Alog {

    private Integer id;
    private String  userId;
    private Integer  type;
    private Integer  logClass;
    private Integer  operationId;
    private String bDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getLogClass() {
        return logClass;
    }

    public void setLogClass(Integer logClass) {
        this.logClass = logClass;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public String getbDate() {
        return bDate;
    }

    public void setbDate(String bDate) {
        this.bDate = bDate;
    }
}
