package com.lsege.entity.otherVo;

import java.util.Date;

/**
 * Created by Administrator on 2017/10/14.
 */
public class ALogVo {
    private Long id;
    private String userId;
    private Integer type;
    private Integer logClass;
    private String operationId;


    //新增
    private String  typeName;
    private String  logClassName;
    private Date    bDate;
    private String  operationName;
    private String  operationPic;

    private Integer merchantType;

    private String finalType;

    public Integer getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(Integer merchantType) {
        this.merchantType = merchantType;
    }

    public String getOperationStr() {
        return operationStr;
    }

    public void setOperationStr(String operationStr) {
        this.operationStr = operationStr;
    }

    private String  operationStr;

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public String getOperationPic() {
        return operationPic;
    }

    public void setOperationPic(String operationPic) {
        this.operationPic = operationPic;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getLogClassName() {
        return logClassName;
    }

    public void setLogClassName(String logClassName) {
        this.logClassName = logClassName;
    }

    public Date getbDate() {
        return bDate;
    }

    public void setbDate(Date bDate) {
        this.bDate = bDate;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getLogClass() {
        return logClass;
    }

    public void setLogClass(Integer logClass) {
        this.logClass = logClass;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getFinalType() {
        return finalType;
    }

    public void setFinalType(String finalType) {
        this.finalType = finalType;
    }
}
