package com.lsege.entity.otherVo;

/**
 * Created by Administrator on 2017/10/23.
 */
public class Orders {
    private Long id;
    private String merchantId;
    private String userId;
    private String payDate;
    private String needMoney;
    private String payMoney;
    private String state;
    private String preferentialMoney;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getNeedMoney() {
        return needMoney;
    }

    public void setNeedMoney(String needMoney) {
        this.needMoney = needMoney;
    }

    public String getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(String payMoney) {
        this.payMoney = payMoney;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPreferentialMoney() {
        return preferentialMoney;
    }

    public void setPreferentialMoney(String preferentialMoney) {
        this.preferentialMoney = preferentialMoney;
    }
}
