package com.lsege.entity.otherVo;

import java.util.List;

/**
 * Created by Administrator on 2017/10/13.
 */
public class FreshNewsVo {
    private Long id;
    private String userHeadPortrait;
    private String userName;
    private String title;
    private String context;
    private String readCount;
    private String likeCount;
    private String state;
    private String type;
    private String postedDate;
    private String finalType;
    private String isLike;
    private List<FreshNewsCommentariesVo> freshNewsCommentariesVos;
    private String imageUrl;
    private String htmlContent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserHeadPortrait() {
        return userHeadPortrait;
    }

    public void setUserHeadPortrait(String userHeadPortrait) {
        this.userHeadPortrait = userHeadPortrait;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getReadCount() {
        return readCount;
    }

    public void setReadCount(String readCount) {
        this.readCount = readCount;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getFinalType() {
        return finalType;
    }

    public void setFinalType(String finalType) {
        this.finalType = finalType;
    }

    public String getIsLike() {
        return isLike;
    }

    public void setIsLike(String isLike) {
        this.isLike = isLike;
    }

    public List<FreshNewsCommentariesVo> getFreshNewsCommentariesVos() {
        return freshNewsCommentariesVos;
    }

    public void setFreshNewsCommentariesVos(List<FreshNewsCommentariesVo> freshNewsCommentariesVos) {
        this.freshNewsCommentariesVos = freshNewsCommentariesVos;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }
}
