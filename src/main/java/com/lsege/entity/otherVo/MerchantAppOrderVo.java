package com.lsege.entity.otherVo;

import java.util.Date;

/**
 * Created by zhanglei on 2017/12/16.
 */
public class MerchantAppOrderVo {

    private  Integer amount;//实付金额
    private  Integer realMoney;//折扣前金额
    private  String  userId;//购买用户的id
    private  String  userName;//姓名
    private  Date    createDate;//订单创建时间
    private  String  dateStr;
    private  Integer state;//支付状态
    private  String  orderNumber;//交易订单号
    private  Integer payType;//支付方式

    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(Integer realMoney) {
        this.realMoney = realMoney;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }
}
