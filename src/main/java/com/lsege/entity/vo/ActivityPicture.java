package com.lsege.entity.vo;

/**
 * Created by Administrator on 2017/10/12.
 */
public class ActivityPicture {
    private Long id;
    private String image;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
