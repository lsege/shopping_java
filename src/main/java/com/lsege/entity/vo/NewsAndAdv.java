package com.lsege.entity.vo;

import com.lsege.entity.otherVo.Advertising;
import com.lsege.entity.otherVo.News;

/**
 * Created by Administrator on 2017/10/12.
 */
public class NewsAndAdv {
    private News aNew;
    private Advertising advertising;

    public News getaNews() {
        return aNew;
    }

    public void setaNew(News aNew) {
        this.aNew = aNew;
    }

    public Advertising getAdvertising() {
        return advertising;
    }

    public void setAdvertising(Advertising advertising) {
        this.advertising = advertising;
    }
}
