package com.lsege.entity.vo;

/**
 * Created by xuzhongyao on 2017/6/17.
 */
public class SocketDataDep {

    private Object data;
    private String description;

    public SocketDataDep(Object data, String description) {
        this.data = data;
        this.description = description;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
