package com.lsege.entity.vo;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by xuzhongyao on 2017/6/12.
 */
public class SocketInfo implements Serializable {
    private Long sId;
    private String sHex;
    private String sCode;
    private String sSuccess;
    private String sDescribe;
    private String sPk;
    private Integer sType;//0收 1发
    private Timestamp time;

    public SocketInfo() {
    }

    public SocketInfo(Long sId, String sHex, String sCode, String sSuccess) {
        this.sId = sId;
        this.sHex = sHex;
        this.sCode = sCode;
        this.sSuccess = sSuccess;
    }

    public Long getsId() {
        return sId;
    }

    public void setsId(Long sId) {
        this.sId = sId;
    }

    public String getsHex() {
        return sHex;
    }

    public void setsHex(String sHex) {
        this.sHex = sHex;
    }

    public String getsCode() {
        return sCode;
    }

    public void setsCode(String sCode) {
        this.sCode = sCode;
    }

    public String getsSuccess() {
        return sSuccess;
    }

    public void setsSuccess(String sSuccess) {
        this.sSuccess = sSuccess;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getsDescribe() {
        return sDescribe;
    }

    public void setsDescribe(String sDescribe) {
        this.sDescribe = sDescribe;
    }

    public String getsPk() {
        return sPk;
    }

    public void setsPk(String sPk) {
        this.sPk = sPk;
    }

    public Integer getsType() {
        return sType;
    }

    public void setsType(Integer sType) {
        this.sType = sType;
    }
}
