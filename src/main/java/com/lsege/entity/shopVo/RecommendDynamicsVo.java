package com.lsege.entity.shopVo;

/**
 * Created by Administrator on 2017/10/20.
 */
public class RecommendDynamicsVo {
    private Long id;
    private String merchantId;
    private String imageShowDynamics;
    private String imageTitle;
    private String content;
    private String style;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getImageShowDynamics() {
        return imageShowDynamics;
    }

    public void setImageShowDynamics(String imageShowDynamics) {
        this.imageShowDynamics = imageShowDynamics;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
