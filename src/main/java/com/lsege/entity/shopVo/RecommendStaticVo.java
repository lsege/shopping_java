package com.lsege.entity.shopVo;

/**
 * Created by Administrator on 2017/10/20.
 */
public class RecommendStaticVo {
    private String merchantId;
    private String imageShowStatic;
    private String imageTitle;
    private String content;
    private String style;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getImageShowStatic() {
        return imageShowStatic;
    }

    public void setImageShowStatic(String imageShowStatic) {
        this.imageShowStatic = imageShowStatic;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
