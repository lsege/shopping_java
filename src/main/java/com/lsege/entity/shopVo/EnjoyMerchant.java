package com.lsege.entity.shopVo;

import java.util.List;

/**
 * Created by Administrator on 2017/10/20.
 */
public class EnjoyMerchant {
    private List<Merchant> merchants;
    private List<Merchant> dynamicsMerchant;
    private Merchant merchant;

    public List<Merchant> getMerchants() {
        return merchants;
    }

    public void setMerchants(List<Merchant> merchants) {
        this.merchants = merchants;
    }

    public List<Merchant> getDynamicsMerchant() {
        return dynamicsMerchant;
    }

    public void setDynamicsMerchant(List<Merchant> dynamicsMerchant) {
        this.dynamicsMerchant = dynamicsMerchant;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }
}
