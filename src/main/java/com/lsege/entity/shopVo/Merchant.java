package com.lsege.entity.shopVo;

import java.util.Date;
import java.util.List;

/**
 * Created by zhanglei on 2017/10/12.
 */
public class Merchant {

    private Integer id;
    private String name;
    private String profile;
    private String startDate;
    private String businessHours;
    private String phone;
    private Double discount;
    private String address;
    private Integer merchantType;
    private Integer isDelivery;
    private Integer isRecommend;
    private Integer isTop;
    private String keyword;
    private String homeImage;
    private String dishesImage;
    private Double vipDiscount;
    private Integer bag;

    public Integer getBag() {
        return bag;
    }

    public void setBag(Integer bag) {
        this.bag = bag;
    }

    public String getDishesImage() {
        return dishesImage;
    }

    public void setDishesImage(String dishesImage) {
        this.dishesImage = dishesImage;
    }

    public Double getVipDiscount() {
        return vipDiscount;
    }

    public void setVipDiscount(Double vipDiscount) {
        this.vipDiscount = vipDiscount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(String businessHours) {
        this.businessHours = businessHours;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(Integer merchantType) {
        this.merchantType = merchantType;
    }

    public Integer getIsDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(Integer isDelivery) {
        this.isDelivery = isDelivery;
    }

    public Integer getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(Integer isRecommend) {
        this.isRecommend = isRecommend;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getHomeImage() {
        return homeImage;
    }

    public void setHomeImage(String homeImage) {
        this.homeImage = homeImage;
    }

    @Override
    public String toString() {
        return "Merchant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", profile='" + profile + '\'' +
                ", startDate='" + startDate + '\'' +
                ", businessHours='" + businessHours + '\'' +
                ", phone='" + phone + '\'' +
                ", discount=" + discount +
                ", address='" + address + '\'' +
                ", merchantType=" + merchantType +
                ", isDelivery=" + isDelivery +
                ", isRecommend=" + isRecommend +
                ", isTop=" + isTop +
                ", keyword='" + keyword + '\'' +
                ", homeImage='" + homeImage + '\'' +
                '}';
    }
}
