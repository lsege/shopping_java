package com.lsege.entity.currency;

import java.util.List;

/**
 * Created by liuze on 2017/7/7.
 */
public class PingppPaySuccessResult {


    /**
     * request : iar_qH4y1KbTy5eLGm1uHSTS00s
     * livemode : true
     * data : {"object":{"order_no":"123456789","time_expire":1440407501,"metadata":{},"livemode":true,"subject":"Your Subject","channel":"upacp","description":null,"body":"Your Body","failure_msg":null,"refunds":{"data":[],"has_more":false,"url":"/v1/charges/ch_Xsr7u35O3m1Gw4ed2ODmi4Lw/refunds","object":"list"},"amount_refunded":0,"time_settle":null,"time_paid":1440407501,"credential":{},"extra":{},"refunded":false,"client_ip":"127.0.0.1","currency":"cny","id":"ch_Xsr7u35O3m1Gw4ed2ODmi4Lw","app":"app_urj1WLzvzfTK0OuL","amount":100,"failure_code":null,"created":1440407501,"amount_settle":0,"paid":true,"transaction_no":"1224524301201505066067849274","object":"charge"}}
     * created : 1440407501
     * id : evt_ugB6x3K43D16wXCcqbplWAJo
     * type : charge.succeeded
     * pending_webhooks : 0
     * object : event
     */
    private String request;
    private boolean livemode;
    private DataEntity data;
    private int created;
    private String id;
    private String type;
    private int pending_webhooks;
    private String object;

    public void setRequest(String request) {
        this.request = request;
    }

    public void setLivemode(boolean livemode) {
        this.livemode = livemode;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPending_webhooks(int pending_webhooks) {
        this.pending_webhooks = pending_webhooks;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getRequest() {
        return request;
    }

    public boolean isLivemode() {
        return livemode;
    }

    public DataEntity getData() {
        return data;
    }

    public int getCreated() {
        return created;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public int getPending_webhooks() {
        return pending_webhooks;
    }

    public String getObject() {
        return object;
    }

    public class DataEntity {
        /**
         * object : {"order_no":"123456789","time_expire":1440407501,"metadata":{},"livemode":true,"subject":"Your Subject","channel":"upacp","description":null,"body":"Your Body","failure_msg":null,"refunds":{"data":[],"has_more":false,"url":"/v1/charges/ch_Xsr7u35O3m1Gw4ed2ODmi4Lw/refunds","object":"list"},"amount_refunded":0,"time_settle":null,"time_paid":1440407501,"credential":{},"extra":{},"refunded":false,"client_ip":"127.0.0.1","currency":"cny","id":"ch_Xsr7u35O3m1Gw4ed2ODmi4Lw","app":"app_urj1WLzvzfTK0OuL","amount":100,"failure_code":null,"created":1440407501,"amount_settle":0,"paid":true,"transaction_no":"1224524301201505066067849274","object":"charge"}
         */
        private ObjectEntity object;

        public void setObject(ObjectEntity object) {
            this.object = object;
        }

        public ObjectEntity getObject() {
            return object;
        }

        public class ObjectEntity {
            /**
             * order_no : 123456789
             * time_expire : 1440407501
             * metadata : {}
             * livemode : true
             * subject : Your Subject
             * channel : upacp
             * description : null
             * body : Your Body
             * failure_msg : null
             * refunds : {"data":[],"has_more":false,"url":"/v1/charges/ch_Xsr7u35O3m1Gw4ed2ODmi4Lw/refunds","object":"list"}
             * amount_refunded : 0
             * time_settle : null
             * time_paid : 1440407501
             * credential : {}
             * extra : {}
             * refunded : false
             * client_ip : 127.0.0.1
             * currency : cny
             * id : ch_Xsr7u35O3m1Gw4ed2ODmi4Lw
             * app : app_urj1WLzvzfTK0OuL
             * amount : 100
             * failure_code : null
             * created : 1440407501
             * amount_settle : 0
             * paid : true
             * transaction_no : 1224524301201505066067849274
             * object : charge
             */
            private String order_no;
            private int time_expire;
            private MetadataEntity metadata;
            private boolean livemode;
            private String subject;
            private String channel;
            private String description;
            private String body;
            private String failure_msg;
            private RefundsEntity refunds;
            private int amount_refunded;
            private String time_settle;
            private int time_paid;
            private CredentialEntity credential;
            private ExtraEntity extra;
            private boolean refunded;
            private String client_ip;
            private String currency;
            private String id;
            private String app;
            private int amount;
            private String failure_code;
            private int created;
            private int amount_settle;
            private boolean paid;
            private String transaction_no;
            private String object;

            public void setOrder_no(String order_no) {
                this.order_no = order_no;
            }

            public void setTime_expire(int time_expire) {
                this.time_expire = time_expire;
            }

            public void setMetadata(MetadataEntity metadata) {
                this.metadata = metadata;
            }

            public void setLivemode(boolean livemode) {
                this.livemode = livemode;
            }

            public void setSubject(String subject) {
                this.subject = subject;
            }

            public void setChannel(String channel) {
                this.channel = channel;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public void setBody(String body) {
                this.body = body;
            }

            public void setFailure_msg(String failure_msg) {
                this.failure_msg = failure_msg;
            }

            public void setRefunds(RefundsEntity refunds) {
                this.refunds = refunds;
            }

            public void setAmount_refunded(int amount_refunded) {
                this.amount_refunded = amount_refunded;
            }

            public void setTime_settle(String time_settle) {
                this.time_settle = time_settle;
            }

            public void setTime_paid(int time_paid) {
                this.time_paid = time_paid;
            }

            public void setCredential(CredentialEntity credential) {
                this.credential = credential;
            }

            public void setExtra(ExtraEntity extra) {
                this.extra = extra;
            }

            public void setRefunded(boolean refunded) {
                this.refunded = refunded;
            }

            public void setClient_ip(String client_ip) {
                this.client_ip = client_ip;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public void setId(String id) {
                this.id = id;
            }

            public void setApp(String app) {
                this.app = app;
            }

            public void setAmount(int amount) {
                this.amount = amount;
            }

            public void setFailure_code(String failure_code) {
                this.failure_code = failure_code;
            }

            public void setCreated(int created) {
                this.created = created;
            }

            public void setAmount_settle(int amount_settle) {
                this.amount_settle = amount_settle;
            }

            public void setPaid(boolean paid) {
                this.paid = paid;
            }

            public void setTransaction_no(String transaction_no) {
                this.transaction_no = transaction_no;
            }

            public void setObject(String object) {
                this.object = object;
            }

            public String getOrder_no() {
                return order_no;
            }

            public int getTime_expire() {
                return time_expire;
            }

            public MetadataEntity getMetadata() {
                return metadata;
            }

            public boolean isLivemode() {
                return livemode;
            }

            public String getSubject() {
                return subject;
            }

            public String getChannel() {
                return channel;
            }

            public String getDescription() {
                return description;
            }

            public String getBody() {
                return body;
            }

            public String getFailure_msg() {
                return failure_msg;
            }

            public RefundsEntity getRefunds() {
                return refunds;
            }

            public int getAmount_refunded() {
                return amount_refunded;
            }

            public String getTime_settle() {
                return time_settle;
            }

            public int getTime_paid() {
                return time_paid;
            }

            public CredentialEntity getCredential() {
                return credential;
            }

            public ExtraEntity getExtra() {
                return extra;
            }

            public boolean isRefunded() {
                return refunded;
            }

            public String getClient_ip() {
                return client_ip;
            }

            public String getCurrency() {
                return currency;
            }

            public String getId() {
                return id;
            }

            public String getApp() {
                return app;
            }

            public int getAmount() {
                return amount;
            }

            public String getFailure_code() {
                return failure_code;
            }

            public int getCreated() {
                return created;
            }

            public int getAmount_settle() {
                return amount_settle;
            }

            public boolean isPaid() {
                return paid;
            }

            public String getTransaction_no() {
                return transaction_no;
            }

            public String getObject() {
                return object;
            }

            public class MetadataEntity {
            }

            public class RefundsEntity {
                /**
                 * data : []
                 * has_more : false
                 * url : /v1/charges/ch_Xsr7u35O3m1Gw4ed2ODmi4Lw/refunds
                 * object : list
                 */
                private List<?> data;
                private boolean has_more;
                private String url;
                private String object;

                public void setData(List<?> data) {
                    this.data = data;
                }

                public void setHas_more(boolean has_more) {
                    this.has_more = has_more;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public void setObject(String object) {
                    this.object = object;
                }

                public List<?> getData() {
                    return data;
                }

                public boolean isHas_more() {
                    return has_more;
                }

                public String getUrl() {
                    return url;
                }

                public String getObject() {
                    return object;
                }
            }

            public class CredentialEntity {
            }

            public class ExtraEntity {
            }
        }
    }
}
