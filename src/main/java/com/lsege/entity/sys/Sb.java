package com.lsege.entity.sys;

/**
 * Created by xuzhongyao on 2017/7/4.
 */
public class Sb {

    private Long sbId;
    private String sbName;
    private String sbAddress;
    private String sbSim;
    private int sbHeartInterval;
    private String sbLatLng;

    public Long getSbId() {
        return sbId;
    }

    public void setSbId(Long sbId) {
        this.sbId = sbId;
    }

    public String getSbName() {
        return sbName;
    }

    public void setSbName(String sbName) {
        this.sbName = sbName;
    }

    public String getSbAddress() {
        return sbAddress;
    }

    public void setSbAddress(String sbAddress) {
        this.sbAddress = sbAddress;
    }

    public String getSbSim() {
        return sbSim;
    }

    public void setSbSim(String sbSim) {
        this.sbSim = sbSim;
    }

    public int getSbHeartInterval() {
        return sbHeartInterval;
    }

    public void setSbHeartInterval(int sbHeartInterval) {
        this.sbHeartInterval = sbHeartInterval;
    }

    public String getSbLatLng() {
        return sbLatLng;
    }

    public void setSbLatLng(String sbLatLng) {
        this.sbLatLng = sbLatLng;
    }
}
