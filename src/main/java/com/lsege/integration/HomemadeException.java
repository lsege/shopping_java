package com.lsege.integration;

/**创建异常类（继承RuntimeException）
 * 创建人:刘俊峰
 * 时  间：2017-10-26
 */
public class HomemadeException extends RuntimeException {

    private String code;

    public HomemadeException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
