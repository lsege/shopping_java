package com.lsege.integration;


import com.lsege.entity.Result;

/**返回结果结构整合
 * 创建人:刘俊峰
 * 时  间：2017-10-26
 */
public class ResultUtil {

    public static Result success(Object object){
        Result result = new Result();
        result.setData(object);
        result.setMessage("操作成功");
        result.setCode("200");
        return result;
    }

    public static Result success(){
        Result result = new Result();
        result.setMessage("操作成功");
        result.setCode("200");
        return result;
    }

    public static Result error(String code,String msg){
        Result result = new Result();
        result.setMessage(msg);
        result.setCode(code);
        return result;
    }

    public static Result error(ResultEnum resultEnum){
        Result result = new Result();
        result.setMessage(resultEnum.getMsg());
        result.setCode(resultEnum.getCode());
        return result;
    }
}
