package com.lsege.integration;

/**
 * Created by lsd on 2017-10-27.
 */
public enum ResultEnum {
    UNKONWM_ERROR("100","未知错误"),
    NULL_ERROR("101","参数为空"),
    FOLLOW_ERROR("102","已被关注"),
    LIKECOUNT_ERROR("103","已经被赞"),
    IMAGE_ERROR("104","图片为空"),
    PHONE_ERROR("105","请输入有效手机号"),
    AMOUNT_ERROR("106","用户名不存在请先注册"),
    AMOUNT_FROZEN("107","该用户已被冻结"),
    PASSWORD_ERROR("108","密码错误，再次输入"),
    AMOUNT_EXISTENCE("109","账号已存在"),
    CODE_ERROR("110","验证码错误"),
    CODE_NOT_EXISTENCE("111","图片为空"),
    NOT_ATTENT_MERCHANT("112","尚未关注店铺"),
    CODE_IS_OLD("113","验证码已过期"),
    THIS_NOT_YOURS("114","这不是你发布的求助"),
    THIS_IS_FINALLY("115","求助已被解决"),
    THIS_IS_USETED("116","该手机号已经注册，请登陆"),

    THIS_IS_ERROR("117","暂不支持输入发布表情"),
    IS_NOT_SEND_COUNT("118","已无发布次数"),


    TOKEN_NOT_FOUND("1010","未找到Token信息" ),
    TOKEN_ERROR("1011","Token验证失败"),
    TOKEN_EXPIRATION("1013","Token已过期" ),
    PARAMS_ERROR("1014","参数验证失败" ),

    MONERY_ERROR("1015","支付金额不符"),


    MONERY_NOT_ENOUGH("1015","余额不足"),


    MERCHANT_LOGIN_ERROR("1020","账户不存在"),
    IS_NOT_REPEAT_ASSOCIATED("1021","不可重复关联"),
    IS_NOT_YOURS("1033","删除失败"),

    SUCCESS("200","操作成功"),
    ;

    private String code;
    private String msg;

    ResultEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
