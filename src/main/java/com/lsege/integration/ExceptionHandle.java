//package com.lsege.integration;
//
//import com.lsege.entity.Result;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//
///**异常捕获
// * 创建人:刘俊峰
// * 时  间：2017-10-26
// */
//@ControllerAdvice
//public class ExceptionHandle {
//
//    private final static Logger logger = LoggerFactory.getLogger(ExceptionHandle.class);
//
//    @ExceptionHandler(value = Exception.class)
//    @ResponseBody
//    public Result handle(Exception e){
//        if (e instanceof HomemadeException){
//            HomemadeException homemadeException = (HomemadeException) e;
//            return ResultUtil.error(homemadeException.getCode(),homemadeException.getMessage());
//        }else {
//            logger.info("【系统异常】={}",e);
//            return ResultUtil.error("-1","未知错误");
//        }
//    }
//
//}
