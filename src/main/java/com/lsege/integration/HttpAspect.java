package com.lsege.integration;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**处理文件
 * 创建人:刘俊峰
 * 时  间：2017-10-26
 */
@Aspect//处理文件标签
@Component//引用到容器
public class HttpAspect {

    private final static Logger logger = LoggerFactory.getLogger(HttpAspect.class);

    @Pointcut("execution(public * com.lsege.controller.*.*.*(..))")//面向切面
    public void log(){
    }

    @Before("log()")//在方法执行之前打印日志
    public void doBefore(JoinPoint joinPoint){

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if(attributes!=null){
            HttpServletRequest request = attributes.getRequest();
            //url
            logger.info("url={}",request.getRequestURL());
            //method
            logger.info("method={}",request.getMethod());
            //ip
//        logger.info("ip={}",request.getRemoteAddr());
            //类方法
            logger.info("class_method={}",joinPoint.getSignature().getDeclaringTypeName()+"."+ joinPoint.getSignature().getName());
            //参数
            logger.info("args={}",joinPoint.getArgs());
        }

    }

    @After("log())")//在方法执行之后打印日志
    public void doAfter(){
//        logger.info("222222");
    }

    @AfterReturning(returning = "obj",pointcut = "log()")
    public void doAfterReturning(Object obj){
        logger.info("response={}",obj);
    }

}
