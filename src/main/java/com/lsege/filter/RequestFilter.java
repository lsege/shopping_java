package com.lsege.filter;



import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * 创建人: 徐众垚
 * 创建时间: 2017/4/21
 * 公司: 唐山徕思歌科技有限公司
 * 描述:
 */
@WebFilter(filterName="requestFilter",urlPatterns="/*")
public class RequestFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
