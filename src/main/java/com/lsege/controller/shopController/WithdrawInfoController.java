package com.lsege.controller.shopController;

import com.lsege.entity.Result;
import com.lsege.entity.WithdrawInfo;
import com.lsege.service.shopService.WithdrawInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 提现信息控制器
 * Created by Administrator on 2018/3/22.
 */
@RestController
@RequestMapping(value = "/withdrawInfo")
public class WithdrawInfoController {

    @Autowired
    WithdrawInfoService withdrawInfoService;

    /**
     * 保存提现信息
     * @return
     */
    @PostMapping("/saveWithdrawInfo")
    public Result saveWithdrawInfo(HttpServletRequest request,String payAccount,String realName){
        return withdrawInfoService.saveWithdrawInfo(request,payAccount,realName);
    }

    /**
     * 删除提现预设信息
     * @param request
     * @param withdrawInfoId
     * @return
     */
    @PostMapping("/deleteWithdrawInfo")
    public Result deleteWithdrawInfo(HttpServletRequest request,Integer withdrawInfoId){
        return withdrawInfoService.deleteWithdrawInfo(request,withdrawInfoId);
    }

    /**
     * 修改提现预设信息
     * @param request
     * @param withdrawInfo
     * @return
     */
    @PostMapping("/updateWithdrawInfo")
    public Result updateWithdrawInfo(HttpServletRequest request,WithdrawInfo withdrawInfo){
        return withdrawInfoService.updateWithdrawInfo(request,withdrawInfo);
    }

    /**
     * 读取提现预设信息
     * @param request
     * @return
     */
    @GetMapping("/queryWithDrawInfo")
    public Result queryWithDrawInfo(HttpServletRequest request){
        return withdrawInfoService.queryWithDrawInfo(request);
    }
}
