package com.lsege.controller.shopController;

import com.lsege.entity.JsonResult;
import com.lsege.entity.Result;
import com.lsege.entity.shopVo.Merchandise;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.entity.shopVo.RecommendDynamicsVo;
import com.lsege.entity.shopVo.RecommendStaticVo;
import com.lsege.exception.GlobalDefaultExceptionHandler;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import com.lsege.service.shopService.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhanglei on 2017/10/12.
 */
@RestController
@RequestMapping(value = "/merchant")
public class MerchantController {

    @Autowired
    MerchantService merchantService;


    /**
     * 查询所有店铺
     *
     * @param  type（1 餐饮，2 不锈钢）
     * @param pageNum 当前页数
     * @param pageSize 每页多少个
     * @return
     */
    @PostMapping("/getMerchantList")
    public Result<Merchant> getMerchantList(Integer type, Integer pageNum, Integer pageSize) throws Exception{
        List<Merchant> list=merchantService.getMerchantList(type,pageNum,pageSize);
        return ResultUtil.success(list);
    }

    /**
     * 通过id 查询店铺的商品
     *
     * @param merchantId
     * @param pageNum 当前页数
     * @param pageSize 每页多少个
     * @return
     */
    @PostMapping("/getMerchant")
    public Result getMerchant(Integer merchantId,Integer pageNum,Integer pageSize,HttpServletRequest request) throws Exception {
        Map<String ,Object> map=new HashMap<>();
        List<Merchandise>  merchandises=merchantService.getMerchandise(merchantId,pageNum,pageSize);
        Merchant merchant=merchantService.getMerchant(merchantId);
        try{
            String userId=request.getAttribute("id").toString();
            if (userId == null || userId.equals("")){
                map.put("userAttentState",0);
            } else {
                String id = merchantService.queryAttent(userId,merchantId);
                if (id == null || id.equals("")){
                    map.put("userAttentState",0);
                } else {
                    map.put("userAttentState",1);
                }
            }
        }catch (NullPointerException e){
            map.put("userAttentState",0);
        }

        map.put("businessHours",merchant.getBusinessHours());
        map.put("phone",merchant.getPhone());
        map.put("list",merchandises);
        map.put("discount",merchant.getDiscount());
        map.put("vipDiscount",merchant.getVipDiscount());
        map.put("address",merchant.getAddress());
        map.put("dishesImage",merchant.getDishesImage());
        map.put("profile",merchant.getProfile());
        return ResultUtil.success(map);
    }

    /**
     * 关注店铺，生成动态日志
     * @param request
     * @param merchantId
     * @return
     */
    @PostMapping("/toAttentionMerchant")
    @Transactional
    public Result toAttentionMerchant(HttpServletRequest request,Integer merchantId) throws GlobalDefaultExceptionHandler{
        String userId=request.getAttribute("id").toString();
        String id = merchantService.queryAttent(userId,merchantId);
        if (id == null || id.equals("")){
            merchantService.toAttentionMerchant(userId,merchantId);
            return ResultUtil.success(ResultEnum.SUCCESS);
        } else {
            return ResultUtil.error(ResultEnum.FOLLOW_ERROR);
        }
    }

    /**
     * 取消关注店铺 （不删除动态信息）
     * @param request
     * @param merchantId
     * @return
     */
    @PostMapping("/closeAttentionMerchant")
    @Transactional
    public Result closeAttentionMerchant(HttpServletRequest request,Integer merchantId){
        String userId=request.getAttribute("id").toString();
        String id = merchantService.queryAttent(userId,merchantId);
        if (id == null || id.length() == 0){
            return ResultUtil.success(ResultEnum.NOT_ATTENT_MERCHANT);
        } else {
        //取消关注
        merchantService.closeAttentionMerchant(userId,merchantId);
        //删除动态
        String type = "2";
        String logClass = "6";
        String aid = merchantService.queryALogId(userId,type,logClass,id);
        merchantService.deleteALog(aid);
            return ResultUtil.success(ResultEnum.SUCCESS);
        }
    }

    /**
     * 获取猜你喜欢店铺图片列表
     * @param type
     * @return
     */
    @PostMapping("/loadRecommendedMerchantList")
    public Result loadRecommendedMerchantList(String type){
        Map<String,Object> map = new HashMap<>();
        try {
            //静态单图
            RecommendStaticVo recommend = merchantService.queryRecommendStatic(type);
            //动态列表
            List<RecommendDynamicsVo> list = merchantService.queryRecommendDynamics(type);
            map.put("staticImage",recommend);
            map.put("dynamicImage",list);
            return ResultUtil.success(map);
        } catch (Exception e){
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.FOLLOW_ERROR);

        }
    }

    /**
     * 付款完成之后，生成订单信息
     * @param merchantId
     * @param request
     * @param needMoney
     * @param payMoney
     * @return
     */
    @PostMapping("/createOrder")
    public Result createOrder(String merchantId,HttpServletRequest request,Integer needMoney,Integer payMoney){
        String userId=request.getAttribute("id").toString();
        //生成订单信息
        merchantService.createOrder(merchantId,userId,needMoney,payMoney);
        //生成店铺钱包
        merchantService.upDate(merchantId,payMoney);
        return ResultUtil.success(ResultEnum.SUCCESS);
    }
}
