package com.lsege.controller.currencyController;

import com.lsege.entity.otherVo.DepositLogs;
import com.lsege.service.otherService.MerchantAppService;
import com.lsege.service.otherService.PayOrderService;
import com.lsege.service.shopService.MerchantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Created by zhanglei on 2018/2/5.
 */
@Component
@Configuration
@EnableScheduling
public class SchedulingConfig {
    private static final Logger logger = LoggerFactory.getLogger(SchedulingConfig.class);
    @Autowired
    PayOrderService payOrderService;
    @Autowired
    MerchantService merchantService;
    @Autowired
    MerchantAppService merchantAppService;


    /**
     * 定时器用来处理  提现订单
     */
    @Scheduled(fixedRate = 1000 * 60) // 每24小时执行一次
    @Transactional
    public void aliWithDraw() throws Exception {
        logger.info("开始处理提现订单");

        List<DepositLogs> depositLogs = payOrderService.getDepositLogsForWithDraw();
        for (DepositLogs depositLogs1 : depositLogs) {

            Integer banlance = merchantAppService.getBanlance(depositLogs1.getMerchantUserId());//钱包总金额
            if (banlance != null) {
                    //进入提现方法
                    DepositLogs d = PayController.aliWithDrawStaticMethod(
                            depositLogs1.getPayeeAccount(), depositLogs1.getOutMoney(),
                            depositLogs1.getPayeeRealName(), depositLogs1.getLogsCode());

                    /**更新提现记录*/
                    payOrderService.updateDepositLogs(d);

                    Integer oldMoney = banlance;
                    Integer outMoney = depositLogs1.getOutMoney();

                    if (d.getState() != 2){

                        Integer newMoney = oldMoney+outMoney;
                        payOrderService.updateBag(depositLogs1.getMerchantUserId(), newMoney);
                    }

            }


        }
        ;


    }

}
