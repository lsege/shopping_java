package com.lsege.controller.currencyController;

import com.lsege.entity.JsonResult;
import com.lsege.entity.Result;
import com.lsege.entity.currency.CurrencyUser;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import com.lsege.service.currencyService.RegisterService;
import com.lsege.util.ErrorUtil;
import com.lsege.util.TokenUtil;
import com.lsege.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;


/**
 * 创建人: 刘俊峰
 * 创建时间: 2017/10/09
 * 公司: 唐山徕思歌科技有限公司
 * 描述: 通用接口整合
 */
@RestController
@RequestMapping(value = "/toLogin")
public class RegisterController {
    @Autowired
    RegisterService registerService;

    /**
     * 用户注册
     * @param client
     * @return
     */
    @PostMapping("customerRegister")
    public Result customerRegister(CurrencyUser client)throws ParseException{
        if(!ValidateUtil.isMobile(client.getPhone())){
            return ResultUtil.error(ResultEnum.PHONE_ERROR);
        }else if(!ValidateUtil.isAuthCode(client.getCode())){
            return ResultUtil.error(ResultEnum.CODE_ERROR);
        } else{
            return registerService.customerRegister(client);
        }
    }

    /**
     * 获取验证码短信
     * @param cuPhone
     * @return
     */
    @PostMapping("getCode")
    public synchronized Result getCode(String cuPhone,String type){
        if (type.equals("1")){
            //注册
            String id = registerService.queryIdByPhone(cuPhone);
            if (id == null){
                //第一次注册
                if(!ValidateUtil.isMobile(cuPhone)){
                    return  ResultUtil.error(ResultEnum.PHONE_ERROR);
                }else{
                    return registerService.getCode(cuPhone);
                }
            } else {
                //已经注册过
                return ResultUtil.error(ResultEnum.THIS_IS_USETED);
            }
        } else if (type.equals("2")){
            //修改密码
            if(!ValidateUtil.isMobile(cuPhone)){
                return  ResultUtil.error(ResultEnum.PHONE_ERROR);
            }else{
                return registerService.getCode(cuPhone);
            }
        } else {
            return  ResultUtil.error(ResultEnum.NULL_ERROR);
        }
    }

    @PostMapping("/getMsgCode")
    @ResponseBody
    @Transactional
    public synchronized Result getMsgCode(String cuPhone){
        if(!ValidateUtil.isMobile(cuPhone)){
            return  ResultUtil.error(ResultEnum.PHONE_ERROR);
        } else {
            return registerService.getCode(cuPhone);
        }
    }

    /**
     * 客户登录
     * @return
     */
    @PostMapping("customerLogin")
    @ResponseBody
    @Transactional
    public Result customerLogin(String code,String phone){
        if (phone.equals("18713888898")){
            //直接通过
                return registerService.customerLoginTrue(phone);
        } else {
            if(!ValidateUtil.isMobile(phone)){
                return ResultUtil.error(ResultEnum.PHONE_ERROR);
            }else{
                return registerService.customerLogin(phone,code);
            }
        }
    }

    /**
     * 忘记密码修改成新的（先调用获取验证码接口）
     * @param code              验证码
     * @param phone             电话号
     * @param newPassword       新密码
     * @return
     */
    @PostMapping("/validationPhone")
    @ResponseBody
    @Transactional
    public Result validationPhone(String code,String phone,String newPassword){
        return registerService.validationPhone(code,phone,newPassword);
    }


    /**
     * 商家app用户登录
     *
     * @param password
     * @param username
     * @return
     */
    @PostMapping("/merchantLogin")
    public Result merchantLogin(String password,String username){

        return registerService.merchantLogin(password,username);
    }
}
