package com.lsege.controller.currencyController;

import cn.beecloud.BCCache;
import cn.beecloud.BeeCloud;
import com.lsege.entity.JsonResult;
import com.lsege.entity.Result;
import com.lsege.entity.currency.CurrencyUser;
import com.lsege.entity.otherVo.DepositLogs;
import com.lsege.entity.otherVo.Orders;
import com.lsege.entity.otherVo.PayOrderVo;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.service.otherService.PayOrderService;
import com.lsege.service.shopService.MerchantService;
import com.lsege.util.DateUtil;
import com.lsege.util.SignUtil;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zhanglei on 2017/11/24.
 */
@RestController
@RequestMapping(value = "/beeCloudwebHook")
public class BeeCloudWebHookController {
    @Autowired
    PayOrderService payOrderService;
    @Autowired
    MerchantService merchantService;

    @PostMapping("webHookVerify")
    public String webHook(HttpServletRequest request) {
        String appID = "b0f1a2a9-47ef-46f5-a35f-1b2d4b940899";
        String testSecret = "506c5aa2-feac-4743-8758-43e3fdc712a4";
        String appSecret = "c2479ebf-b8f4-4f6b-9665-7ec7549bdb0c";
        String masterSecret = "94425876-7692-473d-9b69-24215043f084";
        BeeCloud.registerApp(appID, testSecret, appSecret, masterSecret);
        StringBuffer json = new StringBuffer();
        String line = null;
        try {
            request.setCharacterEncoding("utf-8");
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null) {
                json.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObj = JSONObject.fromObject(json.toString());
        String signature = jsonObj.getString("signature");
        String transactionId = jsonObj.getString("transaction_id");//交易单号
        String transactionType = jsonObj.getString("transaction_type");
        String channelType = jsonObj.getString("channel_type");
        String transactionFee = jsonObj.getString("transaction_fee");//支付金额
        StringBuffer toSign = new StringBuffer();
        toSign.append(BCCache.getAppID()).append(transactionId)
                .append(transactionType).append(channelType)
                .append(transactionFee);
        boolean status = verifySign(toSign.toString(), masterSecret, signature);
        System.out.println("支付类型:-------->"+transactionType);
        if (status) { //验证成功
            //通过订单号取得 订单信息进行比对
                PayOrderVo payOrderVo = payOrderService.getPayOrder(transactionId);
                if (transactionFee != null && !transactionFee.equals("")) {
                    //验证支付金额
                    if (transactionFee.equals(payOrderVo.getAmount().toString())) {
                        //判断 是 订单的支付 还是vip会员的 支付
                        if (payOrderVo.getPayFor() == 1) {
                            //判断折扣 是普通的折扣还是vip折扣
                            CurrencyUser currencyUser = payOrderService.getUserByUserId(payOrderVo.getUserId());
                            Merchant merchant = merchantService.getMerchant(payOrderVo.getMerchantId());
                            Integer realMoney = payOrderVo.getRealMoney();
                            Integer amount = payOrderVo.getAmount();
                            if (currencyUser.getVipTimeout() != null) {
                                if (currencyUser.getVipTimeout().getTime() > new Date().getTime()) {

                                    Integer buyMonery = Integer.parseInt(new BigDecimal(realMoney * merchant.getVipDiscount()).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                                    if (buyMonery .equals(amount)) {
                                        //金额验证成功  支付状态改为已支付
                                        payOrderService.updateState(transactionId);
                                        merchantService.createOrder(merchant.getId().toString(),
                                                payOrderVo.getUserId(),
                                                realMoney,
                                                amount);
                                        merchantService.upDate(merchant.getId().toString(),amount);
                                    } else {
                                        System.out.println("fail");
                                        return "fail";
                                    }

                                } else {

                                    Integer buyMonery = Integer.parseInt(new BigDecimal(realMoney * merchant.getDiscount()).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                                    if (buyMonery .equals(amount)) {
                                        //金额验证成功  支付状态改为已支付
                                        payOrderService.updateState(transactionId);
                                        merchantService.createOrder(merchant.getId().toString(),
                                                payOrderVo.getUserId(),
                                                realMoney,
                                                amount);
                                        merchantService.upDate(merchant.getId().toString(),amount);
                                    } else {
                                        System.out.println("fail");
                                        return "fail";
                                    }
                                }
                            } else {
                                Integer buyMonery = Integer.parseInt(new BigDecimal(realMoney * merchant.getDiscount()).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                                if (buyMonery .equals(amount)) {
                                    //金额验证成功  支付状态改为已支付
                                    payOrderService.updateState(transactionId);
                                    merchantService.createOrder(merchant.getId().toString(),
                                            payOrderVo.getUserId(),
                                            realMoney,
                                            amount);
                                    merchantService.upDate(merchant.getId().toString(),amount);
                                } else {
                                    System.out.println("fail");
                                    return "fail";
                                }
                            }


                        } else if (payOrderVo.getPayFor() == 2) {

                            //通过支付的金额 判断购买会员的天数

                            String day = payOrderService.getDay(Integer.valueOf(transactionFee));
                            if (day != null) {
                                payOrderService.updateState(transactionId);
                                payOrderService.changeVip(payOrderVo.getUserId(), DateUtil.getAfterDayDate(day));
                            } else {
                                System.out.println("fail"); //请不要修改或删除
                                return "fail";
                            }
                        }
                        System.out.println("success"); //请不要修改或删除
                        return "success";
                    } else {
                        System.out.println("fail");
                        return "fail";
                    }
                } else {
                    System.out.println("fail");
                    return "fail";
                }
            // 此处需要验证购买的产品与订单金额是否匹配:
            // 验证购买的产品与订单金额是否匹配的目的在于防止黑客反编译了iOS或者Android app的代码，
            // 将本来比如100元的订单金额改成了1分钱，开发者应该识别这种情况，避免误以为用户已经足额支付。
            // Webhook传入的消息里面应该以某种形式包含此次购买的商品信息，比如title或者optional里面的某个参数说明此次购买的产品是一部iPhone手机，
            // 开发者需要在客户服务端去查询自己内部的数据库看看iPhone的金额是否与该Webhook的订单金额一致，仅有一致的情况下，才继续走正常的业务逻辑。
            // 如果发现不一致的情况，排除程序bug外，需要去查明原因，防止不法分子对你的app进行二次打包，对你的客户的利益构成潜在威胁。
            // 如果发现这样的情况，请及时与我们联系，我们会与客户一起与这些不法分子做斗争。而且即使有这样极端的情况发生，
            // 只要按照前述要求做了购买的产品与订单金额的匹配性验证，在你的后端服务器不被入侵的前提下，你就不会有任何经济损失。
            // 处理业务逻辑
        } else { //验证失败
            System.out.println("fail");
            return "fail";
        }


    }

    private boolean verifySign(String text, String masterKey, String signature) {
        boolean isVerified = verify(text, signature, masterKey, "UTF-8");
        if (!isVerified) {
            return false;
        }
        return true;
    }

    private boolean verify(String text, String sign, String key, String inputCharset) {
        text = text + key;
        String mysign = SignUtil.md5Hex(getContentBytes(text, inputCharset));
        return mysign.equals(sign);
    }

    private byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名过程中出现错误,指定的编码集不对,您目前指定的编码集是:" + charset);
        }
    }


    public static void main(String args[]) throws Exception {
        System.out.println("四舍五入取整:(2)=" + new BigDecimal(20 * 0.88).setScale(0, BigDecimal.ROUND_HALF_UP));
        System.out.println("四舍五入取整:(2.1)=" + new BigDecimal("2.1").setScale(0, BigDecimal.ROUND_HALF_UP));
        System.out.println("四舍五入取整:(2.5)=" + new BigDecimal("2.5").setScale(0, BigDecimal.ROUND_HALF_UP));
        System.out.println("四舍五入取整:(2.9)=" + new BigDecimal("2.9").setScale(0, BigDecimal.ROUND_HALF_UP));


    }

    @PostMapping("test")
    @ResponseBody
    public JsonResult test(String transactionFee) {
        JsonResult jsonResult = new JsonResult();
        try {
            String day = payOrderService.getDay(Integer.valueOf(transactionFee));
            if (day != null) {
                System.out.println(day);
            } else {
                System.out.println("fail");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonResult;

    }


}
