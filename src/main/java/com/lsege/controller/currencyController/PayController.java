package com.lsege.controller.currencyController;

import cn.beecloud.BCEumeration;
import cn.beecloud.BCPay;
import cn.beecloud.BeeCloud;
import cn.beecloud.bean.BCException;
import cn.beecloud.bean.BCOrder;
import cn.beecloud.bean.TransferParameter;
import com.alipay.api.AlipayClient;
import com.alipay.api.AlipayMobilePublicMultiMediaClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayFundTransToaccountTransferRequest;
import com.alipay.api.response.AlipayFundTransToaccountTransferResponse;
import com.lsege.entity.JsonResult;
import com.lsege.entity.Result;
import com.lsege.entity.currency.CurrencyUser;
import com.lsege.entity.currency.PayPledge;
import com.lsege.entity.otherVo.DepositLogs;
import com.lsege.entity.otherVo.PayOrderVo;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import com.lsege.service.currencyService.PayService;
//import com.pingplusplus.model.Charge;
import com.lsege.service.otherService.MerchantAppService;
import com.lsege.service.otherService.PayOrderService;
import com.lsege.service.otherService.PayOrderServiceImpl;
import com.lsege.service.shopService.MerchantService;
import com.lsege.util.DateUtil;
import com.lsege.util.SignUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InterruptedIOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * beecloud支付
 */

@RestController
@RequestMapping(value = "/pay")
public class PayController {
    @Autowired
    PayOrderService payOrderService;
    @Autowired
    MerchantService merchantService;
    @Autowired
    MerchantAppService merchantAppService;

    public static String appID = "cccf74dd-90df-4ba9-a3f7-7b5b415a62e1";
    public static String testSecret = "c04a6248-9313-4a38-b836-1c4707905fcf";
    public static String appSecret = "89f40a2f-acea-4e59-a29a-08621defab83";
    public static String masterSecret = "a045c246-136c-4bcf-9d45-8ee13ac9a496";


    public static String optional = "{\"msg\":\"addtion msg\"}";

    //支付宝//公用变量
    public static String serverUrl = "https://openapi.alipay.com/gateway.do";
    public static String app_id = "2017122001013118";//1
    public static String private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDDKJZb6qniG+vY1ekQJqeotmBG43k2TF/d4ldRS01LfHFTdf9lLfPKcN87hoEflzxZS8gZn9hlEbOBPX+WruHALgqdiEvcAonADkDcgRJtYQd1+2GfnswxLj8OBzQP9rhKJuDkabEVQ+wc2P6t0q5LjUUtUVa23gNzDfzu+HqA0ojxrwOm5PsOeHNKXG2kiZ6paL7FhDY/fzy91pNJkvviCNQX/0zyO25hqs3tlLPNMNejXAuj88oNYHcwnMNxHQO5YoJhYGRvd0wyQd0ID99H3CeFZobaf+8igUS/2039hkHyYoE9QMKXAb0cLBogMwKXTGfim+9/Ovo87XlyTvZRAgMBAAECggEAMpu82hsxcMwyzeky5hhQFAUvVFK65STLG3wNjqxLYA7mSHFqCG/xL4czrHkF1Q5f/PwU6Og1sofzl8k94/J34mFOKWnl2ijiVeacVVI3WBv4VlUZDM6ePwayHU4q5xTg14HbggAShuQ1P4naCXN/9WGuFcFpDkTmehifhczz36HFISMMLU0LcHN8fAml7nLBr6VLKfq1MEuQZBpDFHpnKv4+7Sv3PjVRrAfWx5e1ShwQR1gXo1jd4LPVdxJSF2n9snIizP5qypN8SqW3AiASvY+/BQC4PrJNj5xAnR/QRkf4MrYK89Qp1J0BzuNVO7i+HALBk7A0SW5Hml38zbdPUQKBgQDlGSkZfentU6U1Que5OXkWiqVT7G1EgI93IpH9MxPQjNSSs5UYIl9ggHQ01HGs2WFbytJjeMkmqhhd1WAKGIzfgkK9by7wQKYaus+GHjnrKV9g6ZQ24506lSbhibrjSXejhgDRKiCJCOnTUTsvhrnm6mGfNGIXWojadQTlU638fQKBgQDaEy0/ZdU3+YjcZXLWXo09BhcNKl90z6kZjx359g7sWO7+7Uf9zF8bOHOyjHBNrVbZ9HWbpPm5dkQDropMHHl4N22JhQLtOzXFxwK52e1/3cxoD3dPv0s9iegJ/HFn6kvUJMwhO1fQOJjJwTkguJBiJSOUHkEyD7gealtXy7gNZQKBgQCTTHcRbdSvA73QHeFRRTOmm6Qq7nNevO4BIlgNHP8bcsDmRdKRvt/l8xFEOmeQRAAF/cUBsdJbKrgZ9KwYMaq5tk3k8IyUamyh+QSe7eY9Xc92RxhFplkcJnPmRSDFLCJhIDJ8SYK7uOBmPu834n66im4T56E99LbTBrQMIp5WHQKBgHb3DzAOtYqbrnXNQiB/5DzuHZUpEHoA7xbqZEgAW5PD0F1xxFxIKXoICdXVl05IyaCNI7uM/E6YnfIEPZ3XyswyMKdc0IPqUSNbtbj9kfq3XNNH3Ka8jKc9nEAWXyd5QXSxvkpCvVzyAyfNKQUYqmtgJ1CeUKKmwKKXEs2KbRWhAoGAZoXlEiBpqU60pPY8VhHsqdTXZCfoxlqCIkZcWPL+mfGVn5/jGAc1DWq/gqSooVb/uX7QAsqs7cYhAVaC3RE5lclwKmCtOUAruCK6XayNhfRwqGh3RtOkgMZNCeCEMU10rY5KkMDIqE8ppSRd2pCcvCO3P46UnyCtWiPhbm0DTp8=";
    public static String format = "json";//1
    public static String charset = "utf-8";//1
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkuIaslVpzjcO5EJ7BlrgiigDyPf428t5fPdNBI0d/gB8h+PBHv8L0JbgMho4pyyN0Zx2UU3a9ZocFVRrpsCWaGwGY9IpqvZ/bD/tcz2+vxy9e4JJUqNzZ1k8YOWYEg5LJBehxhrBZk3PcXP52PHtwyGbP364G5taZLzcJ7M0vz14/Xm7vDHU8R8B4vZ9asLqzsOKowgC+o0o8StHNFsHSfKCsu2T7uR9/ZAbSj0ZsdS2WfjNe1KrTDAFLoH3SumgeEmOInZh98PejYxfjpPfTOlk+tng90TqyKhQIg3rDMZZsfS1FTlv/YGtGG1/RfzkE+l3svOsY8fS3+pJjx6KkwIDAQAB";
    public static String RSA2 = "RSA2";

//    /**
//     * ping++支付
//     * @param payPledge
//     * @return
//     */
//    @PostMapping("pledge")
//    public Charge userPledge(PayPledge payPledge) throws Exception {
//        return  payService.userPledge(payPledge);
//    }

    /**
     * beecloud支付
     *
     * @param title
     * @param realMoney
     * @param amount
     * @param request
     * @param merchantId
     * @param payType
     * @return
     * @throws InterruptedException
     */
    @PostMapping("payByBeeCloud")
    public synchronized Result payByBeeCloud(String title, Integer realMoney, Integer amount, HttpServletRequest request, Integer merchantId, Integer payType) throws InterruptedException {
        try {
            String userId = request.getAttribute("id").toString();
            CurrencyUser currencyUser = payOrderService.getUserByUserId(userId);
            Merchant merchant = merchantService.getMerchant(merchantId);


            Boolean canIPay = false;
            if (currencyUser.getVipTimeout() != null) {
                System.out.println(currencyUser.getVipTimeout().getTime());
                System.out.println(new Date().getTime());
                if (currencyUser.getVipTimeout().getTime() > new Date().getTime()) {

                    Integer buyMonery = Integer.parseInt(new BigDecimal(realMoney * merchant.getVipDiscount()).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                    if (buyMonery .equals(amount)) {
                        canIPay = true;
                    } else {
                        canIPay = false;
                    }

                } else {

                    Integer buyMonery = Integer.parseInt(new BigDecimal(realMoney * merchant.getDiscount()).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                    if (buyMonery .equals(amount)) {
                        canIPay = true;
                    } else {
                        canIPay = false;
                    }
                }
            } else {
                Integer buyMonery = Integer.parseInt(new BigDecimal(realMoney * merchant.getDiscount()).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                if (buyMonery .equals(amount)) {
                    canIPay = true;
                } else {
                    canIPay = false;
                }
            }
            if (canIPay) {
                String maxOrderNum = payOrderService.getMaxOrderNumber();


                PayOrderVo payOrderVo = new PayOrderVo();

                if (maxOrderNum == null) {
                    payOrderVo.setOrderNumber(DateUtil.getDays() + "000001");
                } else {
                    payOrderVo.setOrderNumber(getOrder(maxOrderNum));

                }

                payOrderVo.setAmount(amount);
                payOrderVo.setRealMoney(realMoney);
                //        payOrderVo.setSign(sign);
                payOrderVo.setCreateDate(new Date());

                payOrderVo.setState(0);
                payOrderVo.setPayFor(1);
                payOrderVo.setUserId(userId);
                payOrderVo.setMerchantId(merchantId);
                payOrderVo.setPayType(payType);
                payOrderService.savePayOrder(payOrderVo);
                return ResultUtil.success(payOrderVo.getOrderNumber());
            } else {
                return ResultUtil.error(ResultEnum.MONERY_ERROR);
            }
        } catch (Exception e) {
            System.out.println(e);
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }

    }

    @PostMapping("test")
    public Result test(String title, Integer realMoney, Integer amount, Integer merchantId, Integer payType){
        try {
            String userId = "0a45d8bf44c041828ecbde9b81361d39";
            CurrencyUser currencyUser = payOrderService.getUserByUserId(userId);
            Merchant merchant = merchantService.getMerchant(merchantId);


            Boolean canIPay = false;
            if (currencyUser.getVipTimeout() != null) {
                if (currencyUser.getVipTimeout().getTime() > new Date().getTime()) {

                    Integer buyMonery = Integer.parseInt(new BigDecimal(realMoney * merchant.getVipDiscount()).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                    if (buyMonery.equals(amount)) {
                        canIPay = true;
                    } else {
                        canIPay = false;
                    }

                } else {

                    Integer buyMonery = Integer.parseInt(new BigDecimal(realMoney * merchant.getDiscount()).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                    if (buyMonery.equals(amount) ) {
                        canIPay = true;
                    } else {
                        canIPay = false;
                    }
                }
            } else {
                Integer buyMonery = Integer.parseInt(new BigDecimal(realMoney * merchant.getDiscount()).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                if (buyMonery.equals(amount)) {
                    canIPay = true;
                } else {
                    canIPay = false;
                }
            }
            if (canIPay) {
                String maxOrderNum = payOrderService.getMaxOrderNumber();


                PayOrderVo payOrderVo = new PayOrderVo();

                if (maxOrderNum == null) {
                    payOrderVo.setOrderNumber(DateUtil.getDays() + "000001");
                } else {
                    payOrderVo.setOrderNumber(getOrder(maxOrderNum));

                }

                payOrderVo.setAmount(amount);
                payOrderVo.setRealMoney(realMoney);
                //        payOrderVo.setSign(sign);
                payOrderVo.setCreateDate(new Date());

                payOrderVo.setState(0);
                payOrderVo.setPayFor(1);
                payOrderVo.setUserId(userId);
                payOrderVo.setMerchantId(merchantId);
                payOrderVo.setPayType(payType);
                payOrderService.savePayOrder(payOrderVo);
                return ResultUtil.success(payOrderVo.getOrderNumber());
            } else {
                return ResultUtil.error(ResultEnum.MONERY_ERROR);
            }
        } catch (Exception e) {
            System.out.println(e);
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }

    }

    /**
     * 购买会员
     *
     * @param amount
     * @param request
     * @param payType
     * @return
     * @throws InterruptedException
     */
    @PostMapping("buyVip")
    public synchronized Result buyVip(Integer amount, HttpServletRequest request, Integer payType) throws InterruptedException {
        try {
            String userId = request.getAttribute("id").toString();
            String maxOrderNum = payOrderService.getMaxOrderNumber();


            PayOrderVo payOrderVo = new PayOrderVo();

            if (maxOrderNum == null) {
                payOrderVo.setOrderNumber(DateUtil.getDays() + "000001");
            } else {
                payOrderVo.setOrderNumber(getOrder(maxOrderNum));

            }

            payOrderVo.setAmount(amount);
            //        payOrderVo.setSign(sign);
            payOrderVo.setCreateDate(new Date());

            payOrderVo.setState(0);
            payOrderVo.setPayFor(2);
            payOrderVo.setUserId(userId);
            payOrderVo.setPayType(payType);
            payOrderService.savePayOrder(payOrderVo);

            //更改vip剩余时间

            return ResultUtil.success(payOrderVo.getOrderNumber());
        } catch (Exception e) {
            System.out.println(e);
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }

    }

    @PostMapping("/updateVipTime")
    public Result updateVipTime(HttpServletRequest request,Integer date){
        String userId = request.getAttribute("id").toString();
//        PayOrderService payOrderService1 = new PayOrderServiceImpl();
        payOrderService.updateVipTimeOut(userId,date);
        return ResultUtil.success(ResultEnum.SUCCESS);
    }


    /**
     * 生成提现  订单
     *
     * @param money
     * @param request
     * @param type
     * @return
     */
    @PostMapping("withDraw")
    public synchronized Result whithDraw(String money, HttpServletRequest request, String type, String payeeAccount, String payeeRealName) {
        try {
            String userId = request.getAttribute("id").toString();

            System.out.println("金额:"+money);
            System.out.println("用户:"+userId);
            System.out.println("类型:"+type);
            System.out.println("账号:"+payeeAccount);
            System.out.println("名字:"+payeeRealName);

            String num = "DDN" + DateUtil.getDays() + System.currentTimeMillis();
            DepositLogs depositLogs = new DepositLogs();
            depositLogs.setCreateTime(new Date());
            depositLogs.setInfo("申请提现中");
            depositLogs.setState(1);
            depositLogs.setType(type);
            depositLogs.setLogsCode(num);
            depositLogs.setOutMoney(Integer.valueOf(money));
            depositLogs.setMerchantUserId(userId);
            depositLogs.setPayeeAccount(payeeAccount);
            depositLogs.setPayeeRealName(payeeRealName);
            payOrderService.saveLogs(depositLogs);

            /**更改钱包金额*/
            Integer banlance = merchantAppService.getBanlance(userId);
            payOrderService.updateBag(userId, banlance - Integer.parseInt(money));

            return ResultUtil.success("提现交易申请成功");
        } catch (Exception e) {

            e.printStackTrace();
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }

    }

    public static void main(String args[]) throws Exception {

        Integer a = 200;
        Integer b = 200;
        System.out.println("Integer+'=='--->"+(a==b));
        System.out.println("Integer+'equals'--->"+(a.equals(b)));

        Integer a2 = 20;
        Integer b2 = 20;
        System.out.println("Integer+'=='--->"+(a2==b2));
        System.out.println("Integer+'equals'--->"+(a2.equals(b2)));

        int a3 = 20;
        int b3 = 20;
        System.out.println("Integer+'=='--->"+(a3==b3));
//        System.out.println("Integer+'equals'--->"+(a3.equals(b3))); 没有equals 方法

        Integer a4 = 20;
        int b4 = 20;
        System.out.println("Integer+'=='--->"+(a4==b4));
        System.out.println("Integer+'equals'--->"+(a4.equals(b4)));


        Integer a5 = 200;
        int b5 = 200;
        System.out.println("Integer+'=='--->"+(a5==b5));
        System.out.println("Integer+'equals'--->"+(a5.equals(b5)));
    }

//    /**
//     * @param payeeAccount  账号
//     * @param amount        金额
//     * @param payeeRealName 真实姓名
//     * @param num           订单号
//     * @return
//     * @throws Exception
//     */
//    @PostMapping("aliWithDraw")
//    public Result aliWithDraw(String payeeAccount, String amount, String payeeRealName, String num) throws Exception {
//        Double d = Double.valueOf(amount)/100;
//        AlipayClient alipayClient = new DefaultAlipayClient(serverUrl, app_id,
//                private_key, format, charset, alipay_public_key, RSA2);
//        AlipayFundTransToaccountTransferRequest request = new AlipayFundTransToaccountTransferRequest();
//        request.setBizContent("{" +
//                "\"out_biz_no\":\"" + num + "\"," +
//                "\"payee_type\":\"ALIPAY_LOGONID\"," +
//                "\"payee_account\":\"" + payeeAccount + "\"," +
//                "\"amount\":\"" + d + "\"," +
//                "\"payer_show_name\":\"大戴南支付宝转账\"," +
//                "\"payee_real_name\":\"" + payeeRealName + "\"," +
//                "\"remark\":\"大戴南支付宝转账\"" +
//                "}");
//        AlipayFundTransToaccountTransferResponse response = alipayClient.execute(request);
//        if (response.isSuccess()) {
//            System.out.println("调用成功");
//            DepositLogs depositLogs = new DepositLogs();
//            depositLogs.setInfo("提现成功");
//            depositLogs.setState(1);
//            depositLogs.setLogsCode(num);
//            depositLogs.setPayeeAccount(payeeAccount);
//            depositLogs.setPayeeRealName(payeeRealName);
//            payOrderService.updateDepositLogs(depositLogs);
//            return ResultUtil.success("提现成功");
//        } else {
//            System.out.println("调用失败");
//            DepositLogs depositLogs = new DepositLogs();
//            depositLogs.setInfo(response.getSubMsg());
//            depositLogs.setState(0);
//            depositLogs.setLogsCode(num);
//            depositLogs.setPayeeAccount(payeeAccount);
//            depositLogs.setPayeeRealName(payeeRealName);
//            payOrderService.updateDepositLogs(depositLogs);
//            return ResultUtil.error("180",response.getSubMsg());
//        }
//
//
//    }

    public static DepositLogs aliWithDrawStaticMethod(String payeeAccount, Integer amount, String payeeRealName, String num) throws Exception {
        Double d = Double.valueOf(amount) / 100;
        AlipayClient alipayClient = new DefaultAlipayClient(serverUrl, app_id,
                private_key, format, charset, alipay_public_key, RSA2);
        AlipayFundTransToaccountTransferRequest request = new AlipayFundTransToaccountTransferRequest();
        request.setBizContent("{" +
                "\"out_biz_no\":\"" + num + "\"," +
                "\"payee_type\":\"ALIPAY_LOGONID\"," +
                "\"payee_account\":\"" + payeeAccount + "\"," +
                "\"amount\":\"" + d + "\"," +
                "\"payer_show_name\":\"大戴南支付宝转账\"," +
                "\"payee_real_name\":\"" + payeeRealName + "\"," +
                "\"remark\":\"大戴南支付宝转账\"" +
                "}");
        AlipayFundTransToaccountTransferResponse response = alipayClient.execute(request);
        DepositLogs depositLogs = new DepositLogs();
        depositLogs.setLogsCode(num);
        if (response.isSuccess()) {
            depositLogs.setInfo("提现成功");
            depositLogs.setState(2);
        } else {
            depositLogs.setInfo(response.getSubMsg());
            depositLogs.setState(3);
        }
        return depositLogs;
    }

    public static String getOrder(String maxOrderNum) {
        Integer num = Integer.valueOf(maxOrderNum.substring(8, maxOrderNum.length())) + 1;
        String numStr = num.toString();
        String nexStr = "";
        switch (numStr.length()) {
            case 6:
                nexStr = numStr;
                break;
            case 5:
                nexStr = "0" + numStr;
                break;
            case 4:
                nexStr = "00" + numStr;
                break;
            case 3:
                nexStr = "000" + numStr;
                break;
            case 2:
                nexStr = "0000" + numStr;
                break;
            case 1:
                nexStr = "00000" + numStr;
                break;

        }

        return DateUtil.getDays() + nexStr;
    }


}
