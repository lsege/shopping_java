package com.lsege.controller.currencyController;

import com.lsege.entity.JsonResult;
import com.lsege.service.currencyService.PayService;
import com.lsege.service.currencyService.WxPayService;
import com.lsege.util.wx.ConfigUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 创建时间：2016年11月9日 下午5:49:00
 *	微信企业转账
 * @author andy
 * @version 2.2
 */

@RestController
@RequestMapping("/transfer")
@Transactional
public class TransferController {

	@Autowired
	WxPayService wxPayService;

	private static final Logger LOG = Logger.getLogger(TransferController.class);
	/**localhost:6080/transfer/pay?phone=18712882023&amount=101
	 * 企业向个人支付转账
	 */
	@PostMapping("/pay")
	public JsonResult transferPay(String cuPhone, Integer amount) {
		LOG.info("[/transfer/pay]");
		JsonResult json = wxPayService.transferPay(cuPhone, amount);
		return json;
	}

	/**
	 * 企业向个人转账查询
	 * @param request
	 * @param response
	 * @param tradeno 商户转账订单号
	 * @param callback
	 */
	@RequestMapping(value = "/pay/query", method = RequestMethod.POST)
	public JsonResult orderPayQuery(HttpServletRequest request, HttpServletResponse response, String tradeno,
							  String callback) {
		LOG.info("[/transfer/pay/query]");
		JsonResult json = wxPayService.orderPayQuery(request, response, tradeno, callback);
		return json;
	}
}