package com.lsege.controller.currencyController;

import com.lsege.service.currencyService.PayService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;

/**   ping++ 信息回调
 * Created by liuze on 2017/7/7.
 */
@RestController
@RequestMapping(value ="/webHook")
public class WebHookController {

    @Autowired
    PayService payService;


    @PostMapping("paySuccess")
    public String paySuccess(@RequestBody String event, HttpServletRequest request) throws Exception {
        System.out.println("=============接收到sucess============="+event);
        String signatureKey = request.getHeader("X-Pingplusplus-Signature");
        boolean checkOk = verifyData(event, signatureKey, getPubKey());
        if (checkOk){
           boolean isSuccess =  payService.payResultOk(event);
            if (isSuccess){
                return "2xx";
            }
        }
        return null;
    }

    /**
     * 获得公钥
     * @return
     * @throws Exception
     */
    public static PublicKey getPubKey() throws Exception {
        String pubKeyString = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq406c2cIcuC4dgRus+jb\n" +
                "LOzvlOvHv37uLU1ceSW9gSySruR6KLbAK+ORM7rsg3Oqxya6ttpLP8olNMd5vq2g\n" +
                "SId8AVoQ1t1/igC38sSEhpZ49dEql712V/iru/PCW3FbGz6PzDn/3KpIAm97sSAN\n" +
                "6JSWsBUv2xFJLe27XjlFvJauy+Qr+r2wa8fMHK0XNw0M8u3wBmflNzog32Ltqa5F\n" +
                "Eu9MVPI+CTUqxOYMdlFyO3TzsvFb7lhNLTpm4pE3oG1WR516U27fnxbbY1Oey2LF\n" +
                "8ZX+VGuDxAms/EE1OfzinnBwtClQ78eej023DhJejFggiqmW3303C5T0w9zhDzPj\n" +
                "SQIDAQAB";
        byte[] keyBytes = Base64.decodeBase64(pubKeyString);
        System.out.println(pubKeyString);
        // generate public key
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(spec);
        return publicKey;
    }
    /**
     * 验证签名
     * @param dataString
     * @param signatureString
     * @param publicKey
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public static boolean verifyData(String dataString, String signatureString, PublicKey publicKey)
            throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, UnsupportedEncodingException {
        byte[] signatureBytes = Base64.decodeBase64(signatureString);
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(publicKey);
        signature.update(dataString.getBytes("UTF-8"));
        return signature.verify(signatureBytes);
    }
}
