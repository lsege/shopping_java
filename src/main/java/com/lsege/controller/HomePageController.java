package com.lsege.controller;

import com.lsege.entity.JsonResult;
import com.lsege.entity.Result;
import com.lsege.entity.otherVo.News;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.entity.vo.ActivityPicture;
import com.lsege.integration.ResultUtil;
import com.lsege.service.otherService.HomePageService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Administrator on 2017/10/12.
 */
@RestController
@RequestMapping(value = "/homePage")
public class HomePageController {

    @Autowired
    HomePageService homePageService;

    /**
     * 获取首页轮播活动图
     * @return
     */
    @GetMapping("/loadActivityPictures")
    public Result loadActivityPictures(){
        List<ActivityPicture> list = homePageService.loadActivityPictures();
        return ResultUtil.success(list);
    }

    /**
     * 获取首页新闻广告列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    @PostMapping("/loadNewsAndAdvList")
    public Result loadNewsAndAdvList(Integer pageNum,Integer pageSize){
        List<News> list = homePageService.loadNewsAndAdvList(pageNum,pageSize);
        return ResultUtil.success(list);
    }

    /**
     * 关键字搜索
     * @param keyWord
     * @return
     */
    @PostMapping("/queryMerchantKeyWord")
    public Result queryMerchantKeyWord(String keyWord){
        List<Merchant> merchants = homePageService.queryMerchantsByKeyWord("%"+keyWord+"%");
        return ResultUtil.success(merchants);
    }
}
