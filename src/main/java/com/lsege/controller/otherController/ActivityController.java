package com.lsege.controller.otherController;

import com.lsege.entity.Result;
import com.lsege.entity.otherVo.Activity;
import com.lsege.entity.JsonResult;
import com.lsege.entity.otherVo.Invitation;
import com.lsege.entity.shopVo.Merchant;
import com.lsege.integration.ResultUtil;
import com.lsege.service.otherService.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Administrator on 2017/10/12.
 */
@RestController
@RequestMapping("/activity")
public class ActivityController {

    @Autowired
    ActivityService activityService;

    /**
     * 获取活动列表，以开启时间排序
     * @param pageNum
     * @param pageSize
     * @return
     */
    @PostMapping("/loadAllActivity")
    public Result loadAllActivity(Integer pageNum,Integer pageSize){
        List<Activity> list = activityService.loadAllActivity(pageNum,pageSize);
        return ResultUtil.success(list);
    }

    /**
     * 获取活动详细信息
     * @param id
     * @return
     */
    @PostMapping("/loadActivityInfo")
    public Result loadActivityInfo(Integer id,Integer showType){
        Activity activity = activityService.loadActivityInfo(id,showType);
        return ResultUtil.success(activity);
    }

    /**
     * 获取口碑（推荐）店铺列表
     * 先按照是否推荐排序，再按照是否付款（顶置排序）
     * @param pageNum
     * @param pageSize
     * @return
     */
    @PostMapping("/loadRecommendStoreList")
    public Result loadRecommendStoreList(Integer pageNum,Integer pageSize){
        List<Merchant> list = activityService.loadRecommendStoreList(pageNum,pageSize);
        return ResultUtil.success(list);
    }

    /**
     * 查询 店铺旁边的贴字
     * @param pageNum
     * @param pageSize
     * @return
     */
    @PostMapping("getTabList")
    public Result getTabList(Integer pageNum,Integer pageSize,Integer type){
        List<Invitation> list = activityService.getTabList(pageNum,pageSize,type);
        return ResultUtil.success(list);
    }

    /**
     * 查询 tab页中详情
     * @param id
     * @return
     */
    @PostMapping("getTabDetail")
    public Result getTabDetail(Integer id){
        Invitation invitation = activityService.getTabDetail(id);
        return ResultUtil.success(invitation);
    }
}
