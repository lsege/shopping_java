package com.lsege.controller.otherController;

import com.lsege.entity.JsonResult;
import com.lsege.entity.Result;
import com.lsege.entity.otherVo.Alog;
import com.lsege.entity.otherVo.Recruitment;
import com.lsege.integration.HomemadeException;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import com.lsege.service.otherService.RecruitmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by zhanglei on 2017/10/12.
 */
@RestController
@RequestMapping(value = "/recruitment")
public class RecruitmentController {

    @Autowired
    RecruitmentService recruitmentService;



    /**
     * 通过type 获得婚恋求职招聘列表
     *
     * @param type (1:婚恋2:求职3:招聘)
     * @param pageNum 当前页数
     * @param pageSize 每页多少个
     * @return
     */
    @PostMapping("/getRecList")
    public  Result getRecList(Integer type,Integer pageNum,Integer pageSize){
        List<Recruitment> recruitmentList=recruitmentService.getRecLsit(type,pageNum,pageSize);
        return ResultUtil.success(recruitmentList);
    }

    /**
     * 婚恋求职招聘详情列表
     *
     * @param recruitmentId
     * @return
     */
    @PostMapping("/getRecDatial")
    public  Result getRecDetail(Integer recruitmentId){
        Recruitment recruitment=recruitmentService.getRecDatial(recruitmentId);
        return ResultUtil.success(recruitment);
    }

    /**
     * 图片上传
     * @param image
     * @return
     */
    @PostMapping("/imageUpload")
    public Result imageUpload(MultipartFile image) throws  Exception {
        if(image != null && !image.equals("")){
            String filePath="http://101.200.203.98:7080/shop_image/";
            File f1=new File(filePath);
            if (!f1.exists()) {
                f1.mkdirs();
            }
            //图片名称
            String originalFilename = System.currentTimeMillis()+"";
            //图片后缀
            String prefix=originalFilename.substring(originalFilename.lastIndexOf(".")+1);
            FileOutputStream os = new FileOutputStream(filePath + originalFilename + prefix);
            InputStream in = image.getInputStream();
            int b = 0;
            while ((b = in.read()) != -1) {
                os.write(b);
            }
            os.flush();
            os.close();
            in.close();
            String url= "http://101.200.203.98:7080/shop_image/" + originalFilename + prefix;
            return ResultUtil.success(url);
        }else {
            throw new HomemadeException(ResultEnum.LIKECOUNT_ERROR);
        }
    }

    /**
     * 发布婚恋，求职，招聘
     * @param type
     * @param title
     * @param context
     * @return
     */
    @PostMapping("/saveRecruitment")
    public Result saveRecruitment(HttpServletRequest request, String type, String title, String context, String image){
        String userId=request.getAttribute("id").toString();
        try {
            recruitmentService.saveRecruitment(userId,type,title,context,image);
            return ResultUtil.success(ResultEnum.SUCCESS);
        } catch (Exception e){
            return ResultUtil.error(ResultEnum.THIS_IS_ERROR);
        }

    }

}
