package com.lsege.controller.otherController;

import com.lsege.entity.Result;
import com.lsege.entity.otherVo.DepositLogs;
import com.lsege.entity.otherVo.Invitation;
import com.lsege.entity.otherVo.MerchantAppOrderVo;
import com.lsege.entity.otherVo.Orders;
import com.lsege.entity.sys.MerchantUser;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import com.lsege.service.otherService.MerchantAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by zhanglei on 2017/12/16.
 */

@RestController
@RequestMapping("merchantApp")
public class MerchantAppController {


    @Autowired
    MerchantAppService merchantAppService;

    /**
     * 订单列表
     *
     * @param request
     * @return
     */
    @GetMapping("orderList")
    public Result orderList(HttpServletRequest request, Integer pageNum, Integer pageSize) {

        try {
            String userId = request.getAttribute("id").toString();
            Integer startNum = (pageNum - 1) * pageSize;
            List<MerchantAppOrderVo> list = merchantAppService.getOrderList(userId, startNum, pageSize);
            for (int i = 0; i < list.size(); i++) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");

                MerchantAppOrderVo merchantAppOrderVo = list.get(i);
                merchantAppOrderVo.setDateStr(simpleDateFormat.format(merchantAppOrderVo.getCreateDate()));
            }
            return ResultUtil.success(list);
        } catch (Exception e) {
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }

    }


    /**
     * 钱包余额
     *
     * @param request
     * @return
     */
    @GetMapping("/walletBalance")
    public Result walletBalance(HttpServletRequest request) {
        try {
            String userId = request.getAttribute("id").toString();
            Integer banlance = merchantAppService.getBanlance(userId);
//            Integer p = merchantAppService.depositMonery(userId);
//            Integer y = (banlance == null ? 0 : banlance) - (p == null ? 0 : p);
            return ResultUtil.success(banlance);
        } catch (Exception e) {
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }
    }


    /**
     * 提现列表
     *
     * @param request
     * @return
     */
    @GetMapping("/depositLogs")
    public Result depositLogs(HttpServletRequest request,Integer pageNum,Integer pageSize) {

        try {
            String userId = request.getAttribute("id").toString();
            List<DepositLogs> logs = merchantAppService.depositLogsList(userId,pageNum,pageSize);

            for (int i = 0; i < logs.size(); i++) {
                DepositLogs log = logs.get(i);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                log.setCreateTimeStr(simpleDateFormat.format(log.getCreateTime()));
            }
            return ResultUtil.success(logs);
        } catch (Exception e) {

            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }
    }

    /**
     * 商家端新增帖子
     *
     * @param request
     * @return
     */
    @PostMapping("addInvitation")
    public Result addInvitation(HttpServletRequest request, String title, String introduction, String image) {
        try{
            String userId = request.getAttribute("id").toString();
            Integer merchantId = merchantAppService.getUserById(userId);

            Integer count = merchantAppService.querySendCount(merchantId);
            if (count >= 2){
                return ResultUtil.error(ResultEnum.IS_NOT_SEND_COUNT);
            }

            Integer merchantType = null;
            if(merchantId!=null){
                merchantType = merchantAppService.getMerchantById(merchantId);
            }
            Invitation invitation = new Invitation();
            String[] imageUrl;

            imageUrl = image.split(",");

            String html1= "<!DOCTYPE html>\n" +
                    "\n" +
                    "<html>\n" +
                    "\n" +
                    "\t<head>\n" +
                    "\t\t\n" +
                    "<meta charset=\"utf-8\">\n" +
                    "\t\t<meta name=\"viewport\" content=\"width=device-width,height=device-height, minimum-scale=1.0, maximum-scale=1.0\">\n" +
                    "\n" +
                    "\t</head>\n" +
                    "\t\n" +
                    "<style>\n" +
                    "\n" +
                    "\t\timg { height: auto; width: auto\\9; width:100%; }\n" +
                    "\t\t@media only screen and (min-width: 1200px){}\n" +
                    "\t\t\n" +
                    "media only screen and (min-width: 768px) and (max-width: 991px){}\n" +
                    "\n" +
                    "\t\t@media only screen and (max-width: 767px){}\n" +
                    "\n" +
                    "\t\t@media only screen and (-webkit-min-device-pixel-ratio: 1.3),only screen and (-o-min-device-pixel-ratio: 13/10),only screen and (min-resolution: 120dpi){}\n" +
                    "\t</style>\n" +
                    "\n" +
                    "\t<body>\n" +
                    "\n" +
                    "\t\t<h4 style=\"padding-left:20px\">"+title+"</h4>\n" +
                    "\t\t<span style=\"padding-left:20px\">"+introduction+"</span>\n" +
                    "\t\t<table style=\"padding-top:20px;\">";
            for(int i=0; i<imageUrl.length; i++){
                html1 += "<tr>\n" +
                        "\t\t\t\t<td><img src='"+imageUrl[i]+"'/></td\n" +
                        "\t\t\t</tr>" +
                        "<tr><td style=\"height=20px\"></td></td>";
            }
            String html2 = html1+"</table>"+
                    "</body>"+
                    "</html>";

            invitation.setMerchantId(merchantId);
            invitation.setCreateTime(new Timestamp(System.currentTimeMillis()));
            invitation.setContext(html2);
            if(imageUrl.length!=0){
                invitation.setImage(imageUrl[0]);
            }
            invitation.setTitle(title);
            invitation.setIntroduction(introduction);
            invitation.setState(1);
            invitation.setMerchantType(merchantType);

            merchantAppService.addInvitation(invitation);
            return ResultUtil.success("添加成功");

        }catch (Exception e){
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }

    }

    /**
     * 商家端发布活动列表(发布人,店铺)
     * @param request
     * @param pageNum
     * @param pageSize
     *
     * @return
     */
    @GetMapping("/getInvitationList")
    public Result getInvitationList(HttpServletRequest request,Integer pageNum,Integer pageSize){
        try {
            String userId = request.getAttribute("id").toString();
            Integer merchantId = merchantAppService.getUserById(userId);

            List<Invitation> list = merchantAppService.getInvitationList(merchantId,pageNum,pageSize);
            return ResultUtil.success(list);
        } catch (Exception e){
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }
    }

    /**
     * 商家端发布活动详情
     * @param invitationId
     * @return
     */
    @PostMapping("/getInvitationInfo")
    public Result getInvitationInfo(Integer invitationId){
        try {
            Invitation invitation = merchantAppService.getInvitationInfo(invitationId);
            return ResultUtil.success(invitation);
        } catch (Exception e){
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }
    }
}
