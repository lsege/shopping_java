package com.lsege.controller.otherController;

import com.lsege.entity.JsonResult;
import com.lsege.entity.Result;
import com.lsege.entity.otherVo.FreshNews;
import com.lsege.entity.otherVo.FreshNewsVo;
import com.lsege.exception.GlobalDefaultExceptionHandler;
import com.lsege.integration.HomemadeException;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import com.lsege.service.otherService.FreshNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Created by Administrator on 2017/10/13.
 */
@RestController
@RequestMapping("/freshNews")
public class FreshNewsController {

    @Autowired
    FreshNewsService freshNewsService;

    /**
     * 发布新鲜事或求助信息
     * 生成动态日志
     * @param request
     * @param title
     * @param context
     * @param type
     * @return
     */
    @PostMapping("/postedFreshNew")
    public Result postedFreshNew(HttpServletRequest request, String title, String context, String type,String image){
        String[] imageUrl;
        imageUrl = image.split(",");

        String newContext = "";
        if (type.equals("1")){
            String html= "<!DOCTYPE html>\n" +
                    "\n" +
                    "<html>\n" +
                    "\n" +
                    "\t<head>\n" +
                    "\t\t\n" +
                    "<meta charset=\"utf-8\">\n" +
                    "\t\t<meta name=\"viewport\" content=\"width=device-width,height=device-height, minimum-scale=1.0, maximum-scale=1.0\">\n" +
                    "\n" +
                    "\t</head>\n" +
                    "\t\n" +
                    "<style>\n" +
                    "\n" +
                    "\t\timg { height: auto; width: auto\\9; width:100%; }\n" +
                    "\t\t@media only screen and (min-width: 1200px){}\n" +
                    "\t\t\n" +
                    "media only screen and (min-width: 768px) and (max-width: 991px){}\n" +
                    "\n" +
                    "\t\t@media only screen and (max-width: 767px){}\n" +
                    "\n" +
                    "\t\t@media only screen and (-webkit-min-device-pixel-ratio: 1.3),only screen and (-o-min-device-pixel-ratio: 13/10),only screen and (min-resolution: 120dpi){}\n" +
                    "\t</style>\n" +
                    "\n" +
                    "\t<body>\n" +
                    "\n" +
                    "\t\t<h4 style=\"padding-left:20px\">"+title+"</h4>\n" +
                    "\t\t<span style=\"padding-left:20px\">"+context+"</span>\n" +
                    "\t\t<table style=\"padding-top:20px;\">";
            for(int i=0; i<imageUrl.length; i++){
                html += "<tr>\n" +
                        "\t\t\t\t<td><img src='"+imageUrl[i]+"'/></td\n" +
                        "\t\t\t</tr>" +
                        "<tr><td style=\"height=20px\"></td></td>";
            }
            newContext = html+"</table>"+
                    "</body>"+
                    "</html>";
        } else {
            newContext = context;
        }

        String finalType = "0";
        //发布新鲜事求助
        String userId=request.getAttribute("id").toString();
        freshNewsService.postedFreshNew(userId,title,context,newContext,type,finalType,imageUrl[0]);
        //生成日志信息
        return ResultUtil.success(ResultEnum.SUCCESS);
    }

    /**
     * 查看新鲜事或求助信息详情
     * 评论列表分页显示
     * @param freshNewId
     * @return
     */
    @PostMapping("/loadFreshNewInfo")
    @Transactional
    public Result loadFreshNewInfo(HttpServletRequest request, Integer freshNewId,Integer pageNum,Integer pageSize){
        //增加阅读量
        freshNewsService.addReadCount(freshNewId);
        FreshNewsVo freshNewsVo = freshNewsService.loadFreshNewInfo(request,freshNewId,pageNum,pageSize);
        return ResultUtil.success(freshNewsVo);
    }

    /**
     * 获取新鲜事求助列表，根据类型划分
     * @param type
     * @return
     */
    @PostMapping("/loadFreshNewList")
    public Result loadFreshNewList(String type,Integer pageNum,Integer pageSize){
        List<FreshNewsVo> list = freshNewsService.loadFreshNewList(type,pageNum,pageSize);
        return ResultUtil.success(list);
    }

    /**
     * 生成新鲜事丶求助评论信息
     * 生成评论日志
     * @param type
     * @param request
     * @param freshNewsId
     * @param commentariesContext
     * @return
     */
    @PostMapping("/commentFreshNew")
    public Result commentFreshNew(String type, HttpServletRequest request, Long freshNewsId, String commentariesContext) throws HomemadeException{
        String userId=request.getAttribute("id").toString();
        //进行评论
        freshNewsService.commentFreshNew(type,userId,freshNewsId,commentariesContext);
        return ResultUtil.success(ResultEnum.SUCCESS);
    }

    /**
     * 进行新鲜事点赞，生成点赞动态
     * @param request
     * @param freshNewId
     * @return
     */
    @PostMapping("/freshNewsLike")
    @Transactional
    public Result freshNewsLike(HttpServletRequest request,Integer freshNewId) throws HomemadeException{
        String userId=request.getAttribute("id").toString();
        String id = freshNewsService.queryLike(userId,freshNewId);
        if (id == null){
            //增加点赞数
            freshNewsService.addLikeCount(freshNewId);
            //进行点赞
            freshNewsService.freshNewsLike(userId,freshNewId);
        } else {
            throw new HomemadeException(ResultEnum.LIKECOUNT_ERROR);
        }
        return ResultUtil.success(ResultEnum.SUCCESS);
    }

    /**
     * 更改求助状态为已解决
     * @param request
     * @param freshNewId
     * @return
     */
    @PostMapping("/updateFreshNewsFinal")
    @ResponseBody
    @Transactional
    public Result updateFreshNewsFinal(HttpServletRequest request,Integer freshNewId){
        String userId=request.getAttribute("id").toString();
        FreshNews freshNews = freshNewsService.queryFreshNews(userId,freshNewId);
            if (freshNews.getId() == null){
                throw new HomemadeException(ResultEnum.THIS_NOT_YOURS);
            } else {
                if (freshNews.getFinalType().equals("1")){
                    //已经是解决状态
                    throw new HomemadeException(ResultEnum.THIS_IS_FINALLY);
                } else {
                    //解决求助
                    String finalType = "1";
                    freshNewsService.updateFreshNewsStatus(userId,freshNewId,finalType);
                }
            }
        return ResultUtil.success(ResultEnum.SUCCESS);
    }
}
