package com.lsege.controller.otherController;

import com.lsege.entity.JsonResult;
import com.lsege.entity.Result;
import com.lsege.entity.currency.CurrencyUser;
import com.lsege.entity.otherVo.ALogVo;
import com.lsege.entity.otherVo.AttentionVo;
import com.lsege.entity.otherVo.HotWord;
import com.lsege.entity.otherVo.OrdersVo;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import com.lsege.service.otherService.PayOrderService;
import com.lsege.service.otherService.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/10/14.
 */
@RestController
@RequestMapping("/userInfo")
public class UserInfoController {

    @Autowired
    UserInfoService userInfoService;
    @Autowired
    PayOrderService payOrderService;

    /**
     * 根据用户查询会员等级（会员中心功能）
     * @param request
     * @return
     */
    @PostMapping("/querUserLevel")
    public Result querUserLevel(HttpServletRequest request){
        String userId=request.getAttribute("id").toString();
        String level = userInfoService.querUserLevel(userId);
        return ResultUtil.success(level);
    }

    /**
     * 根据用户查询个人订单，根据付款时间排序
     * @param request
     * @return
     */
    @PostMapping("/queryUserOrders")
    public Result queryUserOrders(HttpServletRequest request){
        String userId=request.getAttribute("id").toString();
        List<OrdersVo> list = userInfoService.queryUserOrders(userId);
        for (int i=0; i<list.size(); i++){
            if(list.get(i).getPayDate().indexOf(".")!=-1){
                String payDate = list.get(i).getPayDate().substring(0,list.get(i).getPayDate().indexOf("."));
                System.out.println(payDate);
                list.get(i).setPayDate(payDate);
            }
        }
        return ResultUtil.success(list);
    }

    /**
     * 获取关注列表(根据关注事件排序)
     * @param request
     * @return
     */
    @PostMapping("/queryAttentionList")
    public Result queryAttentionList(HttpServletRequest request){
        String userId=request.getAttribute("id").toString();
        List<AttentionVo> list = userInfoService.queryAttentionList(userId);
        return ResultUtil.success(list);
    }

    /**
     * 发布反馈
     * @param request
     * @param title
     * @param context
     * @return
     */
    @PostMapping("/postedFeedback")
    public Result postedFeedback(HttpServletRequest request,String title,String context){
        String userId=request.getAttribute("id").toString();
        userInfoService.postedFeedback(userId,title,context);
        return ResultUtil.success(ResultEnum.SUCCESS);
    }

    /**
     * 修改用户个人信息
     * @param request
     * @param autograph
     * @param name
     * @param portrait
     * @param sex
     * @param birthday
     * @param address
     * @param phone
     * @return
     */
    @PostMapping("/changeUserInfo")
    public Result changeUserInfo(HttpServletRequest request,String autograph,String name,String portrait,String sex,
                                 String birthday,String address,String phone){
        String userId=request.getAttribute("id").toString();
        userInfoService.changeUserInfo(userId,autograph,name,portrait,sex,birthday,address,phone);
        return ResultUtil.success(ResultEnum.SUCCESS);
    }

    /**
     * 根据用户获取个人动态信息
     * @param request
     * @return
     */
    @PostMapping("/loadALogList")
    public Result loadALogList(HttpServletRequest request){
        String userId=request.getAttribute("id").toString();
        List<ALogVo> list = userInfoService.loadALogList(userId);
        return ResultUtil.success(list);
    }

    /**
     * 删除个人动态信息
     * @param request
     * @param logId
     * @return
     */
    @PostMapping("/deleteALog")
    @ResponseBody
    @Transactional
    public Result deleteALog(HttpServletRequest request,Integer logId){
        String userId=request.getAttribute("id").toString();
        userInfoService.deleteALog(userId,logId);
        return ResultUtil.success(ResultEnum.SUCCESS);
    }

    /**
     * 个人订单已优惠金额
     * @param request
     * @return
     */
    @PostMapping("/queryUserPreferentialMoney")
    public Result queryUserPreferentialMoney(HttpServletRequest request){
        String userId=request.getAttribute("id").toString();
        Integer c=userInfoService.queryUserPreferentialMoney(userId);
        Integer vipLevel = userInfoService.queryUserVipLevel(userId);
        Integer preferentialMoney =c!=null?c:0;
        Map<String,Object> map = new HashMap<>();
        map.put("vipLevel",vipLevel);
        map.put("preferentialMoney",preferentialMoney);
        return ResultUtil.success(map);
    }

    /**
     * 查询用户是否为vip
     * @param request
     * @return
     */
    @GetMapping("isVip")
    public Result  isVip(HttpServletRequest request){
        String userId = request.getAttribute("id").toString();
        CurrencyUser currencyUser=payOrderService.getUserByUserId(userId);
        if(currencyUser.getVipTimeout()!=null){
            if(currencyUser.getVipTimeout().getTime()> new Date().getTime()){
                return ResultUtil.success(true);
            }else{
                return ResultUtil.success(false);
            }
        }else{
            return ResultUtil.success(false);
        }

    }

    /**
     * 热词搜索
     *
     * @return
     */
    @GetMapping("getHotWordList")
    public Result getHotWordList(){
        try{
            List<HotWord> list = userInfoService.getHotWordList();
            return  ResultUtil.success(list);
        }catch (Exception e){
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }
    }

    @PostMapping("/userInvitationCode")
    @ResponseBody
    @Transactional
    public Result userInvitationCode(HttpServletRequest request,String code){
        try {
            String userId = request.getAttribute("id").toString();

            //查看当前用户是否关联过
            String id = userInfoService.queryUserInvitationCode(userId);
            if (id == null || id.length() <=0 || id.equals("")){
                //分解用户code
                String userCode = "";
                switch (code.length()) {
                    case 1:
                        userCode = code.substring(0,3);
                        break;
                    case 2:
                        userCode = code.substring(0,2);
                        break;
                    case 3:
                        userCode = code.substring(0,1);
                        break;
                    case 4:
                        userCode = code.substring(0,0);
                        break;
                    case 5:
                        userCode = code;
                        break;
                }
                //根据code判断用户是否存在
                String beUserId = userInfoService.queryUserByCode(userCode);
                if (beUserId == null || beUserId.length() <= 0 || beUserId.equals("")){
                    //用户不存在
                    return ResultUtil.error(ResultEnum.MERCHANT_LOGIN_ERROR);
                } else {
                    //关联用户邀请码，并为两个用户增加一天VIP
                    userInfoService.addUserCodeCorrelation(userId,userCode,beUserId);
                    return ResultUtil.success(ResultEnum.SUCCESS);
                }
            } else {
                return ResultUtil.error(ResultEnum.IS_NOT_REPEAT_ASSOCIATED);
            }
        } catch (Exception e){
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
        }
    }

    @GetMapping("/getMyUserCode")
    public Result getMyUserCode(HttpServletRequest request){
        String id = request.getAttribute("id").toString();
        String cede = userInfoService.queryMyUserCode(id);
        if (cede == null){
            return ResultUtil.success("");
        } else {
            return ResultUtil.success(cede);
        }
    }

    /**
     * 获得紧急电话列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping("/loadEmergencyCall")
    public Result loadEmergencyCall(Integer pageNum,Integer pageSize){
        return userInfoService.loadEmergencyCall(pageNum,pageSize);
    }

    /**
     * 获取用户VIP到期时间
     * @param request
     * @return
     */
    @GetMapping("/loadUserVipTimeOut")
    public Result loadUserVipTimeOut(HttpServletRequest request){
        String id = request.getAttribute("id").toString();
        return userInfoService.loadUserVipTimeOut(id);
    }

}
