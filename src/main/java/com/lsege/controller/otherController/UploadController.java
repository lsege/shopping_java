package com.lsege.controller.otherController;

import com.lsege.entity.JsonResult;
import com.lsege.entity.Result;
import com.lsege.integration.ResultEnum;
import com.lsege.integration.ResultUtil;
import com.lsege.service.otherService.ImageUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

/**
 * Created by zhanglei on 2017/11/16.
 */
@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    ImageUploadService imageUploadService;

    @PostMapping(value = "/uploadImage")
    @ResponseBody
    public Result uploadPrizePicForSwift(MultipartFile data) throws FileNotFoundException {
        if(data!=null){
            try {
                System.out.println("进入");
                String head = imageUploadService.updateHead(data, 4);//此处是调用上传服务接口，4是需要更新的userId 测试数据。
                System.out.println("=========================================================");
                System.out.println(head);
                return ResultUtil.success(head);
            }catch (Exception e){
                e.printStackTrace();
                System.out.println("上传失败");
                return ResultUtil.error(ResultEnum.UNKONWM_ERROR);
            }
        }else {
            System.out.println("file 是null");
            return ResultUtil.error(ResultEnum.NULL_ERROR);
        }
    }
}