package com.lsege;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
@ServletComponentScan
public class BenseMasterApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder application) {
		return application.sources(BenseMasterApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(BenseMasterApplication.class, args);
	}
	//git remote add git-osc git@git.oschina.net:xuzhongyao/reli-java.git
	/**
	 * 文件上传配置
	 * @return
	 */
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		//单个文件最大
		factory.setMaxFileSize("10240000000"); //KB,MB
		/// 设置总上传数据总大小
		factory.setMaxRequestSize("10240000000");
		return factory.createMultipartConfig();
	}
}
